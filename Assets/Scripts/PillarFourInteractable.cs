using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.VFX;
using Photon.Realtime;

public class PillarFourInteractable : MonoBehaviour, IInteractable
{
    public GameObject pillarSix;

    PhotonView view;

    private void Awake()
    {
        view = GetComponent<PhotonView>();
        transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
    }

    private void Start()
    {
        pillarSix = GameObject.Find("FirePillarSix");
    }
    void IInteractable.Interact()
    {
        view.RPC("InteractionRPC", RpcTarget.All);
    }

    
    [PunRPC]
    public void InteractionRPC()
    {

        if (pillarSix.transform.GetChild(0).gameObject.activeSelf)
        {
            pillarSix.transform.GetChild(0).gameObject.SetActive(false);
            pillarSix.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(false);
            pillarSix.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
        }
        else
        {
            pillarSix.transform.GetChild(0).gameObject.SetActive(true);
            pillarSix.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(true);
            pillarSix.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StartFire");
        }
            

        if (transform.GetChild(0).gameObject.activeSelf)
        {
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(false);
            transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
        }
        else
        {
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(true);
            transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StartFire");
        }
    }
}
