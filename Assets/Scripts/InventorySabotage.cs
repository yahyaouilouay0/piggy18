using UnityEngine;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine.UI;

/// <summary>
///     This Class is responsable of the inventory sabotage which will make Joke/Zela able to
///     remove all the items looted by humans and drop them in a cage in the map loocked with
///     a key and the humans must find a key to open then cage and get items back
/// </summary>
public class InventorySabotage : MonoBehaviour
{

    [SerializeField]
    private GameObject[] dropPoints;
    //[SerializeField]
    //private KeyCode sabotageActivateHotKey = KeyCode.T;
    public Inventory[] humansInventory;
    public Text txt;

    //Each human have a specific area where his items can drop
    [Tooltip("index 0: Xmin; index 1: Xmax; index 2: Zmin; index 3: Zmax")]
    [SerializeField]
    private int[] medicDropPointsRange;
    [Tooltip("index 0: Xmin; index 1: Xmax; index 2: Zmin; index 3: Zmax")]
    [SerializeField]
    private int[] engineerDropPointsRange;
    [Tooltip("index 0: Xmin; index 1: Xmax; index 2: Zmin; index 3: Zmax")]
    [SerializeField]
    private int[] athletDropPointsRange;

    public Vector3[] dropPointsPositions;
    public Button InventorySaboutageButton;

    PhotonView view;

    void Start()
    {
        view = gameObject.GetComponent<PhotonView>();
        if (view.IsMine)
        {
            dropPoints = GameObject.FindGameObjectsWithTag("DropPoint");
            dropPointsPositions = new Vector3[dropPoints.Length];
            for(int i=0;i< dropPoints.Length; i++)
            {
                dropPointsPositions[i] = dropPoints[i].transform.localPosition;
            }
        }
        Debug.Log(dropPointsPositions.Length);
    }

    private void Update()
    {
        if (view.IsMine)
        {
            HandleInventorySabotage();
        }
    }

    private void HandleInventorySabotage()
    {
        //if (Input.GetKeyDown(sabotageActivateHotKey))
        //{
        //    EnableSabotage();
        //}
    }

    public void EnableSabotage()
    {
        InventorySaboutageButton.interactable = false;
        txt.text = "Used";
        view.RPC("DropItems", RpcTarget.Others, (object)dropPointsPositions, (object)medicDropPointsRange, (object)engineerDropPointsRange, (object)athletDropPointsRange);
    }

    [PunRPC]
    void DropItems(Vector3[] dropPointss, int[] medicRange, int[] engineerRange, int[] athletRange)
    {
        GameObject currentClient = GameObject.FindObjectOfType<Canvas>().gameObject.transform.parent.gameObject;

        Inventory currentClientInventory = currentClient.GetComponent<Inventory>();


        for(int i=0; i<6; i++)
        {
            if (currentClientInventory.isFull[i])
            {
                Slot clientSlot = currentClientInventory.slots[i].GetComponent<Slot>();
                clientSlot.gameObject.GetComponent<Image>().sprite = clientSlot.defaultSlot;
                currentClientInventory.isFull[i] = false;
                currentClientInventory.isSelected[i] = false;
                Transform child = clientSlot.gameObject.transform.GetChild(1);
                Destroy(clientSlot.gameObject.transform.GetChild(0).gameObject);
                view.RPC("ResetItem", RpcTarget.All, child.gameObject.GetComponent<PhotonView>().ViewID, (object)dropPointss, (object)medicRange, (object)engineerRange, (object)athletRange, currentClient.name);             
            }
        }

    }
    
    [PunRPC]
    void ResetItem(int viewid, Vector3[] dropPointss, int[] medicRange, int[] engineerRange, int[] athletRange, string itemOwner)
    {
        //get the dropPoints in the specific player range
        Vector3[] dropPointsForClass;
        List<Vector3> dropPointsFilter = new List<Vector3>();

        if (itemOwner == "AthelteFPS(Clone)")
        {
            foreach (Vector3 drop in dropPointss)
            {
                if (drop.x >= athletRange[0] && drop.x <= athletRange[1] && drop.z >= athletRange[2] && drop.z <= athletRange[3])
                {
                    dropPointsFilter.Add(drop);
                }
            }
        }

        if (itemOwner == "MedicFPS(Clone)")
        {
            foreach (Vector3 drop in dropPointss)
            {
                if (drop.x >= medicRange[0] && drop.x <= medicRange[1] && drop.z >= medicRange[2] && drop.z <= medicRange[3])
                {
                    dropPointsFilter.Add(drop);
                }
            }
        }

        if (itemOwner == "EngineerFPS(Clone)")
        {
            foreach (Vector3 drop in dropPointss)
            {
                if (drop.x >= engineerRange[0] && drop.x <= engineerRange[1] && drop.z >= engineerRange[2] && drop.z <= engineerRange[3])
                {
                    dropPointsFilter.Add(drop);
                }
            }
        }

        dropPointsForClass = dropPointsFilter.ToArray();

        PhotonView photonView = PhotonNetwork.GetPhotonView(viewid);
        Transform child = photonView.gameObject.transform;
        child.SetParent(null);
        child.gameObject.SetActive(true);
        if (dropPointsForClass.Length > 0)
        {
            Debug.Log("This Client have drop point in his island he have "+dropPointsForClass.Length);
            int randomPosition = Random.Range(0, dropPointsForClass.Length - 1);
            dropPointsForClass[randomPosition].y += 5f;
            child.localPosition = dropPointsForClass[randomPosition];
            
        }
        else
        {
            Debug.Log("This Client using default drop point");
            child.localPosition = dropPointss[0];
        }
        child.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        child.gameObject.GetComponent<Rigidbody>().useGravity = true;
        child.gameObject.GetComponent<Pickup>().picked = false;
    }


}
