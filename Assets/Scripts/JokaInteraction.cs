using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JokaInteraction : MonoBehaviour
{
    [SerializeField]
    private KeyCode interact = KeyCode.F;
    private IJokaInteractable interactable;
    private bool hitAnInteractable = false;

    public float interactionDistance = 4f;


    private void Update()
    {
        Interaction();
    }

    void Interaction()
    {
        if (hitAnInteractable)
        {
            if (interactable != null)
            {
                if (Input.GetKeyDown(interact))
                {
                    interactable.Interact();
                }
            }
        }
        if (gameObject.transform.childCount > 2)
            gameObject.transform.GetChild(2).GetChild(10).gameObject.SetActive(hitAnInteractable);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PillarPuzzleCage")
        {
            interactable = other.GetComponent<IJokaInteractable>();
            hitAnInteractable = true;
        }
            
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "PillarPuzzleCage")
        {
            interactable = null;
            hitAnInteractable = false;
        }
            
    }
}
