﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_StatsRadarChart : MonoBehaviour {

    [SerializeField] private Material radarMaterial;
    [SerializeField] private Texture2D radarTexture2D;

    private Stats stats;
    private CanvasRenderer radarMeshCanvasRenderer;

    private void Awake() {
        radarMeshCanvasRenderer = transform.Find("radarMesh").GetComponent<CanvasRenderer>();
    }

    public void SetStats(Stats stats) {
        this.stats = stats;
        stats.OnStatsChanged += Stats_OnStatsChanged;
        UpdateStatsVisual();
    }

    private void Stats_OnStatsChanged(object sender, System.EventArgs e) {
        UpdateStatsVisual();
    }

    private void UpdateStatsVisual() {
        Mesh mesh = new Mesh();

        Vector3[] vertices = new Vector3[6];
        Vector2[] uv = new Vector2[6];
        int[] triangles = new int[3 * 5];

        float angleIncrement = 360f / 5;
        float radarChartSize = 145f;

        Vector3 jokaVertex = Quaternion.Euler(0, 0, -angleIncrement * 0) * Vector3.up * radarChartSize * stats.GetStatAmountNormalized(Stats.Type.Joka);
        int jokaVertexIndex = 1;
        Vector3 zelaVertex = Quaternion.Euler(0, 0, -angleIncrement * 1) * Vector3.up * radarChartSize * stats.GetStatAmountNormalized(Stats.Type.Zela);
        int zelaVertexIndex = 2;
        Vector3 engineerVertex = Quaternion.Euler(0, 0, -angleIncrement * 2) * Vector3.up * radarChartSize * stats.GetStatAmountNormalized(Stats.Type.Engineer);
        int engineerVertexIndex = 3;
        Vector3 medicVertex = Quaternion.Euler(0, 0, -angleIncrement * 3) * Vector3.up * radarChartSize * stats.GetStatAmountNormalized(Stats.Type.Medic);
        int medicVertexIndex = 4;
        Vector3 athleteVertex = Quaternion.Euler(0, 0, -angleIncrement * 4) * Vector3.up * radarChartSize * stats.GetStatAmountNormalized(Stats.Type.Athlete);
        int athleteVertexIndex = 5;

        vertices[0] = Vector3.zero;
        vertices[jokaVertexIndex]  = jokaVertex;
        vertices[zelaVertexIndex] = zelaVertex;
        vertices[engineerVertexIndex]   = engineerVertex;
        vertices[medicVertexIndex]    = medicVertex;
        vertices[athleteVertexIndex]  = athleteVertex;

        uv[0]                   = Vector2.zero;
        uv[jokaVertexIndex]   = Vector2.one;
        uv[zelaVertexIndex]  = Vector2.one;
        uv[engineerVertexIndex]    = Vector2.one;
        uv[medicVertexIndex]     = Vector2.one;
        uv[athleteVertexIndex]   = Vector2.one;

        triangles[0] = 0;
        triangles[1] = jokaVertexIndex;
        triangles[2] = zelaVertexIndex;

        triangles[3] = 0;
        triangles[4] = zelaVertexIndex;
        triangles[5] = engineerVertexIndex;

        triangles[6] = 0;
        triangles[7] = engineerVertexIndex;
        triangles[8] = medicVertexIndex;

        triangles[9]  = 0;
        triangles[10] = medicVertexIndex;
        triangles[11] = athleteVertexIndex;

        triangles[12] = 0;
        triangles[13] = athleteVertexIndex;
        triangles[14] = jokaVertexIndex;


        mesh.vertices = vertices;
        mesh.uv = uv;
        mesh.triangles = triangles;

        radarMeshCanvasRenderer.SetMesh(mesh);
        radarMeshCanvasRenderer.SetMaterial(radarMaterial, radarTexture2D);
    }

}
