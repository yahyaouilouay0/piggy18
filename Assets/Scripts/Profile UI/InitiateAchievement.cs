using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class InitiateAchievement : MonoBehaviour
{
    public GameObject achievementParent;
    public GameObject achievementPanel;
   
    private string _url = "https://jokasgame.herokuapp.com/achievements/ShowUserAchivements/";
    private string idUser;

    [System.Serializable]
    public class Achievements
    {
        public string label;
        public string description;
        public string type;
    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(OneSecondTimer());
        idUser = Session.Id;
        StartCoroutine(GetAchievements());
    }

    private IEnumerator OneSecondTimer()
    {
        yield return new WaitForSeconds(1);
    }



    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator GetAchievements()
    {
        using (UnityWebRequest request = UnityWebRequest.Get(_url + idUser))
        {
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log(request.error);
            }
            else
            {
                //Debug.Log("Received" + request.downloadHandler.text);
                var achievements = JsonConvert.DeserializeObject<List<Achievements>>(request.downloadHandler.text);
                foreach (Achievements a in achievements)
                {
                    switch (a.label)
                    {
                        case "Mom, bring the camera !":
                            Transform panel = Instantiate(achievementPanel, achievementParent.transform).transform;
                            panel.GetChild(0).GetComponent<Image>().sprite= Resources.Load<Sprite>("Sprites/"+a.type);
                            panel.GetChild(1).GetChild(0).GetComponent<Text>().text = a.label;
                            panel.GetChild(1).GetChild(1).GetComponent<Text>().text = a.description;
                            break;
                        case "Winner winner couscous dinner":
                            Transform panel1 = Instantiate(achievementPanel, achievementParent.transform).transform;
                            panel1.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/" + a.type);
                            panel1.GetChild(1).GetChild(0).GetComponent<Text>().text = a.label;
                            panel1.GetChild(1).GetChild(1).GetComponent<Text>().text = a.description;
                            break;
                    }

                }

            }
        }
    }
}
