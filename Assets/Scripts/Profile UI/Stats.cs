﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats {

    public event EventHandler OnStatsChanged;

    public static int STAT_MIN = 0;
    public static int STAT_MAX = 20;

    public enum Type {
        Joka,
        Zela,
        Engineer,
        Medic,
        Athlete,
    }

    private SingleStat jokaStat;
    private SingleStat zelaStat;
    private SingleStat engineerStat;
    private SingleStat medicStat;
    private SingleStat athleteStat;

    public Stats(int jokaStatAmount, int zelaStatAmount, int engineerStatAmount, int medicStatAmount, int athleteStatAmount) {
        jokaStat = new SingleStat(jokaStatAmount);
        zelaStat = new SingleStat(zelaStatAmount);
        engineerStat = new SingleStat(engineerStatAmount);
        medicStat = new SingleStat(medicStatAmount);
        athleteStat = new SingleStat(athleteStatAmount);
    }


    private SingleStat GetSingleStat(Type statType) {
        switch (statType) {
        default:
        case Type.Joka:       return jokaStat;
        case Type.Zela:      return zelaStat;
        case Type.Engineer:        return engineerStat;
        case Type.Medic:         return medicStat;
        case Type.Athlete:       return athleteStat;
        }
    }
    
    public void SetStatAmount(Type statType, int statAmount) {
        GetSingleStat(statType).SetStatAmount(statAmount);
        if (OnStatsChanged != null) OnStatsChanged(this, EventArgs.Empty);
    }

    public void IncreaseStatAmount(Type statType) {
        SetStatAmount(statType, GetStatAmount(statType) + 1);
    }

    public void DecreaseStatAmount(Type statType) {
        SetStatAmount(statType, GetStatAmount(statType) - 1);
    }

    public int GetStatAmount(Type statType) {
        return GetSingleStat(statType).GetStatAmount();
    }

    public float GetStatAmountNormalized(Type statType) {
        return GetSingleStat(statType).GetStatAmountNormalized();
    }



    /*
     * Represents a Single Stat of any Type
     * */
    private class SingleStat {

        private int stat;

        public SingleStat(int statAmount) {
            SetStatAmount(statAmount);
        }

        public void SetStatAmount(int statAmount) {
            stat = Mathf.Clamp(statAmount, STAT_MIN, STAT_MAX);
        }

        public int GetStatAmount() {
            return stat;
        }

        public float GetStatAmountNormalized() {
            return (float)stat / STAT_MAX;
        }
    }
}
