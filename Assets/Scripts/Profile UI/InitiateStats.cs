﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Photon.Pun;

public class InitiateStats : MonoBehaviour {

    [SerializeField] private UI_StatsRadarChart uiStatsRadarChart;
    [SerializeField] private Text winRateText;
    [SerializeField] private Text username;
    private string _url = "https://jokasgame.herokuapp.com/match/ShowUserGames/";
    private string idUser;
    private int numberOfMatchs;
    private float numberOfWins=0;
    private int JokaGames=1;
    private int ZelaGames=1;
    private int EngineerGames=1;
    private int MedicGames=1;
    private int AthleteGames=1;

    [System.Serializable]
    public class Match
    {
        public string role;
        public bool win;
    }

    private void Awake()
    {
        
        
    }

    private void Start() 
    {
        idUser = Session.Id;
        username.text = PhotonNetwork.NickName;
        StartCoroutine(GetMatch());

        
    }
    private IEnumerator OneSecondTimer()
    {
        yield return new WaitForSeconds(1);
    }

    private IEnumerator GetMatch()
    {
        using (UnityWebRequest request = UnityWebRequest.Get(_url+ idUser))
        {
            yield return request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log(request.error);
                
            }
            else
            {
                var Matchs = JsonConvert.DeserializeObject<List<Match>>(request.downloadHandler.text);
                numberOfMatchs = Matchs.Count;
                foreach (Match m in Matchs)
                {
                    switch(m.role.ToUpper())
                    {
                        case "ZELA": ZelaGames++;break;
                        case "JOKA" : JokaGames++;break;
                        case "ENGINEER": EngineerGames++;break;
                        case "MEDIC": MedicGames++;break;
                        case "ATHLETE": AthleteGames++;break;
                    }
                    if (m.win)
                        numberOfWins++;
                }
                Stats.STAT_MAX = numberOfMatchs + 1;
                Stats stats = new Stats(JokaGames, ZelaGames, EngineerGames, MedicGames, AthleteGames);
                uiStatsRadarChart.SetStats(stats);
                winRateText.text = (Mathf.Round((float)(numberOfWins / numberOfMatchs) * 100)).ToString() + "%";;
                

            }
        }
    }

}
