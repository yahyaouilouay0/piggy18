using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AthleteSpawn : MonoBehaviour
{
    PhotonView view;

    public GameObject[] playerHead = new GameObject[5];
    // Start is called before the first frame update
    void Start()
    {

        view = GetComponent<PhotonView>();
        if (view.IsMine)
        {
            view.RPC("RPC_ActivateHeadAthlete", RpcTarget.Others);
        }


    }

    [PunRPC]
    public void RPC_ActivateHeadAthlete()
    {
        for (int i = 0; i < 5; i++)
        {
            playerHead[i].SetActive(true);
        }
    }
}
