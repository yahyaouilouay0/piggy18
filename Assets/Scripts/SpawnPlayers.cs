using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;

/// <summary>
/// Class responsibale for spawning players in each client
/// </summary>
public class SpawnPlayers : MonoBehaviour
{
    public List<GameObject> players;
    public float minX;
    public float maxX;
    public float minZ;
    public float maxZ;
    public List<GameObject> SpawnPoints;

    private void Start()
    {       
        //Player client instanitating
        //Vector3 playerRandomPosition = new Vector3(Random.Range(minX, maxX), 5f, Random.Range(minZ, maxZ));
        Vector3 notebookRandomPosition = new Vector3(Random.Range(minX, maxX), 5f, Random.Range(minZ, maxZ));
        PhotonNetwork.Instantiate(players[(int)PhotonNetwork.LocalPlayer.CustomProperties["Role"]].name, SpawnPoints[(int)PhotonNetwork.LocalPlayer.CustomProperties["Role"]].transform.position, Quaternion.identity);
        PhotonNetwork.Instantiate("Notebook", notebookRandomPosition, Quaternion.identity);
    }
}
