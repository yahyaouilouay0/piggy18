using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.VFX;
using Photon.Realtime;


public class PillarFiveInteractable : MonoBehaviour, IInteractable
{
    public GameObject pillarTwo;
    public GameObject pillarThree;

    PhotonView view;

    private void Awake()
    {
        view = GetComponent<PhotonView>();
        transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
    }

    private void Start()
    {
        pillarTwo = GameObject.Find("FirePillarTwo");
        pillarThree = GameObject.Find("FirePillarThree");
    }

    void IInteractable.Interact()
    {
        view.RPC("InteractionRPC", RpcTarget.All);
    }

    
    [PunRPC]
    public void InteractionRPC()
    {
        
        if (pillarTwo.transform.GetChild(0).gameObject.activeSelf)
        {
            pillarTwo.transform.GetChild(0).gameObject.SetActive(false);
            pillarTwo.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(false);
            pillarTwo.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
        }
        else
        {
            pillarTwo.transform.GetChild(0).gameObject.SetActive(true);
            pillarTwo.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(true);
            pillarTwo.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StartFire");
        }
            
        if (pillarThree.transform.GetChild(0).gameObject.activeSelf)
        {
            pillarThree.transform.GetChild(0).gameObject.SetActive(false);
            pillarThree.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(false);
            pillarThree.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
        }
        else
        {
            pillarThree.transform.GetChild(0).gameObject.SetActive(true);
            pillarThree.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(true);
            pillarThree.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StartFire");
        }

    }
}
