using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OfferingPlate : MonoBehaviour
{
    public GameObject offering;
    public bool offeringDetected;
    public string offeringRequired;



    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "item" && collision.gameObject.name == offeringRequired)
        {
            offeringDetected = true;
            transform.parent.GetComponent<GhostQuest>().offeringAccepted = true;
            collision.gameObject.SetActive(false);
            gameObject.SetActive(false);
        }
    }

}