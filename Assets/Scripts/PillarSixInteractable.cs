using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.VFX;
using Photon.Realtime;

public class PillarSixInteractable : MonoBehaviour, IInteractable
{
    PhotonView view;

    private void Awake()
    {
        view = GetComponent<PhotonView>();
        transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
    }


    void IInteractable.Interact()
    {
        view.RPC("InteractionRPC", RpcTarget.All);
    }

    
    [PunRPC]
    public void InteractionRPC()
    {
        if (transform.GetChild(0).gameObject.activeSelf)
        {
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(false);
            transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
        }
        else
        {
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(true);
            transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StartFire");
        }
            
    }
    

}
