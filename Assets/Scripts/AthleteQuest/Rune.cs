
using UnityEngine;
using Photon.Pun;


[RequireComponent(typeof(PhotonView))]
[RequireComponent(typeof(PhotonTransformViewClassic))]
public class Rune : MonoBehaviour
{
    [Range(1,3)]
    public int runeId;
    public string runeText;

    private void Start()
    {
        gameObject.GetComponentInChildren<TextMesh>().text = runeText;
    }
}
