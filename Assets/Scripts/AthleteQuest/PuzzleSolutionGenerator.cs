using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class PuzzleSolutionGenerator : MonoBehaviour
{
    [SerializeField]
    private GameObject grid;
    private MagicDoorManager shapeGenerator;
    PhotonView view;
    private GameObject gridInstance;

    private void Start()
    {
        view = gameObject.GetComponent<PhotonView>();
        shapeGenerator = GameObject.FindObjectOfType<MagicDoorManager>();
    }

    private void Update()
    {
        if (view.IsMine)
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var selection = hit.transform;
                if (selection.CompareTag("puzzleSolution") && Input.GetKeyDown(KeyCode.F))
                {
                    Cursor.lockState = CursorLockMode.None;
                    GameObject.Find("EngineerFPS(Clone)").GetComponent<PlayerMovement>().isWriting = true;
                    GameObject gridInstance = PhotonNetwork.Instantiate(grid.name, grid.transform.position, Quaternion.identity);
                    gridInstance.transform.SetParent(gameObject.GetComponentInChildren<Canvas>().gameObject.transform);
                    gridInstance.GetComponent<RectTransform>().localPosition = new Vector3(0f, 0f, 0f);
                    for (int i = 0; i < shapeGenerator.shapes.Length; i++)
                    {
                        GameObject gridElement = gridInstance.transform.GetChild(0).GetChild(i).gameObject;
                        gridElement.SetActive(true);
                        gridElement.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = shapeGenerator.shapes[i];
                        gridElement.GetComponentInChildren<Text>().text = shapeGenerator.combinations[i];
                    }
                    GameObject.FindObjectOfType<AudioManager>().Stop("PageFlip");
                }
            }
        }
    }

    public void CloseSolution()
    {
        Cursor.lockState = CursorLockMode.Locked;
        GameObject.Find("EngineerFPS(Clone)").GetComponent<PlayerMovement>().isWriting = false;
        PhotonNetwork.Destroy(GameObject.Find("MagicalDoorPuzzleSolution(Clone)"));
    }
}
