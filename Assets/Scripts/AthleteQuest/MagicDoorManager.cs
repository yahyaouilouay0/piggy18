using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

/// <summary>
///     This Class Is Responsable of the combination validation
///     It Contain all the possible combinations
/// </summary>
[RequireComponent(typeof(PhotonView))]
public class MagicDoorManager : MonoBehaviour
{
    public Sprite[] shapes;
    public string[] combinations = new string[6];
    [SerializeField]
    private MagicRuneSlot slotARef;
    [SerializeField]
    private MagicRuneSlot slotBRef;
    [SerializeField]
    private int runesQuantity = 3;
    public int theDoorKey;
    private float opacity = 1f;
    PhotonView view;

    private void Start()
    {
        view = gameObject.GetComponent<PhotonView>();
        int compt = -1;
        for (int i = 1; i<= runesQuantity; i++)
        {
            for (int j = 1; j <= runesQuantity; j++)
            {
                if (i != j)
                {
                    compt++;
                    combinations[compt] = i.ToString() + j.ToString(); 
                }
            }
        }
        if (view.IsMine)
        {
            theDoorKey = Random.Range(0, 5);
            view.RPC("SetRandomShape", RpcTarget.All, theDoorKey);
        }
    }
    public void CheckRunesInSlots()
    {
        string currentCombinationInSlots = slotARef.runeInSlotId.ToString() + slotBRef.runeInSlotId.ToString();
        if (currentCombinationInSlots == combinations[theDoorKey])
        {
            PhotonView shapeView = gameObject.GetComponent<PhotonView>();
            shapeView.RPC("disableItems", RpcTarget.All, shapeView.ViewID);
            GameObject.FindObjectOfType<AudioManager>().Play("QuestCompleted");
            gameObject.transform.parent.gameObject.GetComponent<Collider>().isTrigger = true;
        }
    }

    [PunRPC]
    void disableItems(int shapeID)
    {
        PhotonView shapeView = PhotonNetwork.GetPhotonView(shapeID);
        shapeView.gameObject.SetActive(false);
        shapeView.gameObject.transform.parent.gameObject.GetComponent<Animator>().SetTrigger("KeySolved");
        GameObject.Find("GameStateChecker").GetComponent<GameStateChecker>().questCounter += 1;
        foreach (Rune r in GameObject.FindObjectsOfType<Rune>())
        {
            r.gameObject.SetActive(false);
        }
    }

    [PunRPC]
    void SetRandomShape(int randomIndex)
    {
        GameObject shapeRanderer = GameObject.FindObjectOfType<MagicDoorManager>().gameObject;
        shapeRanderer.GetComponent<SpriteRenderer>().sprite = shapeRanderer.GetComponent<MagicDoorManager>().shapes[randomIndex];
        shapeRanderer.GetComponent<MagicDoorManager>().theDoorKey = randomIndex;
    }

}
