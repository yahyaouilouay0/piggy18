using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;
using UnityEngine.UI;

/// <summary>
///     This script to manage the slot behaviour
/// </summary>
[RequireComponent(typeof(PhotonView))]
public class MagicRuneSlot : MonoBehaviour
{
    [SerializeField]
    private float runeSlotRange = 0.5f;
    [SerializeField]
    private LayerMask runeMask;
    public int runeInSlotId = -1;
    [SerializeField]
    private bool runeInSlot;
    PhotonView view;
    private void Start()
    {
        view = gameObject.GetComponent<PhotonView>();
        if (view.IsMine)
        {
            StartCoroutine(MagicDoorSlotRoutine());
        }
    }

    private IEnumerator MagicDoorSlotRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.2f);
            lookForRune();
        }
    }

    private void lookForRune()
    {
        Collider[] slotACollider = Physics.OverlapSphere(transform.position, runeSlotRange, runeMask);
        //Test if there is a rune in range of a slot
        if (slotACollider.Length > 0)
        {
            // Test if there is no rune in the slot alreadt
            if (!runeInSlot)
            {
                //Set the rune info in the slot
                //runeInSlot = true;
                //runeInSlotId = slotACollider[0].GetComponent<Rune>().runeId;
                view.RPC("ToggleSlotState", RpcTarget.All, slotACollider[0].GetComponent<Rune>().runeId, true, view.ViewID);
                //Remove the rune from the inventory of the player
                GameObject player = GameObject.Find("AthelteFPS(Clone)");
                int runeId = slotACollider[0].GetComponent<PhotonView>().ViewID;
                view.RPC("RemoveRuneFromPlayer", RpcTarget.All, player.GetComponent<PhotonView>().ViewID, runeId);
                
            }
        }
        else if (runeInSlot)
        {
            view.RPC("ToggleSlotState", RpcTarget.All, -1 ,false, view.ViewID);
        }

    }

    [PunRPC]
    void RemoveRuneFromPlayer(int placerId, int runeViewId)
    {
        GameObject player = GameObject.FindObjectOfType<Canvas>().transform.parent.gameObject;
        if (player.GetComponent<PhotonView>().ViewID == placerId)
        {
            Inventory playerInventory = player.GetComponent<Inventory>();
            int indexSelected = -1;
            for (int i = 0; i < playerInventory.isSelected.Length; i++)
            {
                if (playerInventory.isSelected[i])
                {
                    indexSelected = i;
                    break;
                }
            }

            Slot playerSlot = playerInventory.slots[indexSelected].GetComponent<Slot>();
            playerSlot.gameObject.GetComponent<Image>().sprite = playerSlot.defaultSlot;
            playerInventory.isFull[indexSelected] = false;
            playerInventory.isSelected[indexSelected] = false;
            Destroy(playerSlot.gameObject.transform.GetChild(0).gameObject);
            gameObject.GetComponent<PhotonView>().RPC("SetRuneInSlot", RpcTarget.All, runeViewId, transform.position);
        
            GameObject.FindObjectOfType<MagicDoorManager>().CheckRunesInSlots();
        }
    }

    [PunRPC]
    void SetRuneInSlot(int runeId, Vector3 newPos)
    {
        PhotonView runeView = PhotonNetwork.GetPhotonView(runeId);
        Transform child = runeView.gameObject.transform;
        child.SetParent(null);
        child.gameObject.SetActive(true);
        child.position = newPos;
        child.rotation = Quaternion.identity;
        child.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        child.gameObject.GetComponent<Pickup>().picked = false;
        child.gameObject.GetComponent<Rigidbody>().useGravity = false;
    }

    [PunRPC]
    void ToggleSlotState (int runeid, bool state, int runeSlotId)
    {
        GameObject runeslot = PhotonNetwork.GetPhotonView(runeSlotId).gameObject;

        runeslot.GetComponent<MagicRuneSlot>().runeInSlot = state;
        runeslot.GetComponent<MagicRuneSlot>().runeInSlotId = runeid;
        
    }
}
