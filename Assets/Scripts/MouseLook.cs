using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System;

/// <summary>
/// Cette classe contient le contr�le de la cam�ra qui suit le curseur.
/// </summary>
public class MouseLook : MonoBehaviour
{
    public float mouseSensitivity = 100f;
    public Transform playerBody;
    private float xRotation = 0f;
    [HideInInspector]
    //public bool isWriting=false;

    PhotonView view;

    // Start is called before the first frame update
    void Awake()
    {
        view = GetComponent<PhotonView>();
        if (view.IsMine)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(view.IsMine)
        {
            //Ne pas bouger la cam�ra si le joueur est en train d'�crire
            if(!playerBody.gameObject.GetComponent<PlayerMovement>().isWriting)
            {
                //R�cuperer l'input du axe X � partir de la souris
                float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
                //R�cuperer l'input du axe Y � partir de la souris
                float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
                xRotation -= mouseY;
                //Limiter la rotation Verticale � l'angle 90� nord et -90� sud
                xRotation = Mathf.Clamp(xRotation, -75f, 60f);
                //Appliquer la rotation de la cam�ra sur son axe X (L'axe Y de la souris)
                transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
                //faire une rotation du joueur quand il d�palce � la cam�ra sur l'axe Y (L'axe X de la souris)
                playerBody.Rotate(Vector3.up * mouseX);
            }

        }
    }

}
