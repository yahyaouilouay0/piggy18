using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class Options : MonoBehaviour
{
    public GameObject optionsPanel;
    public GameObject mainMenuPanel;
    public Button lowButton;
    public Button mediumButton;
    public Button highButton;
    public RenderPipelineAsset[] quality;
    public AudioSource audioSource;
    public Slider volumeSlider;

    public void Start()
    {
        volumeSlider.value = audioSource.volume ;
        highButton.Select();
    }
    public void SetVolume()
    {
        audioSource.volume = volumeSlider.value;
    }
    public void OnClickHome()
    {
        optionsPanel.SetActive(false);
        mainMenuPanel.SetActive(true);
    }

    public void ChangeQuality(int value)
    {
        QualitySettings.SetQualityLevel(value);
        QualitySettings.renderPipeline = quality[value];
    }

    public void OnClickLow()
    {
        ChangeQuality(0);
    }

    public void OnClickMedium()
    {
        ChangeQuality(1);
    }

    public void OnClickHigh()
    {
        ChangeQuality(2);
    }
}
