using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ColorConfig : MonoBehaviour,IInteractable
{
    // Start is called before the first frame update
    public PhotonView view;

    private bool setColor;

    private void Start()
    {
        view = GetComponent<PhotonView>();
        setColor = false;
    }

    private void Update()
    {
        if (setColor)
        {
            view.RPC("ColorSettingFunction", RpcTarget.All);
            setColor = false;
        }
    }

    public void Interact()
    {
        setColor = true;
    }

    [PunRPC]
    public void ColorSettingFunction()
    {
        transform.parent.GetChild(0).GetComponent<SetBandTrait>().bandColorSet = GetComponent<Renderer>().material.color;
    }
}
