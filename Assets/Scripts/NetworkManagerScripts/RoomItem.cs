using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class for Room Item prefab
/// </summary>
public class RoomItem : MonoBehaviour
{
    public Text roomName;
    LobbyManager manager;
    public RoomInfo RoomInfo;

    private void Start()
    {
        manager = FindObjectOfType<LobbyManager>();
    }

    /// <summary>
    /// Function to set room information
    /// </summary>
    /// <param name="roomInfo">room information</param>
    public void SetRoomInfo(RoomInfo roomInfo)
    {
        roomName.text = roomInfo.Name;
        RoomInfo = roomInfo;
    }

    /// <summary>
    /// On Click Event when clicked a room from rooms list
    /// </summary>
    public void OnClickItem()
    {
        manager.JoinRoom(RoomInfo.Name);
    }
}
