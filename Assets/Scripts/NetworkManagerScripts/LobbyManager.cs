using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using UnityEngine.SceneManagement;

/// <summary>
/// Class responsible for lobby management
/// </summary>
public class LobbyManager : MonoBehaviourPunCallbacks
{
    public InputField roomNameInput;
    public GameObject lobbyPanel;
    public GameObject roomPanel;
    public GameObject warningPanel;
    public GameObject kickedPanel;
    public Text roomName;
    public Text errorText;
    public Text noRoomText;
    public GameObject profileButton;
    public GameObject mainMenuButton;
    public GameObject startButton;
    public GameObject loadingPanel;
    public GameObject roomListLayout;
    bool isLeft;
    Hashtable hash = new Hashtable();
    List<int> roleIds = new List<int>() { 0, 1, 2, 3 };
    //List<int> roleIds = new List<int>() { 0, 1 };
    public RoomItem roomItemPrefab;
    public Transform contentObject;
    public PlayerItemPrefab playerItemPrefab;
    public Transform playersContentObject;
    private static Dictionary<string, RoomInfo> cachedRoomList = new Dictionary<string, RoomInfo>();
    public byte maxPlayers = 4;


    

    /// <summary>
    /// On Click Event function to create room
    /// </summary>
    public void OnClickCreate()
    {
        if (roomNameInput.text.Length >= 1)
        {
            PhotonNetwork.CreateRoom(roomNameInput.text, new RoomOptions() { MaxPlayers = maxPlayers });
            errorText.gameObject.SetActive(false);
        }
        else
        {
            errorText.gameObject.SetActive(true);
        }
    }

    private void Update()
    {
        if (roomListLayout.transform.childCount > 0)
        {
            noRoomText.gameObject.SetActive(false);
        }
        else
        {
            noRoomText.gameObject.SetActive(true);
        }
    }

    public void Start()
    {
        PhotonNetwork.JoinLobby();
    }

    /// <summary>
    /// On Click Event function to leave room
    /// </summary>
    public void OnClickLeaveRoom()
    {
        isLeft = true;
        PhotonNetwork.LeaveRoom();
        profileButton.SetActive(true);
        mainMenuButton.SetActive(true);
    }

    public void OnClickMainMenu()
    {
        SceneManager.LoadScene("MainScene");
    }

    /// <summary>
    /// On Click Event function to start game
    /// </summary>
    public void OnClickStartGame()
    {
        if (PhotonNetwork.CurrentRoom.PlayerCount < 4)
            warningPanel.SetActive(true);
        else
        {
            roomPanel.SetActive(false);
            loadingPanel.SetActive(true);
            PhotonNetwork.LoadLevel("MainLevel");
        }
            
    }

    /// <summary>
    /// On Click Event function to confirm warning box
    /// </summary>
    public void OnClickConfirm()
    {
        warningPanel.SetActive(false);
    }

    /// <summary>
    /// On Click Event function to confirm kick message
    /// </summary>
    public void OnClickOkConfirm()
    {
        kickedPanel.SetActive(false);
    }

    /// <summary>
    /// Function to join a room
    /// </summary>
    /// <param name="_roomName"> room name</param>
    public void JoinRoom(string _roomName)
    {
        isLeft = false;
        PhotonNetwork.JoinRoom(_roomName);
        
    }

    /// <summary>
    /// Callback function when successfully joined a room
    /// </summary>
    public override void OnJoinedRoom()
    {
        profileButton.SetActive(false);
        mainMenuButton.SetActive(false);
        //Assign random role to each player using Player.CustomProperties
        Player[] players1 = PhotonNetwork.PlayerList;
        for (int i = 0; i < players1.Length; i++)
        {
            if(players1[i].CustomProperties["Role"]!=null)
                if (roleIds.Contains((int)players1[i].CustomProperties["Role"]))
                {
                    roleIds.Remove((int)players1[i].CustomProperties["Role"]);
                }
        }
        if (roleIds.Count > 0)
        {
            int role = roleIds[Random.Range(0, roleIds.Count - 1)];
            if(!hash.ContainsKey("Role"))
                hash.Add("Role", role);
            roleIds.Remove(role);
            PhotonNetwork.LocalPlayer.SetCustomProperties(hash);
        }
        
        //Hide lobby panel and display room panel
        lobbyPanel.SetActive(false);
        roomPanel.SetActive(true);
        roomName.text = "Room: " + PhotonNetwork.CurrentRoom.Name;
        
        //Loop to display players prefabs in the room
        Player[] players = PhotonNetwork.PlayerList;
        foreach(Transform child in playersContentObject)
        {
            Destroy(child.gameObject);
        }
        for (int i = 0; i < players.Length; i++)
        {
            PlayerItemPrefab playerItem = Instantiate(playerItemPrefab, playersContentObject);
            playerItem.GetComponent<PlayerItemPrefab>().SetPlayerInfo(players[i]);
            Button kickButton = playerItem.transform.GetChild(3).GetComponent<Button>();
            kickButton.onClick.AddListener(playerItem.OnClickKickPlayer);
            
        }

        startButton.SetActive(PhotonNetwork.IsMasterClient);
    }

    /// <summary>
    /// Callback function when switching master client
    /// </summary>
    /// <param name="newMasterClient">new host player</param>
    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        startButton.SetActive(PhotonNetwork.IsMasterClient);
    }

    /// <summary>
    /// Callback function to update room list
    /// </summary>
    /// <param name="roomList">room list</param>
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        
        //Loop to display room list in the lobby panel
        foreach (Transform trans in contentObject)
        {
            Destroy(trans.gameObject);
        }
        for (int i = 0; i < roomList.Count; i++)
        {
            RoomInfo info = roomList[i];
            if (info.RemovedFromList)
            {
                cachedRoomList.Remove(info.Name);
            }
            else
            {
                cachedRoomList[info.Name] = info;
            }
        }
        foreach (KeyValuePair<string, RoomInfo> entry in cachedRoomList)
        {
            Instantiate(roomItemPrefab, contentObject).GetComponent<RoomItem>().SetRoomInfo(cachedRoomList[entry.Key]);
        }
        
    }

    /// <summary>
    /// Callback function when a player left a room
    /// </summary>
    public override void OnLeftRoom()
    {
        //Hide room panel and display lobby panel
        roomPanel.SetActive(false);
        if (!isLeft)
            kickedPanel.SetActive(true);
        lobbyPanel.SetActive(true);
        roomNameInput.text = "";
        cachedRoomList.Clear();
        profileButton.SetActive(true);
        mainMenuButton.SetActive(true);
    }

    /// <summary>
    /// Callback function when the player left the room
    /// </summary>
    /// <param name="otherPlayer">the player that left the room</param>
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        //Reset player CustomProperties when left the room
        roleIds.Add((int)otherPlayer.CustomProperties["Role"]);
        otherPlayer.SetCustomProperties(null);
        if (otherPlayer.IsMasterClient)
            otherPlayer.SetCustomProperties(null);
    }

    /// <summary>
    /// Callback function when connecting to the lobby
    /// </summary>
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
    }

    /// <summary>
    /// Callback function when a player enter a room
    /// </summary>
    /// <param name="newPlayer">new player entered the room</param>
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        PlayerItemPrefab playerItem = Instantiate(playerItemPrefab, playersContentObject);
        playerItem.GetComponent<PlayerItemPrefab>().SetPlayerInfo(newPlayer);
        Button kickButton = playerItem.transform.GetChild(3).GetComponent<Button>();
        kickButton.onClick.AddListener(playerItem.OnClickKickPlayer);
    }

}
