using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Photon.Realtime;
using UnityEngine.Networking;
using System.Text.RegularExpressions;

/// <summary>
/// Main class to connect to the photon server
/// </summary>
public class ConnectToServer : MonoBehaviourPunCallbacks
{
    private string connectionEndPoint = "https://jokasgame.herokuapp.com";
    public InputField loginUsernameInput;
    public InputField loginPasswordInput;
    public InputField registerUsernameInput;
    public InputField registerPasswordInput;
    public InputField registerConfirmPasswordInput;
    public InputField emailInput;
    public InputField dateOfBirthInput;
    public Text loginButtonText;
    public GameObject LoginPanel;
    public GameObject ConnectingPanel;
    public GameObject MainMenuPanel;
    public GameObject OptionsPanel;
    public GameObject RegistrationPanel;
    public Text errorText;
    public Text regErrorText;
    public Text usernameErrorText;
    public Text emailErrorText;
    public Text passwordErrorText;
    public Text confirmPwErrorText;
    public Text dateErrorText;
    public Text registerButtonText;
    public Button registerButton;
    private bool username;
    private bool email;
    private bool password;
    private bool date;
    private void Start()
    {
        if (PlayerPrefs.HasKey("username"))
        {
            if (PhotonNetwork.InLobby)
            {
                ConnectingPanel.SetActive(false);
                MainMenuPanel.SetActive(true);
            }
            else
            {
                LoginPanel.SetActive(false);
                ConnectingPanel.SetActive(true);
                PhotonNetwork.NickName = PlayerPrefs.GetString("username", "No Name");
                Session.Id = PlayerPrefs.GetString("id", "null id");
                PhotonNetwork.ConnectUsingSettings();
            }
        }
        else
        {
            LoginPanel.SetActive(true);
            ConnectingPanel.SetActive(false);
        }
    }
    /// <summary>
    /// On Click Event to connect to photon server
    /// </summary>
    public void OnClickLogin()
    {
        if(loginUsernameInput.text.Length > 0 || loginPasswordInput.text.Length > 0)
        {
            StartCoroutine(TryLogin());
        }
        else
        {
            errorText.text = "Fields are empty";
        }
    }

    public void OnClickLogout()
    {
        PlayerPrefs.DeleteAll();
        PhotonNetwork.Disconnect();
        MainMenuPanel.SetActive(false);
        LoginPanel.SetActive(true);
    }

    public void OnClickOptions()
    {
        OptionsPanel.SetActive(true);
        MainMenuPanel.SetActive(false);
    }

    public void OnClickQuit()
    {
        Application.Quit();
    }

    public void OnClickRegister()
    {
        if (registerUsernameInput.text.Length == 0 || registerPasswordInput.text.Length == 0 || emailInput.text.Length == 0 || dateOfBirthInput.text.Length == 0)
        {   
            regErrorText.text = "Fields are empty";
            username = false;
            email = false;
            password = false;
            date = false;
        }
        else
        {
            regErrorText.text = "";
        }

        if ((registerUsernameInput.text.Length < 3 || registerUsernameInput.text.Length > 20) && registerUsernameInput.text.Length > 0)
        {
            usernameErrorText.text = "Between 3 and 20 characters";
            username = false;
        }
        else if (!Regex.IsMatch(registerUsernameInput.text, "^[a-zA-Z][a-zA-Z0-9]*$", RegexOptions.IgnoreCase))
        {
            usernameErrorText.text = "Must start with alphabet";
            username = false;
        }
        else
        {
            username = true;
            usernameErrorText.text = "";
        }

        if(!Regex.IsMatch(emailInput.text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase) && emailInput.text.Length > 0)
        {
            email = false;
            emailErrorText.text = "Invalid email format";
        }
        else
        {
            email = true;
            emailErrorText.text = "";
        }

        if (registerPasswordInput.text.Length < 6 && registerPasswordInput.text.Length > 0)
        {
            password = false;
            passwordErrorText.text = "Minimum 6 symbols";
        }
        else
        {
            password = true;
            passwordErrorText.text = "";
        }

        if (string.Compare(registerPasswordInput.text,registerConfirmPasswordInput.text) != 0)
        {
            password = false;
            confirmPwErrorText.text = "Password doesn't match";
        }
        else
        {
            password = true;
            confirmPwErrorText.text = "";
        }

        if (!Regex.IsMatch(dateOfBirthInput.text, "^(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)[0-9]{2}$", RegexOptions.IgnoreCase) && dateOfBirthInput.text.Length > 0)
        {
            date = false;
            dateErrorText.text = "Invalid date format, use : dd/mm/yyyy";
        }
        else
        {
            date = true;
            dateErrorText.text = "";
        }
        if (username && password && email && date)
        {
            registerButtonText.text = "Creating...";
            registerButton.interactable = false;
            StartCoroutine(TryRegister());
        }
            
    }

    /// <summary>
    /// Callback function when successfully connected to photon server 
    /// </summary>
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.EnableCloseConnection = true;
    }

    /// <summary>
    /// Callback function when successfully joined a lobby
    /// </summary>
    public override void OnJoinedLobby()
    {
        LoginPanel.SetActive(false);
        ConnectingPanel.SetActive(false);
        MainMenuPanel.SetActive(true);
    }

    public void OnClickJoinPhoton()
    {
        SceneManager.LoadScene("LobbyScene");
    }

    public void OnClickSwitchToLoginPanel()
    {
        LoginPanel.SetActive(true);
        RegistrationPanel.SetActive(false);
    }

    public void OnClickSwitchToRegistrationPanel()
    {
        LoginPanel.SetActive(false);
        RegistrationPanel.SetActive(true);
    }

    public IEnumerator TryLogin()
    {
        WWWForm form = new WWWForm();
        form.AddField("username", loginUsernameInput.text);
        form.AddField("password", loginPasswordInput.text);
        errorText.text = "";
        loginButtonText.text = "Connecting...";

        UnityWebRequest request = UnityWebRequest.Post(connectionEndPoint+"/user/signin", form);
        var handler = request.SendWebRequest();

        float startTime = 0.0f;
        while (!handler.isDone)
        {
            startTime += Time.deltaTime;
            if(startTime > 10.0f)
            {
                break;
            }
            yield return null;
        }

        if (request.result == UnityWebRequest.Result.Success)
        {
            UserAccount userAccount = JsonUtility.FromJson<UserAccount>(request.downloadHandler.text);
            Session.Id = userAccount.id;
            PhotonNetwork.NickName = loginUsernameInput.text;
            loginButtonText.text = "Connecting...";
            PlayerPrefs.SetString("username", loginUsernameInput.text);
            PlayerPrefs.SetString("id", Session.Id);
            PhotonNetwork.ConnectUsingSettings();
        }
        else
        {        
            UserAccount userAccount = JsonUtility.FromJson<UserAccount>(request.downloadHandler.text);           
            if(userAccount != null)
            {
                if (userAccount.error == "Invalid Credentials")
                {                    
                    errorText.text = userAccount.error;
                    loginButtonText.text = "Login";
                }
            }
            else
            {
                loginButtonText.text = "Login";
                errorText.text = "Server Error";
            }
            
        }
        yield return null;
    }

    public IEnumerator TryRegister()
    {
        WWWForm form = new WWWForm();
        form.AddField("username", registerUsernameInput.text);
        form.AddField("email", emailInput.text);
        form.AddField("password", registerPasswordInput.text);
        form.AddField("dateOfBirth", dateOfBirthInput.text);


        UnityWebRequest request = UnityWebRequest.Post(connectionEndPoint+"/user/signup", form);
        var handler = request.SendWebRequest();
        
        float startTime = 0.0f;
        while (!handler.isDone)
        {
            startTime += Time.deltaTime;
            if (startTime > 10.0f)
            {
                break;
            }
            yield return null;
        }
        Debug.Log(request.downloadHandler.text);
        if (request.result == UnityWebRequest.Result.Success)
        {
            registerUsernameInput.text = "";
            emailInput.text = "";
            registerPasswordInput.text = "";
            registerConfirmPasswordInput.text = "";
            dateOfBirthInput.text = "";
            usernameErrorText.text = "";
            registerButtonText.text = "Register";
            registerButton.interactable = true;
            LoginPanel.SetActive(true);
            RegistrationPanel.SetActive(false);
        }
        else
        {
            registerButton.interactable = true;
            registerButtonText.text = "Register";
            if (request.downloadHandler.text.Contains("username"))
            {
                usernameErrorText.text = "Username already exists";
            }      
            else
            {
                regErrorText.text = "Server Error";
            }
        }
        yield return null;
    }
}
