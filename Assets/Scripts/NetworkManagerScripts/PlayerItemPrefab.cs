using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class for Player Item prefab
/// </summary>
public class PlayerItemPrefab : MonoBehaviourPunCallbacks
{
    Player player;
    public Text playerName;
    public Text hostText;
    public Button kickButton;

    /// <summary>
    /// Function to set player information
    /// </summary>
    /// <param name="_player">player information</param>
    public void SetPlayerInfo(Player _player)
    {
        if (_player.IsMasterClient)
        {
            hostText.gameObject.SetActive(true);
        }

        playerName.text = _player.NickName;

        player = _player;
        if (player.IsLocal)
        {
            playerName.color = Color.gray;
        }
        if (player.IsLocal || player.IsMasterClient)
        {
            kickButton.gameObject.SetActive(false);
        }
        else if(PhotonNetwork.IsMasterClient)
        {
            kickButton.gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// Callback function when the player left the room
    /// </summary>
    /// <param name="otherPlayer">player left</param>
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        otherPlayer.SetCustomProperties(null);
        if (player == otherPlayer)
        {          
            hostText.gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }

    /// <summary>
    /// Callback function when switching master client
    /// </summary>
    /// <param name="newMasterClient">new host player</param>
    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        if (player.IsMasterClient)
        {
            hostText.gameObject.SetActive(true);
        }
        if (player.IsLocal || player.IsMasterClient)
        {
            kickButton.gameObject.SetActive(false);
        }
        else if (PhotonNetwork.IsMasterClient)
        {
            kickButton.gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// Callback function when the player left the room
    /// </summary>
    public override void OnLeftRoom()
    {
        player.SetCustomProperties(null);
        Destroy(gameObject);
    }

    /// <summary>
    /// On Click Event function to kick a player
    /// </summary>
    public void OnClickKickPlayer()
    {
        PhotonNetwork.CloseConnection(player);
    }
}
