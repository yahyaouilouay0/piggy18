using System;
using UnityEngine;

public class AddFriend : MonoBehaviour
{
    [SerializeField] private string friendName;
    public static Action<string> OnAddFriend = delegate { };

    public void SetAddFriendName(string name)
    {
        friendName = name;
    }

    public void OnClickAddFriend()
    {
        if (string.IsNullOrEmpty(friendName)) return;
        OnAddFriend?.Invoke(friendName);
    }
}
