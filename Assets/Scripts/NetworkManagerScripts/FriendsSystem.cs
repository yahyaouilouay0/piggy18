using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendsSystem : MonoBehaviour
{
    public GameObject FriendsPanel;
    public Animator animator;
    private bool isClosed;

    public void OnClickCloseAndOpenFriendsPanel()
    {
        if (isClosed)
        {
            isClosed = false;
            animator.SetTrigger("Open");
        }     
        else
        {
            isClosed = true;
            animator.SetTrigger("Close");
        }

    }

}
