using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerIneraction : MonoBehaviour
{
    [SerializeField]
    private KeyCode interact = KeyCode.F;
    private GameObject interactableObject;

    public Camera mainCamera;
    public float interactionDistance = 4f;
    Transform canvas;

    private void Start()
    {
        canvas = GameObject.Find("Canvas").transform;
    }

    private void Update()
    {
        Interaction();
    }

    void Interaction ()
    {
        if(mainCamera!=null)
        {
            Ray ray = mainCamera.ViewportPointToRay(Vector3.one / 2f);
            RaycastHit hit;

            bool hitAnInteractable = false;

            if (Physics.Raycast(ray, out hit, interactionDistance))
            {
                IInteractable interactable = hit.collider.GetComponent<IInteractable>();

                if (interactable != null)
                {
                    hitAnInteractable = true;

                    if (Input.GetKeyDown(interact))
                    {
                        interactable.Interact();
                    }  
                }  
            }
            if(transform.childCount > 2)
            {
                
                    if (canvas.childCount>10)
                            canvas.GetChild(11).gameObject.SetActive(hitAnInteractable);
            }
            
        }
    }
}
