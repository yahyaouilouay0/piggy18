using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Rock : MonoBehaviour
{
    public GameObject sword;
    public int timesMined;
    private bool pickaxeCharged;
    private bool rockDestroyed;
    private bool rockRemoved;

    PhotonView view;

    private void Awake()
    {
        view = GetComponent<PhotonView>();
        rockRemoved = false;
        rockDestroyed = false;
        pickaxeCharged = true;
        timesMined = 0;
        sword = GameObject.Find("Sword");
    }

    private void Update()
    {
        if (timesMined > 3)
        {    
            rockDestroyed = true;
        }
        
        if(rockDestroyed && !rockRemoved)
        {
            Debug.Log("entered");
            view.RPC("RemoveRock", RpcTarget.All);
            Debug.Log("outed");
        }
        else
            sword.transform.SetPositionAndRotation(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 1.5f, gameObject.transform.position.z), Quaternion.Euler(new Vector3(310f, 345f, 10f)));

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Sword")
            Physics.IgnoreCollision(sword.GetComponent<Collider>(), gameObject.GetComponent<Collider>());

        //if (other.gameObject.name == "Pickaxe" && pickaxeCharged)
        //{
        //    timesMined++;
        //    pickaxeCharged = false;
        //}
    }


    [PunRPC]
    public void RemoveRock()
    {
        Debug.Log("problem 1");
        gameObject.SetActive(false);
        Debug.Log("problem 2");
        rockRemoved = true;
        sword.transform.position = transform.position;
    }

    //private void OnTriggerExit(Collider other)
    //{
    //    if (other.gameObject.name == "Pickaxe" && !pickaxeCharged)
    //        pickaxeCharged = true;
    //}

}
