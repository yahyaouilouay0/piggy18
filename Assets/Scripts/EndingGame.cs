using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndingGame : MonoBehaviour
{
    public void OnClickContinue()
    {
        SceneManager.LoadScene("LobbyScene");
    }
}
