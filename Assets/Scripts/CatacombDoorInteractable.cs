using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class CatacombDoorInteractable : MonoBehaviour,IInteractable
{
    public bool keyInLock;
    public bool keyTurned;
    public bool doorUnlocked;
    public GameObject key;
    public float doorSpeed = 0.2f;
    
    
    private Vector3 initalDoorPos;
    private float timeElapsed=0f;
    private readonly float lerpDuration=1f;
    private readonly float startValue=-90f;
    private readonly float endValue=90f;
    private float lerpResult=-90f;
    private bool interactionOn;
    private bool doorOpened;

    PhotonView view;

    private void Awake()
    {
        view = GetComponent<PhotonView>();
        doorOpened = false;
        interactionOn = false;
        keyInLock = false;
        doorUnlocked = false;
        keyTurned = false;
        initalDoorPos = transform.parent.transform.GetChild(1).position;
    }

    private void Update()
    {
        if (keyInLock)
        {
            if(keyTurned)
                LerpKeyRotation();

            //key.transform.SetPositionAndRotation(new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z-0.1f), Quaternion.Euler(new Vector3(0f, lerpResult, 180f)));
            key.transform.localPosition = new Vector3(gameObject.transform.position.x - 0.05f, gameObject.transform.position.y - 0.05f, gameObject.transform.position.z + 0.02f);
            key.transform.localRotation = Quaternion.Euler(new Vector3(180f, 90f, lerpResult));
            key.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition;
            
            if (interactionOn)
            {
                if (!doorUnlocked)
                {
                    keyTurned = true;
                    key.tag = "usedItem";
                    Invoke("OpenDoor", 2.5f);
                }
            }

            if(doorUnlocked && !doorOpened)
                view.RPC("OpenDoorRPC", RpcTarget.All);

        }


    }

    [PunRPC]
    public void OpenDoorRPC()
    {
        doorOpened = true;
        transform.parent.GetChild(0).GetComponent<Animator>().SetTrigger("doorUnlocked");
        gameObject.SetActive(false);
        key.SetActive(false);
    }

    void LerpKeyRotation()
    {
        if (timeElapsed < lerpDuration)
        {
            lerpResult = Mathf.Lerp(startValue, endValue, timeElapsed / lerpDuration);
            timeElapsed += Time.deltaTime;
        }
        else
        {
            lerpResult = endValue;
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Key")
        {
            keyInLock = true;
            key=other.transform.gameObject;
        }
            

    }

    void OpenDoor()
    {
        doorUnlocked = true;
    }
    public void Interact()
    {
        interactionOn = true;
    }
}
