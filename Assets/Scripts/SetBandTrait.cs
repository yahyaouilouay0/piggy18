using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class SetBandTrait : MonoBehaviour,IInteractable
{
    public float bandNumberSet;
    public Color bandColorSet;
    public PhotonView view;

    private GameObject pointer;
    private bool setBandSetting;
    private void Start()
    {
        view = GetComponent<PhotonView>();
        pointer = GameObject.Find("Pointer");
        setBandSetting = false;
        bandNumberSet = 0;
    }

    private void Update()
    {
        if(bandColorSet!=null)
        {
            GetComponent<Renderer>().material.color = bandColorSet;

        }
            

        if (setBandSetting)
        {
            view.RPC("SettingBand", RpcTarget.All);
            setBandSetting = false;
        }

    }

    public void Interact()
    {
        setBandSetting = true;
        pointer = GameObject.Find("Pointer");
        Debug.Log("Hit the button");
    }

    [PunRPC]
    public void SettingBand()
    {
        if (pointer != null)
        {
            string bandName = pointer.GetComponent<PointerPosition>().currentBlock;
            if (GameObject.Find(bandName) != null && bandName !="None")
            {
                GameObject.Find(bandName).GetComponent<BandSetting>().bandColor = bandColorSet;
                GameObject.Find(bandName).GetComponent<BandSetting>().bandPosition = bandNumberSet;
                GameObject.Find(bandName).GetComponent<BandSetting>().setBand = true;
                bandNumberSet++;
            }
        }
    }
}
