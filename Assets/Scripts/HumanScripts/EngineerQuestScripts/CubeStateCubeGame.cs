using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class CubeStateCubeGame : MonoBehaviour
{
    //public int indexI;
    //public int indexJ;
    public GameObject linkedCube;

    public Material waitingMaterial;
    public Material validMaterial;
    public Material shootMaterial;
    public Material defaultMaterial;
    public float dedicatedTime = 20;
    //[HideInInspector]
    public bool isWaiting =false;
    //[HideInInspector]
    public bool isValid =false;
    [HideInInspector]
    public bool isShootable = true;
    [HideInInspector]
    public float timer = 0;
    CubeGame cubeGame;
    bool updatedMaterial = false;

    PhotonView view;

    private void Awake()
    {
        view = GetComponent<PhotonView>();
    }
    // Start is called before the first frame update
    void Start()
    {
        cubeGame = transform.parent.parent.parent.GetComponent<CubeGame>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (view.IsMine)
        //{
        //    if (isWaiting)
        //    {
        //        timer += Time.deltaTime;
        //        if (timer >= dedicatedTime)
        //        {
        //            view.RPC("RPC_ResetCubeTimer", RpcTarget.All);
        //        }

        //    }
        //    if (isShootable && !isWaiting && !isValid)
        //    {
        //        //view.RPC("RPC_UpdateShootMaterial", RpcTarget.All);
        //        //if (!updatedMaterial)
        //        //{

        //        //}
        //    }
        //}

    }

    private void OnCollisionEnter(Collision collision)
    {
        if(view.IsMine)
        {
            if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 1 ||
            (int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 2)
            {
                if (isShootable)
                {
                    if (collision.gameObject.name.ToUpper().Contains("BALL1"))
                    {
                        if (!isWaiting && !isValid)
                        {
                            view.RPC("RPC_startCubeTimer", RpcTarget.All);
                        }
                    }

                    if (collision.gameObject.name.ToUpper().Contains("BALL2"))
                    {
                        if (isWaiting && !isValid)
                        {
                            view.RPC("RPC_EndCubeTimer", RpcTarget.All);
                        }
                    }
                }
            }
        }
    }

    [PunRPC]
    public void RPC_startCubeTimer()
    {
        isWaiting = true;
        linkedCube.GetComponent<CubeStateCubeGame>().isWaiting = true;
        linkedCube.GetComponent<CubeStateCubeGame>().isShootable = true;
        GetComponent<Renderer>().material = waitingMaterial;
        linkedCube.GetComponent<Renderer>().material = waitingMaterial;
        timer = 0;
        cubeGame.cubeIsActive = true;
    }

    [PunRPC]
    public void RPC_EndCubeTimer()
    {
        isValid = true;
        isWaiting = false;
        isShootable = false;
        linkedCube.GetComponent<CubeStateCubeGame>().isShootable = false;
        linkedCube.GetComponent<CubeStateCubeGame>().isWaiting = false;
        linkedCube.GetComponent<CubeStateCubeGame>().isValid = true;
        GetComponent<Renderer>().material = validMaterial;
        linkedCube.GetComponent<Renderer>().material = validMaterial;
        cubeGame.player1Confirmed = true;
        cubeGame.player2Confirmed = true;
        cubeGame.cubeIsActive = false;
        cubeGame.counter++;
    }

    [PunRPC]
    public void RPC_ResetCubeTimer()
    {
        isValid = false;
        isWaiting = false;
        isShootable = true;
        linkedCube.GetComponent<CubeStateCubeGame>().isShootable = true;
        linkedCube.GetComponent<CubeStateCubeGame>().isValid = false;
        linkedCube.GetComponent<CubeStateCubeGame>().isWaiting = false;
        GetComponent<Renderer>().material = defaultMaterial;
        linkedCube.GetComponent<Renderer>().material = defaultMaterial;
        timer = 0;
        cubeGame.cubeIsActive = false;
    }

    [PunRPC]
    public void RPC_UpdateShootMaterial()
    {
        GetComponent<Renderer>().material = shootMaterial;
    }
}


//bool canShoot = false;
//for (int k = 0; k < collision.contacts.Length; k++)
//{
//    if (Vector3.Angle(collision.contacts[k].normal, Vector3.forward) <= 30)
//    {
//        canShoot = true;
//        break;
//    }
//}
//if (canShoot)
//{

//}
