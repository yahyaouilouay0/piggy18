using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BallTransformPointReset : MonoBehaviour
{
    public Transform ballSpawnPoint;
    PhotonView view;
    // Start is called before the first frame update
    void Start()
    {
        view = GetComponent<PhotonView>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.name.ToUpper().Contains("BALL"))
        {
            if (other.GetComponent<Pickup>().picked == false)
            {
                other.transform.position = ballSpawnPoint.transform.position + new Vector3(0, 1);
                view.RPC("RPC_ResetBallTransform", RpcTarget.All, other.GetComponent<PhotonView>().ViewID);
            }

        }
    }

    [PunRPC]
    public void RPC_ResetBallTransform(int viewID)
    {
        PhotonNetwork.GetPhotonView(viewID).transform.position = ballSpawnPoint.transform.position + new Vector3(0, 1);
        PhotonNetwork.GetPhotonView(viewID).GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
    }
}