using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class CubeGame : MonoBehaviour
{
    public int height=5;
    public int width=5;
    public Material defaultMaterial;
    public Material shootMaterial;
    public float dedicatedTime=20;

    [HideInInspector]
    public int counter=0;
    [HideInInspector]
    public bool player1Confirmed,player2Confirmed,cubeIsActive = false;

    Transform[,] firstCubeMat;
    Transform[,] secondCubeMat;
    float shootTimer=0;
    bool startShootTimer=false;
    bool startGame=false;
    System.Random rng;

    PhotonView view;
    private bool questCompleted = false;

    private void Awake()
    {
        view = GetComponent<PhotonView>();
    }
    // Start is called before the first frame update
    void Start()
    {
        firstCubeMat = new Transform[height, width];
        secondCubeMat = new Transform[height, width];
        rng = new System.Random();

    }

    // Update is called once per frame
    void Update()
    {
        if(view.IsMine)
        {
            if (player1Confirmed && player2Confirmed && !startGame)
            {
                view.RPC("RPC_InitiateCubes", RpcTarget.All);

            }
            if (startGame)
            {
                if (!cubeIsActive)
                {
                    int i, j;
                    do
                    {
                        i= rng.Next(0, height);
                        j = rng.Next(0, width);
                    } while (firstCubeMat[i, j].GetComponent<CubeStateCubeGame>().isValid
                                && counter<height*width-1);
                    Debug.Log(i+","+ j);
                    view.RPC("RPC_CubeGameActivateCube", RpcTarget.All,i,j);

                }
                if (startShootTimer)
                {
                    shootTimer += Time.deltaTime;
                    //view.RPC("RPC_CubeGameStartTimer", RpcTarget.All);

                }
                if (shootTimer >= dedicatedTime)
                {
                    shootTimer = 0;
                    startShootTimer = false;
                    view.RPC("RPC_CubeGameEndTimer", RpcTarget.All);

                }
            }
            if (counter >= 22 && !questCompleted)
            {
                view.RPC("RPC_EndCubeGame", RpcTarget.All);
            }
        }
        
    }

    [PunRPC]
    public void RPC_EndCubeGame()
    {
        questCompleted = true;
        GameObject.Find("GameStateChecker").GetComponent<GameStateChecker>().questCounter += 1;
    }

    [PunRPC]
    public void RPC_InitiateCubes()
    {
        Transform FirstCubes = transform.GetChild(0).GetChild(1);
        Transform SecondCubes = transform.GetChild(1).GetChild(1);
        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                firstCubeMat[i, j] = FirstCubes.GetChild((height * i) + j);
                firstCubeMat[i, j].gameObject.SetActive(true);
                secondCubeMat[i, j] = SecondCubes.GetChild((height * i) + j);
                secondCubeMat[i, j].gameObject.SetActive(true);
                CubeStateCubeGame FirstCubeState = FirstCubes.GetChild((height * i) + j).GetComponent<CubeStateCubeGame>();
                CubeStateCubeGame SecondCubeState = SecondCubes.GetChild((width * i) + j).GetComponent<CubeStateCubeGame>();
                FirstCubeState.linkedCube = SecondCubeState.gameObject;
                FirstCubeState.isShootable = false;
                FirstCubeState.timer = 0;
                FirstCubeState.GetComponent<Renderer>().material = FirstCubeState.defaultMaterial;

                SecondCubeState.linkedCube = FirstCubeState.gameObject;
                SecondCubeState.isShootable = false;
                SecondCubeState.timer = 0;
                SecondCubeState.GetComponent<Renderer>().material = FirstCubeState.defaultMaterial;
            }
        }
        firstCubeMat[2, 2].gameObject.GetComponent<Renderer>().material = defaultMaterial;
        secondCubeMat[2, 2].gameObject.GetComponent<Renderer>().material = defaultMaterial;
        player1Confirmed = false;
        player2Confirmed = false;
        startGame = true;
        //if (view.IsMine && ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 1 ||
        //    (int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 2))
        //{
            
        //    Debug.Log("cubeIsActive " + cubeIsActive);
        //}
        


    }


    [PunRPC]
    public void RPC_CubeGameActivateCube(int i,int j)
    {
        //Debug.Log("i " + i);
        //Debug.Log("j " + j);
        cubeIsActive = true;
        firstCubeMat[i, j].GetComponent<CubeStateCubeGame>().isShootable = true;
        firstCubeMat[i, j].GetComponent<Renderer>().material = shootMaterial;
        shootTimer = 0;
        startShootTimer = true;

    }

    //[PunRPC]
    //public void RPC_CubeGameStartTimer()
    //{
    //    shootTimer += Time.deltaTime;
    //}

    [PunRPC]
    public void RPC_CubeGameEndTimer()
    {
        cubeIsActive = false;
        shootTimer = 0;
        startShootTimer = false;
        if (view.IsMine)
        {
            if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 1 ||
            (int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 2)
            {

                view.RPC("RPC_InitiateCubes", RpcTarget.All);
            }
        }
        
    }
}
