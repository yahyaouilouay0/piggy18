using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayCastReflection : MonoBehaviour
{
    public int reflections;
    public float maxLength;
    public Material material;
    public Material validMaterial;
    public GameObject endPoint;

    private LineRenderer lineRenderer;
    private Ray ray;
    private RaycastHit hit;
    private Vector3 direction;
    private bool questCompleted=false;
    PhotonView view;


    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.material = material;
        lineRenderer.startColor = Color.blue;
        lineRenderer.endColor = Color.blue;

    }
    // Start is called before the first frame update
    void Start()
    {
        view = GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        ray = new Ray(transform.position, transform.forward);

        lineRenderer.positionCount = 1;
        lineRenderer.SetPosition(0, transform.position);
        
        float remainingLength = maxLength;
        for (int i = 0; i < reflections; i++)
        {
            if (Physics.Raycast(ray.origin, ray.direction, out hit, remainingLength,7))
            {
                if(hit.transform.name.ToUpper().Contains("ENDLASERGAME") && !questCompleted)
                {
                    view.RPC("RPC_EndLaserGame", RpcTarget.All);
                }
                lineRenderer.positionCount +=1;
                lineRenderer.SetPosition(lineRenderer.positionCount - 1, hit.point);
                remainingLength-= Vector3.Distance(ray.origin, hit.point);
                ray = new Ray(hit.point, Vector3.Reflect(ray.direction, hit.normal));
                if (hit.collider.tag != "Mirror")
                    break;
            }
            else
            {
                lineRenderer.positionCount +=1;
                lineRenderer.SetPosition(lineRenderer.positionCount - 1, ray.origin + ray.direction * remainingLength);
            }
        }   
    }

    [PunRPC]
    public void RPC_EndLaserGame()
    {
        questCompleted = true;
        endPoint.GetComponent<Renderer>().material = validMaterial;
        endPoint.GetComponent<Light>().intensity = 1000;
        GameObject.Find("GameStateChecker").GetComponent<GameStateChecker>().questCounter += 1;
    }
}
