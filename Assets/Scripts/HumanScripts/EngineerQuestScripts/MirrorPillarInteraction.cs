using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class MirrorPillarInteraction : MonoBehaviour
{

    bool canRotateMirrorPillar = false;
    PhotonView view;
    public float speed = 20f;
    

    private void Awake()
    {
        view = GetComponent<PhotonView>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.F) &&
                canRotateMirrorPillar &&
                ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 1 ||
                 (int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 2 ||
                 (int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 3))
        {
            //Envoyer un RPC � tous les joueurs pour modifiers l'�tat
            //des pierres et lancer l'animation
            view.RPC("rotateMirrorHorizentalLeft", RpcTarget.All);
        }

        if (Input.GetKey(KeyCode.R) &&
                canRotateMirrorPillar &&
                ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 1 ||
                 (int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 2 ||
                 (int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 3))
        {
            //Envoyer un RPC � tous les joueurs pour modifiers l'�tat
            //des pierres et lancer l'animation
            view.RPC("rotateMirrorHorizentalRight", RpcTarget.All);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Si un Humain est entr� dans la zone de collision
        if (other.gameObject.name.ToUpper().Contains("MEDIC") ||
            other.gameObject.name.ToUpper().Contains("ATHELTE") ||
            other.gameObject.name.ToUpper().Contains("ENGINEER"))
        {
            //alors il peut tourner les pilliers
            canRotateMirrorPillar = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Si un Humain sort de la zone de collision
        if (other.gameObject.name.ToUpper().Contains("MEDIC") ||
            other.gameObject.name.ToUpper().Contains("ATHELTE") ||
            other.gameObject.name.ToUpper().Contains("ENGINEER"))
        {
            //alors il ne peut plus activer les runes
            canRotateMirrorPillar = false;
        }
    }

    [PunRPC]
    public void rotateMirrorHorizentalLeft()
    {
        transform.GetChild(1).Rotate(Vector3.up * speed * Time.deltaTime,Space.World);
    }

    [PunRPC]
    public void rotateMirrorHorizentalRight()
    {
        transform.GetChild(1).Rotate(-Vector3.up * speed * Time.deltaTime, Space.World);
    }

    //[PunRPC]
    //public void RPC_DisableMirrorStick(int MirrorStickViewID)
    //{
    //    Debug.Log("RPC Recu ");
    //    PhotonView MirrorStickView = PhotonNetwork.GetPhotonView(MirrorStickViewID);
    //    MirrorStickView.gameObject.SetActive(false);
    //    Debug.Log("RPC" + MirrorStickView.name.ToUpper());
    //}
}
