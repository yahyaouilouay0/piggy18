using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Cette classe est utilis� pour la qu�te secondaire de l'athl�te
/// o� il doit ramasser 3 pierres d�pos� al�toirement dans la carte
/// puis les mettres � cot� de la principal et ainsi pouvoir les activer
/// une fois activer une maquette du labyrinthe apparaitra
/// </summary>
public class MagicRockActivation : MonoBehaviour
{
    //Particle system de l'effect magique
    public ParticleSystem activcationEffect;

    bool canActiviateMagicRock = false;
    bool isActiveMagicRock = false;
    PhotonView view;

    //D�clarer les 3 pierrs � trouver
    Transform rock3;
    Transform rock4;
    Transform rock5;

    //Savoir si les pierres ont �t� plac�
    bool rock3isPlaced = false;
    bool rock4isPlaced = false;
    bool rock5isPlaced = false;

    //Savoir si les pierres peuvent �tre plac�
    bool rock3CanBePlaced = false;
    bool rock4CanBePlaced = false;
    bool rock5CanBePlaced = false;

    //L'emplacement de base des 3 pierres
    Transform rock3Transform;
    Transform rock4Transform;
    Transform rock5Transform;

    //L'animateur des 3 pierres
    Animator rock3Animator;
    Animator rock4Animator;
    Animator rock5Animator;

    //Les parents des spawn point pour la distribution al�toire des pierres
    Transform rock3SpawnPoint;
    Transform rock4SpawnPoint;
    Transform rock5SpawnPoint;

    Renderer rock1Renderer;

    private void Awake()
    {
        //R�cup�rer les 3 pierres
        rock3 = transform.GetChild(1);
        rock4 = transform.GetChild(2);
        rock5 = transform.GetChild(3);

        //R�cup�rer les positions d'origines des 3 pierres
        rock3Transform = transform.GetChild(4);
        rock4Transform = transform.GetChild(5);
        rock5Transform = transform.GetChild(6);

        //R�cup�rer les spawn point o� peuvent apparaitres les pierres
        rock3SpawnPoint = transform.GetChild(7);
        rock4SpawnPoint = transform.GetChild(8);
        rock5SpawnPoint = transform.GetChild(9);

        //R�cup�rer le renderer de la pierre principale 
        rock1Renderer = transform.GetChild(0).GetComponent<Renderer>();

        //R�cup�rer la PhotonView du GO
        view = GetComponent<PhotonView>();

        //Arr�ter l'effet magique au lancement
        activcationEffect.Stop();

    }

    // Start is called before the first frame update
    void Start()
    {
        //R�cup�rer les animateur des 3 pierres
        rock3Animator = rock3.GetComponent<Animator>();
        rock4Animator = rock4.GetComponent<Animator>();
        rock5Animator = rock5.GetComponent<Animator>();

        //Pour pouvoir g�n�rer les m�mes chiffres al�toires pour chaque
        //client on utilise view.IsMine
        if (view.IsMine)
        {
            //Distribuer les pierres al�toirement dans la carte
            view.RPC("PlaceRocksRandom", RpcTarget.All,
            Random.Range(1, rock3SpawnPoint.childCount),
            Random.Range(1, rock4SpawnPoint.childCount),
            Random.Range(1, rock5SpawnPoint.childCount));

        }
    }

    // Update is called once per frame
    void Update()
    {
        //Si les pierres n'ont pas �t� toute plac�
        if (!(rock3isPlaced && rock4isPlaced && rock5isPlaced))
        {
            //Si je suis dans la mesure d'activer les pierres (dans la Range)
            if (canActiviateMagicRock)
            {

                if (rock3CanBePlaced)
                {
                    //Placer la 3�me pierre
                    view.RPC("PlaceRock3", RpcTarget.All);
                }

                if (rock4CanBePlaced)
                {
                    //Placer la 4�me pierre
                    view.RPC("PlaceRock4", RpcTarget.All);
                }

                if (rock5CanBePlaced)
                {
                    //Placer la 5�me pierre
                    view.RPC("PlaceRock5", RpcTarget.All);
                }
            }
        }

        //V�rifier si les 3 pierres ont �t� placer
        if (rock3isPlaced && rock4isPlaced && rock5isPlaced)
        {
            //V�rifier si un Humain est � cot� des rochers 
            // et qu'il a appuiy� sur le bouton F
            if (Input.GetKeyDown(KeyCode.F) &&
                canActiviateMagicRock &&
                ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 1 ||
                 (int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 2 ||
                 (int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 3))
            {
                //Envoyer un RPC � tous les joueurs pour modifiers l'�tat
                //des pierres et lancer l'animation
                view.RPC("ActivateMagicRocks", RpcTarget.All);
            }

            //Si les pierres sont actives 
            if (isActiveMagicRock)
            {
                //alors lancer l'effet de pulsation
                float glow = Mathf.PingPong(Time.time, 2.0f);
                rock1Renderer.material.SetFloat("Vector1_6a347c3b4b5b4fea90a98e040bf2b9ac", glow);
            }
        }


    }

    private void OnTriggerEnter(Collider other)
    {
        //Si un Humain est entr� dans la zone de collision
        if (other.gameObject.name.ToUpper().Contains("MEDIC") ||
            other.gameObject.name.ToUpper().Contains("ATHELTE") ||
            other.gameObject.name.ToUpper().Contains("ENGINEER"))
        {
            //alors il peut activer les runes
            canActiviateMagicRock = true;
        }

        //V�rifier si un item est entr� dans la zone de collision
        if (other.CompareTag("item"))
        {
            //Si item est pas dans les mains du joueurs
            if (!other.GetComponent<Pickup>().picked)
            {
                //Si l'item en question est la pierre 3
                if (other.name.Contains("StylRocksMagic_3_LOD2"))
                    //alors elle peut �tre placer
                    rock3CanBePlaced = true;
                //Si l'item en question est la pierre 4
                if (other.name.Contains("StylRocksMagic_4_LOD2"))
                    //alors elle peut �tre placer
                    rock4CanBePlaced = true;
                //Si l'item en question est la pierre 5
                if (other.name.Contains("StylRocksMagic_5_LOD2"))
                    //alors elle peut �tre placer
                    rock5CanBePlaced = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Si un Humain sort de la zone de collision
        if (other.gameObject.name.ToUpper().Contains("MEDIC") ||
            other.gameObject.name.ToUpper().Contains("ATHELTE") ||
            other.gameObject.name.ToUpper().Contains("ENGINEER"))
        {
            //alors il ne peut plus activer les runes
            canActiviateMagicRock = false;
        }

        //V�rifier si un item est entr� dans la zone de collision
        if (other.CompareTag("item"))
        {
            //Si item est pas dans les mains du joueurs
            if (!other.GetComponent<Pickup>().picked)
            {
                //Si l'item en question est la pierre 3
                if (other.name.Contains("StylRocksMagic_3_LOD2"))
                    //alors elle ne peut plus �tre placer
                    rock3CanBePlaced = false;
                //Si l'item en question est la pierre 4
                if (other.name.Contains("StylRocksMagic_4_LOD2"))
                    //alors elle ne peut plus �tre placer
                    rock4CanBePlaced = false;
                //Si l'item en question est la pierre 3
                if (other.name.Contains("StylRocksMagic_5_LOD2"))
                    //alors elle ne peut plus �tre placer
                    rock5CanBePlaced = false;
            }
        }
    }

    [PunRPC]
    public void ActivateMagicRocks()
    {
        //Activer l'animateur des 3 pierre
        rock3Animator.enabled = true;
        rock4Animator.enabled = true;
        rock5Animator.enabled = true;

        //Lancer l'animation (pierre qui tourne)
        rock3Animator.SetBool("canMove", true);
        rock4Animator.SetBool("canMove", true);
        rock5Animator.SetBool("canMove", true);

        //Mettre l'�tat de la pierre � active
        isActiveMagicRock = true;
        //Lancer l'effet magique
        activcationEffect.Play();
        //Faire "spawn" la maquette en la rendant visible
        transform.GetChild(transform.childCount - 4).gameObject.SetActive(true);
    }

    [PunRPC]
    public void PlaceRocksRandom(int randomChildofSpawnPoint3, int randomChildofSpawnPoint4, int randomChildofSpawnPoint5)
    {
        //Assigner la pierre � l'un des fils du spawn point al�atoirement
        rock3.SetParent(rock3SpawnPoint.GetChild(randomChildofSpawnPoint3 - 1));
        //Mettre la pierre � la position 0 0 0 pour qu'elle soit la m�me du parent
        rock3.transform.localPosition = new Vector3(0, 0, 0);
        //Mettre la rotation de la pierre comme celle du parent
        rock3.transform.rotation = rock3SpawnPoint.GetChild(randomChildofSpawnPoint3).rotation;

        //Assigner la pierre � l'un des fils du spawn point al�atoirement
        rock4.SetParent(rock4SpawnPoint.GetChild(randomChildofSpawnPoint4 - 1));
        //Mettre la pierre � la position 0 0 0 pour qu'elle soit la m�me du parent
        rock4.transform.localPosition = new Vector3(0, 0, 0);
        //Mettre la rotation de la pierre comme celle du parent
        rock4.transform.rotation = rock4SpawnPoint.GetChild(randomChildofSpawnPoint4).rotation;

        //Assigner la pierre � l'un des fils du spawn point al�atoirement
        rock5.SetParent(rock5SpawnPoint.GetChild(randomChildofSpawnPoint5 - 1));
        //Mettre la pierre � la position 0 0 0 pour qu'elle soit la m�me du parent
        rock5.transform.localPosition = new Vector3(0, 0, 0);
        //Mettre la rotation de la pierre comme celle du parent
        rock5.transform.rotation = rock5SpawnPoint.GetChild(randomChildofSpawnPoint5).rotation;
    }

    [PunRPC]
    public void PlaceRock3()
    {
        //Mettre le parent de la pierre 3 au GO qui les r�unit tous
        rock3.SetParent(transform);
        //Mettre la pierre � sa position original
        rock3.localPosition = rock3Transform.localPosition;
        //Mettre la pierre � sa taille original
        rock3.localScale = rock3.GetComponent<Pickup>().objectScale;
        //Mettre la pierre � sa rotation original
        //rock3.rotation = new Quaternion(0, 0, 0, 0);
        rock3.localEulerAngles = new Vector3(0, 0, 0);
        //Mettre l'�tat de la pierre � plac� pour pouvoir les activ�
        rock3isPlaced = true;
        //Mettre l'�tat de la pierre � pris pour ne plus pouvoir la mettre dans son inv
        rock3.GetComponent<Pickup>().picked = true;
        rock3CanBePlaced = false;
        rock3.GetComponent<Rigidbody>().isKinematic = true;
    }

    [PunRPC]
    public void PlaceRock4()
    {
        //Mettre le parent de la pierre 4 au GO qui les r�unit tous
        rock4.SetParent(transform);
        //Mettre la pierre � sa position original
        rock4.localPosition = rock4Transform.localPosition;
        //Mettre la pierre � sa taille original
        rock4.localScale = rock4.GetComponent<Pickup>().objectScale;
        //Mettre la pierre � sa rotation original
        //rock4.rotation = new Quaternion(0, 0, 0, 0);
        rock4.localEulerAngles = new Vector3(0, 0, 0);
        //Mettre l'�tat de la pierre � plac� pour pouvoir les activ�
        rock4isPlaced = true;
        //Mettre l'�tat de la pierre � pris pour ne plus pouvoir la mettre dans son inv
        rock4.GetComponent<Pickup>().picked = true;
        rock4CanBePlaced = false;
        rock4.GetComponent<Rigidbody>().isKinematic = true;
    }

    [PunRPC]
    public void PlaceRock5()
    {
        //Mettre le parent de la pierre 5 au GO qui les r�unit tous
        rock5.SetParent(transform);
        //Mettre la pierre � sa position original
        rock5.localPosition = rock5Transform.localPosition;
        //Mettre la pierre � sa taille original
        rock5.localScale = rock5.GetComponent<Pickup>().objectScale;
        //Mettre la pierre � sa rotation original
        //rock5.rotation = new Quaternion(0, 0, 0, 0);
        rock5.localEulerAngles = new Vector3(0, 0, 0);
        //Mettre l'�tat de la pierre � plac� pour pouvoir les activ�
        rock5isPlaced = true;
        //Mettre l'�tat de la pierre � pris pour ne plus pouvoir la mettre dans son inv
        rock5.GetComponent<Pickup>().picked = true;
        rock5CanBePlaced = false;
        rock5.GetComponent<Rigidbody>().isKinematic = true;
    }
}
