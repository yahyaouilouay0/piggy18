using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
/// <summary>
/// Cette classe sert � faire le rendu du labyrinthe
/// Faire spawn les murs selon la matrice g�n�r�
/// </summary>
public class MazeRenderer : MonoBehaviour//, IOnEventCallback
{
    //Largeur du labyrinthe
    [SerializeField]
    [Range(1, 50)]
    private int width = 10;

    //Hauteur du labyrinthe
    [SerializeField]
    [Range(1, 50)]
    private int height = 10;

    //Scale de l'hauteur
    [SerializeField]
    private float heightScale = 4;

    //Scale de la largeur
    [SerializeField]
    private float widthScale = 4;

    //Taille des murs
    //[SerializeField]
    private float size = 1f;

    //L'item qui sera � la fin du labyrinthe
    [SerializeField]
    private Transform itemPrefab = null;

    //La hauteur de la carte 
    [SerializeField]
    private float localHeight = 2.6f;

    //Le GO parent qui va contenir le labyrinthe
    private Transform parent;

    //Le GO parent qui va contenir la r�plique du labyrinthe
    private Transform parentMaquette;

    //la vue du l'objet parent
    private PhotonView view;

    //Le lieu o� se trouve les pierres magique
    private Transform MagicRocks;

    //[SerializeField]
    //private Transform floorPrefab = null;

    private void Awake()
    {
        //R�cup�rer l'objet parent qui va contenir le labyrinthe
        parent = GameObject.Find("MazeRenderer").transform;
        //R�cuperer les pierres magique
        MagicRocks = GameObject.Find("StylRocksMagic_LOD2").transform;
        //R�cup�rer la vue Photon
        view = GetComponent<PhotonView>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //R�cuper le scale de l'objet � rammaser � la fin
        Vector3 itemPrefabScale = itemPrefab.localScale;
        if (view.IsMine)
        {
            //Instancier l'objet qui va servir comme r�plique
            parentMaquette = PhotonNetwork.Instantiate("MazeMaquette", new Vector3(0, 5, 0), Quaternion.identity).transform;
            //G�n�rer un labyrinthe avec le script MazeGenerator
            var maze = MazeGenerator.Generate(width, height);
            //Lancer la m�thode qui permet d'instancier les murs
            Draw(maze);
            //Adapter la taille du labyrinthe � l'environnement
            view.RPC("MazeScaler", RpcTarget.AllBuffered, itemPrefabScale, parentMaquette.GetComponent<PhotonView>().ViewID);
        }

    }

    private void Draw(WallState[,] maze)
    {
        //Parcourir la matrice
        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < height; ++j)
            {
                //Cellule recoit la case actuelle
                var cell = maze[i, j];
                //Affecter la position de la cellule selon l'it�tarion
                var position = new Vector3(-width / 2 + i, 0f, -height / 2 + j);
                //Si la cellule a un mur en haut
                if (cell.HasFlag(WallState.UP))
                {
                    //Instancier un mur en haut dans le labyrinthe
                    var topWall = PhotonNetwork.Instantiate("MazeWall", new Vector3(0, 0, 0), Quaternion.identity).transform;
                    //Instancier un mur en haut dans la ma quette
                    var topWallMaquette = PhotonNetwork.Instantiate("MazeMaquetteWall", new Vector3(0, 0, 0), Quaternion.identity).transform;
                    //Plancer le mur dans le bon endroit
                    view.RPC("topWallSpawn", RpcTarget.AllBuffered,
                        topWall.GetComponent<PhotonView>().ViewID,
                        topWallMaquette.GetComponent<PhotonView>().ViewID,
                        parentMaquette.GetComponent<PhotonView>().ViewID,
                        position);
                }

                //Si la cellule a un mur � gauche
                if (cell.HasFlag(WallState.LEFT))
                {
                    //Instancier un mur � gauche dans le labyrinthe
                    var leftWall = PhotonNetwork.Instantiate("MazeWall", new Vector3(0, 0, 0), Quaternion.identity).transform;
                    //Instancier un mur en haut dans la maquette
                    var leftWallMaquette = PhotonNetwork.Instantiate("MazeMaquetteWall", new Vector3(0, 0, 0), Quaternion.identity).transform;
                    //Plancer le mur dans le bon endroit
                    view.RPC("leftWallSpawn", RpcTarget.AllBuffered,
                        leftWall.GetComponent<PhotonView>().ViewID,
                        leftWallMaquette.GetComponent<PhotonView>().ViewID,
                        parentMaquette.GetComponent<PhotonView>().ViewID,
                        position);
                }

                //Si la cellule est celle de la sortie
                if (cell.HasFlag(WallState.EXIT))
                {
                    //Instancier l'item dans le labyrinthe
                    var item = PhotonNetwork.Instantiate(itemPrefab.name, new Vector3(0, 0, 0), Quaternion.identity).transform;
                    //Instancier la croix dans la maquette
                    var cross = PhotonNetwork.Instantiate("MazeCross", new Vector3(0, 0, 0), Quaternion.identity).transform;
                    //Plancer l'item et la croix dans le bon endroit
                    view.RPC("itemSpawn", RpcTarget.AllBuffered,
                        item.GetComponent<PhotonView>().ViewID,
                        cross.GetComponent<PhotonView>().ViewID,
                        parentMaquette.GetComponent<PhotonView>().ViewID,
                        position);
                }

                //Si j'arrive � la fin de la largeur
                if (i == width - 1)
                {
                    //Si la cellule a un mur � droite
                    if (cell.HasFlag(WallState.RIGHT))
                    {
                        //Instancier un mur � drotie dans le labyrinthe
                        var rightWall = PhotonNetwork.Instantiate("MazeWall", new Vector3(0, 0, 0), Quaternion.identity).transform;
                        //Instancier un mur en � droite dans la maquette
                        var rightWallMaquette = PhotonNetwork.Instantiate("MazeMaquetteWall", new Vector3(0, 0, 0), Quaternion.identity).transform;
                        //Plancer le mur dans le bon endroit
                        view.RPC("rightWallSpawn", RpcTarget.AllBuffered,
                            rightWall.GetComponent<PhotonView>().ViewID,
                            rightWallMaquette.GetComponent<PhotonView>().ViewID,
                            parentMaquette.GetComponent<PhotonView>().ViewID,
                            position);
                    }
                }

                //Si je suis au d�but du labyrinthe
                if (j == 0)
                {
                    //Si la cellule a un mur en bas
                    if (cell.HasFlag(WallState.DOWN))
                    {
                        //Instancier un mur en bas dans le labyrinthe
                        var downWall = PhotonNetwork.Instantiate("MazeWall", new Vector3(0, 0, 0), Quaternion.identity).transform;
                        //Instancier un mur bas dans la maquette
                        var downWallMaquette = PhotonNetwork.Instantiate("MazeMaquetteWall", new Vector3(0, 0, 0), Quaternion.identity).transform;
                        //Plancer le mur dans le bon endroit
                        view.RPC("downWallSpawn", RpcTarget.AllBuffered,
                            downWall.GetComponent<PhotonView>().ViewID,
                            downWallMaquette.GetComponent<PhotonView>().ViewID,
                            parentMaquette.GetComponent<PhotonView>().ViewID,
                            position);

                    }
                }
            }

        }

    }

    [PunRPC]
    void MazeScaler(Vector3 itemPrefabScale, int parentMaquetteViewID)
    {
        //R�cupere la maquette par son photonView
        Transform parentMaquette = PhotonNetwork.GetPhotonView(parentMaquetteViewID).transform;
        //Changer la taille du labyrinthe
        parent.localScale = new Vector3(widthScale, 1, heightScale);
       //Changer la taille de la maquette
        parentMaquette.localScale = new Vector3(0.1f, 0.005f, 0.1f);
        //Affecter la maquette comme fils des pierres magiques
        parentMaquette.SetParent(MagicRocks);
        //Mettre la position de la maquette l� o� il y a l'effet magique
        parentMaquette.localPosition = MagicRocks.GetChild(MagicRocks.childCount - 2).localPosition + new Vector3(0, 0.5f, 0);
        //D�sactiver la maquette
        parentMaquette.gameObject.SetActive(false);
        //Enlever le parent de l'item
        itemPrefab.SetParent(null);
        //Mettre l'item � taille d'origine
        //itemPrefab.localScale = itemPrefabScale;
        itemPrefab.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        //D�sactiver le cr�ateur du labyrinthe dans le GO pour qu'il n'y pas de boucle infini
        gameObject.GetComponent<MazeRenderer>().enabled = false;
    }

    [PunRPC]
    void topWallSpawn(int topWallViewID, int topWallMaquetteViewID, int parentMaquetteViewID, Vector3 position)
    {
        //R�cup�rer le mur du haut
        Transform topWall = PhotonNetwork.GetPhotonView(topWallViewID).transform;
        //Mettre le GO actuel en tant que parent
        topWall.SetParent(parent);
        //Mettre sa position en haut
        topWall.localPosition = position + new Vector3(0, localHeight, size / 2);
        //Adapter sa taille
        topWall.localScale = new Vector3(size, topWall.localScale.y, topWall.localScale.z);

        //Les m�me �tape que le labyrinthe mais pour la maquette
        Transform parentMaquette = PhotonNetwork.GetPhotonView(parentMaquetteViewID).transform;
        Transform topWallMaquette = PhotonNetwork.GetPhotonView(topWallMaquetteViewID).transform;
        topWallMaquette.SetParent(parentMaquette);
        topWallMaquette.localPosition = position + new Vector3(0, localHeight, size / 2);
        topWallMaquette.localScale = new Vector3(size, topWall.localScale.y, topWall.localScale.z);
    }

    [PunRPC]
    void leftWallSpawn(int leftWallViewID, int leftWallMaquetteViewID, int parentMaquetteViewID, Vector3 position)
    {
        //R�cup�rer le mur de gauche
        Transform leftWall = PhotonNetwork.GetPhotonView(leftWallViewID).transform;
        //Mettre le GO actuel en tant que parent
        leftWall.SetParent(parent);
        //Mettre sa position � gauche
        leftWall.localPosition = position + new Vector3(-size / 2, localHeight, 0);
        //Adapter sa taille
        leftWall.localScale = new Vector3(size, leftWall.localScale.y, leftWall.localScale.z);
        //Faire une rotation de 90�
        leftWall.eulerAngles = new Vector3(0, 90, 0);

        //Les m�me �tape que le labyrinthe mais pour la maquette
        Transform parentMaquette = PhotonNetwork.GetPhotonView(parentMaquetteViewID).transform;
        Transform leftWallMaquette = PhotonNetwork.GetPhotonView(leftWallMaquetteViewID).transform;
        leftWallMaquette.SetParent(parentMaquette);
        leftWallMaquette.localPosition = position + new Vector3(-size / 2, localHeight, 0);
        leftWallMaquette.localScale = new Vector3(size, leftWall.localScale.y, leftWall.localScale.z);
        leftWallMaquette.eulerAngles = new Vector3(0, 90, 0);
    }

    [PunRPC]
    void rightWallSpawn(int rightWallViewID, int rightWallMaquetteViewID, int parentMaquetteViewID, Vector3 position)
    {
        //R�cup�rer le mur de droite
        Transform rightWall = PhotonNetwork.GetPhotonView(rightWallViewID).transform;
        //Mettre le GO actuel en tant que parent
        rightWall.SetParent(parent);
        //Mettre sa position � droite
        rightWall.localPosition = position + new Vector3(+size / 2, localHeight, 0);
        //Adapter sa taille
        rightWall.localScale = new Vector3(size, rightWall.localScale.y, rightWall.localScale.z);
        //Faire une rotation de 90�
        rightWall.eulerAngles = new Vector3(0, 90, 0);

        //Les m�me �tape que le labyrinthe mais pour la maquette
        Transform parentMaquette = PhotonNetwork.GetPhotonView(parentMaquetteViewID).transform;
        Transform rightWallMaquette = PhotonNetwork.GetPhotonView(rightWallMaquetteViewID).transform;
        rightWallMaquette.SetParent(parentMaquette);
        rightWallMaquette.localPosition = position + new Vector3(+size / 2, localHeight, 0);
        rightWallMaquette.localScale = new Vector3(size, rightWall.localScale.y, rightWall.localScale.z);
        rightWallMaquette.eulerAngles = new Vector3(0, 90, 0);
    }

    [PunRPC]
    void downWallSpawn(int downWallViewID, int downWallMaquetteViewID, int parentMaquetteViewID, Vector3 position)
    {
        //R�cup�rer le mur du bas
        Transform downWall = PhotonNetwork.GetPhotonView(downWallViewID).transform;
        //Mettre le GO actuel en tant que parent
        downWall.SetParent(parent);
        //Mettre sa position en bas
        downWall.localPosition = position + new Vector3(0, localHeight, -size / 2);
        //Adapter sa taille
        downWall.localScale = new Vector3(size, downWall.localScale.y, downWall.localScale.z);

        //Les m�me �tape que le labyrinthe mais pour la maquette
        Transform parentMaquette = PhotonNetwork.GetPhotonView(parentMaquetteViewID).transform;
        Transform downWallMaqutte = PhotonNetwork.GetPhotonView(downWallMaquetteViewID).transform;
        downWallMaqutte.SetParent(parentMaquette);
        downWallMaqutte.localPosition = position + new Vector3(0, localHeight, -size / 2);
        downWallMaqutte.localScale = new Vector3(size, downWall.localScale.y, downWall.localScale.z);
    }

    [PunRPC]
    void itemSpawn(int itemViewID, int crossViewID, int parentMaquetteViewID, Vector3 position)
    {
        //R�cuper l'item avec sa photon View
        Transform item = PhotonNetwork.GetPhotonView(itemViewID).transform;
        //Mettre le parent de l'item, le GO actuel
        item.SetParent(parent);
        //Mettre la position de l'item au centre
        item.localPosition = position + new Vector3(0, 5, 0);
        //Affecter l'attribut itemPrefab avec l'item
        itemPrefab = item;

        //R�cup�rer la maquette avec son PhotonView
        Transform parentMaquette = PhotonNetwork.GetPhotonView(parentMaquetteViewID).transform;
        //R�cup�rer la croix avec sa photon View
        Transform cross = PhotonNetwork.GetPhotonView(crossViewID).transform;
        //Mettre le parent de la croix, le GO Actuel
        cross.SetParent(parentMaquette);
        //mettre la croix au milieu
        cross.localPosition = position;
        //Faire une rotation de 45� � la croix
        cross.Rotate(new Vector3(0, 45, 0));
    }

}
