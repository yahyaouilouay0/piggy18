using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


//Enum servant � d�finir les murs que poss�de une cellule 
//dans la matrice (labyrinthe)
[Flags]
public enum WallState
{
    // 0000 -> Pas de mur
    // 1111 -> Gauche,droite,haut,bas
    LEFT = 1, // 0001
    RIGHT = 2, // 0010
    UP = 4, // 0100
    DOWN = 8, // 1000
    EXIT=64,
    VISITED = 128, // 1000 0000
}

//Enregistrement servant � enregistrer la position des voisins dans la matrice
public struct Position
{
    public int X;
    public int Y;
}

//Enregistrement pour conna�tre le mur partag� entre les voisins et ainsi
//le supprimer
public struct Neighbour
{
    public Position Position;
    public WallState SharedWall;
}

/// <summary>
/// Cette classe statique va servir � g�n�rer une matrice qui 
/// repr�sentera le labyrinthe
/// </summary>
public static class MazeGenerator
{


    //Remplissage de la matrice et g�n�ration du labyrinthe
    public static WallState[,] Generate(int width, int height)
    {
        //Cr�ation de la matrice avec la longueur et largeur demand�
        WallState[,] maze = new WallState[width, height];
        //Pr�parer la cellule par d�faut (avec tous les murs)
        WallState initial = WallState.RIGHT | WallState.LEFT | WallState.UP | WallState.DOWN;
        //remplissage de la matrice
        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < height; ++j)
            {
                //affecter chaque case par une cellule avec 4 murs
                maze[i, j] = initial;  // 1111
            }
        }

        //Appliquer l'algorithme de backtracking r�cursive sur la matrice initialis�
        return ApplyRecursiveBacktracker(maze, width, height);
    }

    private static WallState[,] ApplyRecursiveBacktracker(WallState[,] maze, int width, int height)
    {
        //Pr�parer le syst�me Random
        var rng = new System.Random(/*seed*/);
        //Une pile des positions des cellule visit�
        var positionStack = new Stack<Position>();
        //Choisir une position de cellule al�toire dans la matrice
        var position = new Position { X = rng.Next(0, width), Y = rng.Next(0, height) };
        
        //Ajouter le flag visit� � la cellule
        maze[position.X, position.Y] |= WallState.VISITED;  // 1000 1111
        //Ajouter la cellule � la pile
        positionStack.Push(position);

        //Tant que la pile n'est pas vide
        while (positionStack.Count > 0)
        {
            //Faire sortir le dernier �lement de la pile
            var current = positionStack.Pop();
            //Chercher les voisins non visit� de la cellule courante
            var neighbours = GetUnvisitedNeighbours(current, maze, width, height);

            //Si j'ai des voisins non visit� de la cellule courante
            if (neighbours.Count > 0)
            {
                //Remettre la cellule courante dans la pile
                positionStack.Push(current);

                //Choisir un voisins au hasard parmis ceux trouve
                var randIndex = rng.Next(0, neighbours.Count);
                //Affecter le voisins � randomNeighbour
                var randomNeighbour = neighbours[randIndex];
                //Affecter la poisition du voisin � nPosition
                var nPosition = randomNeighbour.Position;
                //Enlever le mur qui est entre la cellule courante et le voisin dans 
                //la cellule courante
                maze[current.X, current.Y] &= ~randomNeighbour.SharedWall;
                //Enlever le mur qui est entre la cellule courante et le voisin dans 
                //la cellule du voisin 
                maze[nPosition.X, nPosition.Y] &= ~GetOppositeWall(randomNeighbour.SharedWall);
                //Ajouter l'�tat visit� au voisin
                maze[nPosition.X, nPosition.Y] |= WallState.VISITED;

                //Ajouter le voisin � la pile
                positionStack.Push(nPosition);
            }
        }
        //Liste des mur en largeur
        List<int> widthWalls = new List<int>();
        for (int i = 1; i < width-1; ++i)
        {
            //Si la cellule n'a pas de mur � gauche et � droite
            if (!maze[i, 0].HasFlag(WallState.LEFT) && !maze[i, 0].HasFlag(WallState.RIGHT))
            {
                //ajouter l'indice de la cellule � la liste
                widthWalls.Add(i);
            }
        }
        //Choisir une valeur al�atoire entre 0 et le nombre de cellule possible
        int randWidthWallIndex = rng.Next(0, widthWalls.Count);
        //R�cup�re l'indice de la cellule au hasard
        int randomWidthWall = widthWalls[randWidthWallIndex];
        //Enlever le mur du bas pour que �a sert comme entr�e
        maze[randomWidthWall, 0] &= ~WallState.DOWN;
        //Vider la liste
        widthWalls.Clear();

        //Parcourir la matrice en largeur mais cette fois � la fin
        for (int i = 1; i < width - 1; ++i)
        {
            //Si la cellule n'a pas de mur � droite ou � gauche
            if (!maze[i, height-1].HasFlag(WallState.LEFT) && !maze[i, height-1].HasFlag(WallState.RIGHT))
            {
                //Ajouter l'indice de la cellule  � la liste
                widthWalls.Add(i);
            }
        }
        //Identique � la ligne 124
        randWidthWallIndex = rng.Next(0, widthWalls.Count);
        randomWidthWall = widthWalls[randWidthWallIndex];
        //Ajouter l'�tat de sortie � la cellule
        maze[randomWidthWall, height-1] |= WallState.EXIT;



        return maze;
    }

    private static WallState GetOppositeWall(WallState wall)
    {
        //Selon le mur que j'ai je vais l'inverser
        switch (wall)
        {
            //Retourner mur gauche si on a droite
            case WallState.RIGHT: return WallState.LEFT;
            //Retourner mur droite si on gauche
            case WallState.LEFT: return WallState.RIGHT;
            //Retourner mur bas si on a haut
            case WallState.UP: return WallState.DOWN;
            //Retourner mur haut si on a bas
            case WallState.DOWN: return WallState.UP;
            //Obligatoire mais inutile
            default: return WallState.LEFT;
        }
    }

    private static List<Neighbour> GetUnvisitedNeighbours(Position p, WallState[,] maze, int width, int height)
    {
        //Liste des voisins � la cellule
        var list = new List<Neighbour>();

        //pas besoin de v�rifier si c'est sup � width vu qu'on est dans une matrice
        //V�rifier si j'ai un voisin � gauche
        if (p.X > 0)
        {
            //Si il n'est pas encore visit�
            if (!maze[p.X - 1, p.Y].HasFlag(WallState.VISITED))
            {
                //Ajouter le voisins � la liste
                list.Add(new Neighbour
                {
                    Position = new Position
                    {
                        X = p.X - 1,
                        Y = p.Y
                    },
                    SharedWall = WallState.LEFT
                });
            }
        }

        //V�rifier si j'ai un voisin en bas
        if (p.Y > 0)
        {
            //Si il n'est pas encore visit�
            if (!maze[p.X, p.Y - 1].HasFlag(WallState.VISITED))
            {
                //Ajouter le voisins � la liste
                list.Add(new Neighbour
                {
                    Position = new Position
                    {
                        X = p.X,
                        Y = p.Y - 1
                    },
                    SharedWall = WallState.DOWN
                });
            }
        }

        //V�rifier si j'ai un voisin en haut
        if (p.Y < height - 1) // UP
        {
            //Si il n'est pas encore visit�
            if (!maze[p.X, p.Y + 1].HasFlag(WallState.VISITED))
            {
                //Ajouter le voisins � la liste
                list.Add(new Neighbour
                {
                    Position = new Position
                    {
                        X = p.X,
                        Y = p.Y + 1
                    },
                    SharedWall = WallState.UP
                });
            }
        }

        //V�rifier si j'ai un voisin � droite
        if (p.X < width - 1)
        {
            //Si il n'est pas encore visit�
            if (!maze[p.X + 1, p.Y].HasFlag(WallState.VISITED))
            {
                //Ajouter le voisins � la liste
                list.Add(new Neighbour
                {
                    Position = new Position
                    {
                        X = p.X + 1,
                        Y = p.Y
                    },
                    SharedWall = WallState.RIGHT
                });
            }
        }

        //Retourner la liste des voisins
        return list;
    }


}
