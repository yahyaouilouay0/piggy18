using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


/// <summary>
/// Cette classe contient les mouvement du joueur basique 
/// Et la possibilit� de r�cup�rer des objets
/// </summary>
public class PlayerMovement : MonoBehaviour
{
    //Le charactercontrolelr du gameobject
    public CharacterController controller;

    public Animator playerAnimator;
    public Transform[] playerAvatar = new Transform[4];

    public Transform playerCamera;
    public Transform Head;
    bool Headback = true;
    //La vitesse
    public float speed = 12f;
    //La gravit� � appliquer
    public float gravity = -9.81f;
    //La longueur du saut
    public float jumpHeight = 2f;

    //Point pour v�rifier si le joueur touche le sol ou non
    public Transform groundCheck;
    //La distance entre le joueur et le sol
    public float groundDistance = 0.6f;
    //R�cuper le layer � comparer (Qui sera Ground)
    public LayerMask groundMask;

    //Les coordon�es � partir des quelle le joueur va jeter son objet
    public Transform dropPoint;


    public GameObject CraftingUI;

    [HideInInspector]
    public Vector3 move;
    [HideInInspector]
    public float x, z;

    //La v�loocit� du joueur
    Vector3 velocity;
    //Bool�n pour changer l'�tat du joueur s'il est sur le sol non
    bool isGrounded;
    //La Vue photon du joueur
    PhotonView view;

    //Le tag des gameobjets � comparer pour savoir si c'est des objets r�cup�rable
    [SerializeField] private string selectableTag = "item";
    //Pour r�cup�rer l'objet selectionner via le Ray
    //private Transform _selection;
    //L'inventaire du joueur
    private Inventory inventory;

    //L'�tat du joueur s'il est en train d'�crire dans un livre ou non
    [HideInInspector]
    public bool isWriting = false;

    public Ray publicRay;
    private bool walking;



    // Start is called before the first frame update
    void Awake()
    {
        //R�cuperer la vue du joueur
        view = GetComponent<PhotonView>();
    }
    void Start()
    {
        if (!view.IsMine)
        {
            //D�truire les cam�ras et les canvas des autres joueurs pour �viter
            //tout type d'int�rf�rance
            Destroy(GetComponentInChildren<Camera>().gameObject);
            Destroy(GetComponentInChildren<Canvas>().gameObject);
            GameObject.FindObjectOfType<AudioManager>().Play("Theme");
            GameObject.FindObjectOfType<AudioManager>().Play("Ocean");
        }
        GameObject.FindObjectOfType<AudioManager>().Play("Theme");
        GameObject.FindObjectOfType<AudioManager>().Play("Ocean");
        //R�cuperer l'inventaire du joueur
        inventory = gameObject.GetComponent<Inventory>();
    }

    // Update is called once per frame
    void Update()
    {
        if (view.IsMine)
        {
            //Permettre le joueur de se d�placer et prendre des objets uniquement
            //s'il n'est pas en train d'�crire
            if(!isWriting)
            {
                //Cette m�thode permet au joueur de se d�placer dans la carte
                Movement();
                //Cette m�thode permet au joueur d'avoir un rayon qui lui permettra
                //de s�lectionner un objet qui pourra ensuite le mettre dans son inventaire
                //si c'est un objet r�cup�rable.
                RaySelector();
                if (!isGrounded)
                {
                    GameObject.FindObjectOfType<AudioManager>().Stop("FootStep");
                    walking = false;
                }
            }
            
        }

    }

    public void Movement()
    {
        //V�rifier l'�tat du joueur s'il est sur le sol ou non avec un checkSphere
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        //R�tablir la v�locit� apr�s un saut
        if (isGrounded && velocity.y < 0)
        {
            if (!Headback)
            {

                Headback = true;
                for (int i = 0; i < playerAvatar.Length; i++)
                {
                    if (playerAvatar[i] != null)
                        playerAvatar[i].gameObject.SetActive(true);
                }
            }
            velocity.y = -2f;
        }
        //R�cup�rer les input du joueur sur l'axe X (Q,D)
        x = Input.GetAxis("Horizontal");
        //R�cup�rer les input du joueur sur l'axe Y (Z,S)
        z = Input.GetAxis("Vertical");
        //Appliquer les valeurs obtenues � au Vecteur move
        move = transform.right * x + transform.forward * z;
        //Faire bouger le joueur selon le vecteur move
        controller.Move(move * speed * Time.deltaTime);
        if (x != 0 || z != 0)
        {
            if (!walking)
            {
                GameObject.FindObjectOfType<AudioManager>().Play("FootStep");
                walking = true;
            }
        }
        else if (walking)
        {
            GameObject.FindObjectOfType<AudioManager>().Stop("FootStep");
            walking = false;
        }

        //Si le joueur appuie sur espace et qu'il est sur le sol
        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            //faire un saut
            EmoteWheelController.emoteID = 0;
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            Headback = false;
            for (int i = 0; i < playerAvatar.Length; i++)
            {
                if (playerAvatar[i] != null)
                    playerAvatar[i].gameObject.SetActive(false);
            }
            playerAnimator.SetTrigger("Jump");
            

        }

        //Appliquer la gravit� pour faire descendre le joueur
        velocity.y += gravity * Time.deltaTime;
        //Appliquer le saut au joueur
        controller.Move(velocity * Time.deltaTime);
        playerAnimator.SetFloat("Velocity X", move.x / 2);
        playerAnimator.SetFloat("Velocity Z", move.z / 2);
        //view.RPC("MovementSync_RPC", RpcTarget.Others, move.x / 2, move.z / 2);
    }

    [PunRPC]
    public void MovementSync_RPC(float x, float z)
    {
        playerAnimator.SetFloat("Velocity X", x);
        playerAnimator.SetFloat("Velocity Z", z);
    }

    private void RaySelector()
    {
        //if (_selection != null)
        //{
        //    _selection = null;
        //}

        //Activer le rayon � partir de la cam�ra
        if(Camera.main != null)
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            publicRay = ray;
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                //R�cup�rer l'objet selctionn�
                var selection = hit.transform;
                //Si l'objet � un tag "item" et que l'utilisateur a appuiy� sur E
                //et que la distance entre lui est l'objet est moins que 5
                if (selection.CompareTag(selectableTag) &&
                    Input.GetKeyDown(KeyCode.E) &&
                    hit.distance < 10)
                {
                    //Alors v�rifier s'il n'a pas d�j� �t� pris (Pour �viter de r�cup�rer un 
                    //objet qui est d�j� r�cup�r�
                    if (!selection.gameObject.GetComponent<Pickup>().picked)
                    {
                        playerAnimator.SetTrigger("PickUp");
                        //Mettre l'objet dans son inventaire avec la m�thode PickUpObject
                        PickUpObject(selection.gameObject);
                        //_selection = selection;
                        GameObject.FindObjectOfType<AudioManager>().Play("Pickup");
                    }

                }

                if (selection.name.ToUpper().Contains("CRAFT TABLE") &&
                    Input.GetKeyUp(KeyCode.F) &&
                    hit.distance < 5)
                {
                    CraftingUI.SetActive(true);
                    Cursor.lockState = CursorLockMode.None;
                    isWriting = true;
                    GetComponent<InteractWithItem>().isCrafting = true;

                }

                if (selection.name.ToUpper().Contains("BIRDOFPARADISE") &&
                            Input.GetKeyUp(KeyCode.E) &&
                            hit.distance < 5)
                {
                    playerAnimator.SetTrigger("PickUp");
                    GameObject leaf = PhotonNetwork.Instantiate("Leaf", selection.position, Quaternion.identity);
                    for (int i = 0; i < inventory.slots.Length; i++)
                    {
                        if (inventory.isFull[i] == false && inventory.slots[i].transform.childCount == 0)
                        {
                            leaf.GetComponent<Pickup>().picked = true;
                            break;
                        }
                        else
                        {
                            leaf.GetComponent<Pickup>().picked = false;
                        }
                    }
                    PickUpObject(leaf);

                }

                if (dropPoint.childCount > 0)
                {
                    if (dropPoint.GetChild(0).name.ToUpper().Contains("PICKAXE"))
                    {
                        if (selection.name.ToUpper().Contains("CRYSTAL ROCK") &&
                            Input.GetKeyUp(KeyCode.E) &&
                            hit.distance < 5)
                        {
                            playerAnimator.SetTrigger("Mine");
                            GameObject crystal = PhotonNetwork.Instantiate("Crystal", selection.position, Quaternion.identity);
                            for (int i = 0; i < inventory.slots.Length; i++)
                            {
                                if (inventory.isFull[i] == false && inventory.slots[i].transform.childCount == 0)
                                {
                                    crystal.GetComponent<Pickup>().picked = true;
                                    break;
                                }
                                else
                                {
                                    crystal.GetComponent<Pickup>().picked = false;
                                }
                            }
                            PickUpObject(crystal);

                        }
                        if (selection.name.ToUpper().Contains("ROCKGHOST") &&
                            Input.GetKeyUp(KeyCode.E) &&
                            hit.distance < 5)
                        {
                            playerAnimator.SetTrigger("Mine");
                            selection.GetComponent<Rock>().timesMined++;

                        }
                    }

                    if (dropPoint.GetChild(0).name.ToUpper().Equals("AXE"))
                    {
                        if (selection.name.ToUpper().Contains("TREE") &&
                            Input.GetKeyUp(KeyCode.E) &&
                            hit.distance < 5)
                        {
                            playerAnimator.SetTrigger("CutWood");
                            GameObject Woodlog = PhotonNetwork.Instantiate("Wood Log", selection.position, Quaternion.identity);
                            for (int i = 0; i < inventory.slots.Length; i++)
                            {
                                if (inventory.isFull[i] == false && inventory.slots[i].transform.childCount == 0)
                                {
                                    Woodlog.GetComponent<Pickup>().picked = true;
                                    break;
                                }
                                else
                                {
                                    Woodlog.GetComponent<Pickup>().picked = false;
                                }
                            }
                            PickUpObject(Woodlog);

                        }
                    }

                }
            }
        }
        
    }

    private void PickUpObject(GameObject selected)
    {
        //if(selected.name.Contains("Crystal"))
        //{
        //    playerAnimator.SetTrigger("Mine");
        //}
        //else
        //{
        //    if(selected.name.Contains("Wood Log"))
        //    {
        //        playerAnimator.SetTrigger("CutWood");
        //    }
        //    else
        //    {
        //        playerAnimator.SetTrigger("PickUp");
        //    }
        //}
        //R�cup�rer l'ic�ne de l'objet
        GameObject itemButton = selected.GetComponent<Pickup>().itemButton;
        //Parcourir tous les slots du joueurs
        for (int i = 0; i < inventory.slots.Length; i++)
        {
            //Si le slots[i] n'est pas remplis et que le gameobject slots[i] n'a aucun fils
            //�a veut dire que le slot est libre
            if (inventory.isFull[i] == false && inventory.slots[i].transform.childCount == 0)
            {
                //Mettre la case i du tableau full � vrai
                //pour indiquer que le slot est rempli
                inventory.isFull[i] = true;
                //Instancier l'�cone du raccourci dans le UI du joueur
                Instantiate(itemButton, inventory.slots[i].transform, false);
                //Envoyer un RPC aux autres joueurs pour notifier l'�tat de l'objet r�cup�r�
                view.RPC("RPC_PickUpObject", RpcTarget.Others, selected.GetComponent<PhotonView>().ViewID);
                //Si c'est ma vue (test potentiellement inutile)
                if (view.IsMine)
                {
                    //Mettre l'�tat de l'objet en r�cup�r�
                    selected.gameObject.GetComponent<Pickup>().picked = true;
                    //Mettre l'objet en �tat Kinematic
                    selected.GetComponent<Rigidbody>().isKinematic = true;
                    //Activer le rigibody
                    selected.GetComponent<Rigidbody>().WakeUp();
                    //Mettre l'objet en tant que fils du slot
                    selected.transform.SetParent(inventory.slots[i].transform);
                    //Mettre son �tat active � faux
                    selected.SetActive(false);
                }
                //Quitter la boucle d�s qu'on r�cup�re un objet
                break;
            }
        }

        int itemInDropPointViewID = selected.GetComponent<PhotonView>().ViewID;
        PhotonView dropPointView = dropPoint.GetComponent<PhotonView>();
        if (selected.name.Equals("RewardCapsule"))
        {
            if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 2)
                dropPointView.RPC("RPC_EditRewardCapsule", RpcTarget.All, itemInDropPointViewID);
        }
        if (selected.name.ToUpper().Contains("GASOIL"))
        {
            if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 2)
                dropPointView.RPC("RPC_RewardGasoil", RpcTarget.All, itemInDropPointViewID);
        }
        if (selected.name.ToUpper().Contains("MAGICCUBELOCKED"))
        {
            if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 1)
            {
                dropPointView.RPC("RPC_RewardMaze", RpcTarget.All, itemInDropPointViewID);
            }
        }
        if (selected.name.ToUpper().Contains("KEY"))
        {
            if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 1)
                dropPointView.RPC("RPC_RewardPillars", RpcTarget.All, itemInDropPointViewID);
        }
        if (selected.name.ToUpper().Contains("DISK"))
        {
            if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 3)
                dropPointView.RPC("RPC_RewardCem", RpcTarget.All, itemInDropPointViewID);
        }
        if (selected.name.ToUpper().Contains("BATTERY"))
        {
            if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 3)
                dropPointView.RPC("RPC_RewardBattery", RpcTarget.All, itemInDropPointViewID);
        }
    }

    [PunRPC]
    public void RPC_PickUpObject(int viewToDisableID)
    {
        //R�cuperer la vue Photon de l'objet r�cup�rer par le joueur
        PhotonView viewToDisable = PhotonNetwork.GetPhotonView(viewToDisableID);
        //Annoncer aux autres joueurs que son �tat est Picked
        viewToDisable.gameObject.GetComponent<Pickup>().picked = true;
        //Mettre l'�tat active de l'objet � Faux pour qu'il n'appara�t plus dans leur sc�ne
        viewToDisable.gameObject.SetActive(false);
        playerAnimator.SetTrigger("PickUp");
    }

    [PunRPC]
    public void RPC_PlayAnimationWithTrigger(string triggerName)
    {
        playerAnimator.SetTrigger(triggerName);
    }


}
