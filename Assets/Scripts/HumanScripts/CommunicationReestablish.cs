using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

/// <summary>
/// Cette classe permet aux humains de r�tablir les tubes de communication
/// ainsi rendre les deux points d'entr�e actif
/// </summary>
public class CommunicationReestablish : MonoBehaviourPun
{
    // Le point d'entr�e 1 du tube de communication
    public GameObject entryPoint1;
    // Le point d'entr�e 2 du tube de communication
    public GameObject entryPoint2;

    //L'�tat de la valve si elle peut tourner ou non
    bool canTurn = false;
    //Le GO de la valve
    GameObject valve;
    //La vue du GO qui contient le script
    PhotonView view;
    //L'animateur qui lance l'animation de la valve
    Animator valveAnimation;


    private void Awake()
    {
        //R�cup�rer la PhotonView du GO
        view = GetComponent<PhotonView>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //R�cup�rer la valve � tourner pour fermer les communications
        valve = transform.GetChild(2).gameObject;
        //R�cup�rer l'animateur sur la valve
        valveAnimation = valve.GetComponent<Animator>();
    }

    //Update is called once per frame
    void Update()
    {
        //V�rifier si un Humain est � cot� de la valve 
        // et qu'il a appuiy� sur le bouton F
        if (Input.GetKeyDown(KeyCode.F) &&
            canTurn &&
            ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 1||
             (int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 2||
             (int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 3))
        {
            //Envoyer un RPC � tous les joueurs pour modifiers l'�tat
            //des points d'entr�e et lancer l'animation
            view.RPC("TurnValveToReestablish", RpcTarget.All);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        //Si un Humain est entr� dans la zone de collision
        if (other.gameObject.name.ToUpper().Contains("MEDIC")||
            other.gameObject.name.ToUpper().Contains("ATHELTE") ||
            other.gameObject.name.ToUpper().Contains("ENGINEER"))
        {
            //alors il peut tourner la valve
            canTurn = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Si un Humain sort de la zone de collision
        if (other.gameObject.name.ToUpper().Contains("MEDIC") ||
            other.gameObject.name.ToUpper().Contains("ATHELTE") ||
            other.gameObject.name.ToUpper().Contains("ENGINEER"))
        {
            //alors il ne peut plus tourner la valve
            canTurn = false;
        }
    }

    [PunRPC]
    public void TurnValveToReestablish()
    {
        //Lancer l'animation (valve qui tourne)
        valveAnimation.SetTrigger("click");
        //D�sactiver le point d'entr�e 1
        entryPoint1.SetActive(true);
        //D�sactiver le point d'entr�e 2
        entryPoint2.SetActive(true);
    }
}
