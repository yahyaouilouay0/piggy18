using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanShield : MonoBehaviourPun
{
    [HideInInspector]
    public int numOfTriggers;
    [HideInInspector]
    public bool isActivated;
    private bool isCalled;

    private void Update()
    {
        if (numOfTriggers == 3)
        {
            isActivated = true;
            if (!isCalled)
            {
                isCalled = true;
                base.photonView.RPC("ActivateShield", RpcTarget.All);
            }
            
        }
        else
        {
            isActivated = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] != 0 && other.gameObject.CompareTag("Player"))
        {
            
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 0 && other.gameObject.CompareTag("Player"))
        {
            GameObject.Find("Joka(Clone)").GetComponent<JokaAbilitiesManager>().DeactivateAbilities();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 0 && other.gameObject.CompareTag("Player"))
        {
            GameObject.Find("Joka(Clone)").GetComponent<JokaAbilitiesManager>().ActivateAbilities();
        }
        if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] != 0 && other.gameObject.CompareTag("Player"))
        {
            
        }
    }

    [PunRPC]
    public void ActivateShield()
    {
        if (!gameObject.GetComponent<MeshRenderer>().enabled)
            gameObject.GetComponent<MeshRenderer>().enabled = true;
        if (!gameObject.GetComponent<MeshCollider>().enabled)
            gameObject.GetComponent<MeshCollider>().enabled = true;
    }
}
