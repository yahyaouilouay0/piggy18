using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Cette classe contient le text qui est dans livre (notebook)
/// et l'�tat du livre s'il est ouvert ou ferm�
/// </summary>
public class NotesSave : MonoBehaviour
{
    //le text que contient le livre
    [HideInInspector]
    public string text = "";
    //l'�tat du livre
    [HideInInspector]
    public bool isOpen = false;
}
