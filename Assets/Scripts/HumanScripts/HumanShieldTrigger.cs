using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanShieldTrigger : MonoBehaviour
{
    public GameObject shield;

   
    private void OnTriggerEnter(Collider other)
    {
        if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] != 0 && other.gameObject.CompareTag("Player"))
        {
            shield.GetComponent<HumanShield>().numOfTriggers++;
        }
            
    }

    private void OnTriggerExit(Collider other)
    {
        if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] != 0 && other.gameObject.CompareTag("Player"))
        {
            shield.GetComponent<HumanShield>().numOfTriggers--;
        }
    }
}
