using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineerSpawns : MonoBehaviour
{
    PhotonView view;
    [Range(0,9)]
    public int diffcultyMirrorQuest = 3;

    public GameObject[] playerHead = new GameObject[5];
    private void Awake()
    {
        //PhotonNetwork.SetMasterClient(PhotonNetwork.LocalPlayer);
    }
    // Start is called before the first frame update
    void Start()
    {
        Vector3 spawnBallPoint = GameObject.Find("BallSpawnPlayer1").transform.position;
        view = GetComponent<PhotonView>();
        if (view.IsMine)
        {
            for (int i = 1; i < 4; i++)
            {
                PhotonNetwork.Instantiate("Ball1", spawnBallPoint + new Vector3(0, i, 0), Quaternion.identity);
            }
            Transform mirrorPlatform = GameObject.Find("Mirror Platform").transform;
            var rng = new System.Random();
            int rndNumber;
            List<int> numbersGenerated = new List<int>();
            for (int i = 0; i < diffcultyMirrorQuest; i++)
            {
                
                rndNumber = rng.Next(0, 8);
                while (numbersGenerated.Contains(rndNumber))
                {
                    rndNumber = rng.Next(0, 8);
                }
                numbersGenerated.Add(rndNumber);
                PhotonNetwork.Instantiate("P_AncientRuins_Rock4", mirrorPlatform.GetChild(8+rndNumber).position, Quaternion.identity);
            }

            view.RPC("RPC_ActivateHead", RpcTarget.Others);
        }
        
         
    }

    [PunRPC]
    public void RPC_ActivateHead()
    {
        for(int i=0;i<5;i++)
        {
            playerHead[i].SetActive(true);
        }
    }
}
