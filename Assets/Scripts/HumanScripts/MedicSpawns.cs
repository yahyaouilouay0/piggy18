using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MedicSpawns : MonoBehaviour
{
    PhotonView view;
    public GameObject[] playerHead = new GameObject[6];
    // Start is called before the first frame update
    void Start()
    {
        Vector3 spawnBallPoint = GameObject.Find("BallSpawnPlayer2").transform.position;
        view = GetComponent<PhotonView>();
        if(view.IsMine)
        {
            for(int i=1;i<4;i++)
            {
                PhotonNetwork.Instantiate("Ball2", spawnBallPoint + new Vector3(0, i, 0), Quaternion.identity);
            }

            view.RPC("RPC_ActivateHeadMedic", RpcTarget.Others);
        }
    }

    [PunRPC]
    public void RPC_ActivateHeadMedic()
    {
        for (int i = 0; i < 6; i++)
        {
            playerHead[i].SetActive(true);
        }
    }
}
