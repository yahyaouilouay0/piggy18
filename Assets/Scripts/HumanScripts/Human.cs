using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
[RequireComponent(typeof(PhotonTransformView))]
public class Human : MonoBehaviour
{
    public float health;
    public float initialHealth;
    public bool isDying;
    public bool isKnocked=false;
    public Animator playerAnimator;
    public Transform playerCamera;
    public Transform canvas;
    public Transform timerUI;
    float timer = 60;
    bool cameraIsSet = false;
    List<Transform> canvasItems = new List<Transform>();

    private void Start()
    {
        initialHealth = health;
    }
    private void Update()
    {
        if(health<=0)
        {
            isKnocked = true;
            playerCamera.parent = null;
            
            if (!cameraIsSet)
            {
                cameraIsSet = true;
                playerCamera.transform.position += new Vector3(0, 2, 5);
                playerCamera.LookAt(transform, Vector3.down);
                playerCamera.transform.localEulerAngles = new Vector3(40, -180);
                for (int i = 0; i < canvas.childCount; i++)
                {
                    if (canvas.GetChild(i).gameObject.activeInHierarchy)
                    {
                        canvasItems.Add(canvas.GetChild(i));
                        canvas.GetChild(i).gameObject.SetActive(false);
                    }
                }
            }
            GetComponent<PlayerMovement>().isWriting = true;
            if(timer <3)
            {
                playerAnimator.SetBool("isKnocked", false);
                
            }
            else
            {
                if(timer<=55)
                {
                    playerAnimator.SetBool("isKnocked", true);
                    playerAnimator.SetBool("isInjured", false);
                }
                else
                {
                    playerAnimator.SetBool("isInjured", true);
                }
            }
            //if (timer <= 55)
            //{
            //    playerAnimator.SetBool("isKnocked", true);
            //}
            //else
            //{
            //    if(timer <=5)
            //    {
            //        playerAnimator.SetBool("isKnocked", false);
            //    }
            //    else
            //    {
            //        playerAnimator.SetBool("isInjured", true);
            //    }
                
            //}
            
            timer -= Time.deltaTime;
            if(!timerUI.gameObject.activeInHierarchy)
            {
                timerUI.gameObject.SetActive(true);
            }
            if(!timerUI.GetComponent<Animator>().GetBool("isKnocked"))
            {
                timerUI.GetComponent<Animator>().SetBool("isKnocked", true);
            }
            timerUI.GetChild(0).GetComponent<Text>().text = Mathf.CeilToInt(timer).ToString();
            
            if(TryGetComponent<EngineerSpawns>(out EngineerSpawns engineerSpawns))
            {
                for (int i = 0; i < engineerSpawns.playerHead.Length; i++)
                {
                    engineerSpawns.playerHead[i].SetActive(true);
                }
            }
            if (TryGetComponent<AthleteSpawn>(out AthleteSpawn athleteSpawn))
            {
                for (int i = 0; i < athleteSpawn.playerHead.Length; i++)
                {
                    athleteSpawn.playerHead[i].SetActive(true);
                }
            }
            if (TryGetComponent<MedicSpawns>(out MedicSpawns medicSpawns))
            {
                for (int i = 0; i < medicSpawns.playerHead.Length; i++)
                {
                    medicSpawns.playerHead[i].SetActive(true);
                }
            }
            playerCamera.RotateAround(transform.position, Vector3.up, 12 * Time.deltaTime);
            if (timer<=0)
            {
                foreach(Transform c in canvasItems)
                {
                    c.gameObject.SetActive(true);
                }
                timerUI.gameObject.SetActive(false);
                timerUI.GetComponent<Animator>().SetBool("isKnocked", false);
                GetComponent<PlayerMovement>().isWriting = false;
                //playerAnimator.SetBool("isKnocked", false);
                timer = 60;
                playerCamera.parent = transform;
                playerCamera.transform.localPosition = new Vector3(0, 2.4f, 0.3f);
                playerCamera.transform.localEulerAngles = new Vector3(0, 0, 0);
                playerAnimator.gameObject.transform.localEulerAngles = new Vector3(0, 0, 0);
                playerAnimator.gameObject.transform.localPosition = new Vector3(0, 0, 0);
                if (TryGetComponent<EngineerSpawns>(out engineerSpawns))
                {
                    for (int i = 0; i < engineerSpawns.playerHead.Length; i++)
                    {
                        engineerSpawns.playerHead[i].SetActive(false);
                    }
                }
                if (TryGetComponent<AthleteSpawn>(out athleteSpawn))
                {
                    for (int i = 0; i < athleteSpawn.playerHead.Length; i++)
                    {
                        athleteSpawn.playerHead[i].SetActive(false);
                    }
                }
                if (TryGetComponent<MedicSpawns>(out medicSpawns))
                {
                    for (int i = 0; i < medicSpawns.playerHead.Length; i++)
                    {
                        medicSpawns.playerHead[i].SetActive(false);
                    }
                }
                health = initialHealth / 2;
                isKnocked = false;
                cameraIsSet = false;
            }

        }

        if (!isKnocked)
        {
            if (isDying && health > 0)
            {
                health -= Time.deltaTime * 32;
            }
            else if (!isDying && health < 100)
            {
                health += Time.deltaTime * 48;
            }
        }
    }
}
