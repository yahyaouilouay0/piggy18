using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Cette classe contient l'�tat de l'inventaire de l'humain 
/// et les diff�rents slots o� il peut stocker ses objets
/// </summary>
public class Inventory : MonoBehaviour
{
    //Un tableau de bool�en pour d�finir l'�tat de la case X si elle est remplie ou non
    public bool[] isFull;
    //Un tableau de bool�en pour d�finir qu'elle case est s�lectionn� actuellement
    public bool[] isSelected;
    //Un tableau de gameobjets pour mettre le UI slot afin de stocker dedans les objets
    public GameObject[] slots;

    //Le transform du dernier objet s�lectionner
    [HideInInspector]
    public Transform lastSelected;

    PhotonView view;

    void Awake()
    {
        view = GetComponent<PhotonView>();
    }
}
