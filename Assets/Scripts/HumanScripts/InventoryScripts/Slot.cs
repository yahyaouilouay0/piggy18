using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slot : MonoBehaviour, IOnEventCallback
{
    //L'inventaire du joueur
    private Inventory inventory;
    //L'index du slot sur le quelle est le script
    public int slotIndex;
    //L'image du slot par d�faut
    public Sprite defaultSlot;
    //L'image du slot quand il est s�lectionn�
    public Sprite selectedSlot;
    //Les coordon�es � partir des quelle le joueur va jeter son objet
    public Transform dropPoint;

    //public float dropForwardForce = 10f;
    //public float dropUpwardForce = 2f;

    //Le joueur
    public GameObject player;
    //La vue photon du slot
    PhotonView view;
    //L'�tat du joueur s'il est en train d'�crire ou non
    bool isWriting;

    void Awake()
    {
        //R�cup�rer la vue Photon du slot
        view = GetComponent<PhotonView>();
    }

    private void Start()
    {
        //R�cup�rer l'inventaire du joueur
        inventory = player.GetComponent<Inventory>();
    }

    private void Update()
    {
        if (view.IsMine)
        {
            //R�cup�rer l'�tat du joueur s'il est en train d'�crire ou non
            isWriting = player.GetComponent<PlayerMovement>().isWriting;
            //Si le slot n'a aucun objet fils �a veut dire qu'il ne contient aucun item
            if (transform.childCount <= 0)
            {
                //alors mettre l'�tat de isFull � faux
                inventory.isFull[slotIndex] = false;
            }
            //Si je suis en train d'�crire ne pas laisser le joueur s�lectionner un 
            //autre objet
            if (isWriting)
                return;

            //Si le joueur appuie sur &
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                //V�rifier si j'ai des objets en tant que fils dans le slot
                if (transform.childCount > 0)
                {
                    //R�cup�rer le premier fils du slot
                    Transform child = transform.GetChild(0);
                    //V�rifier que le nom du parent du slot correspond 
                    //� la touche que j'ai appuiy�
                    if (child.parent.name == "InventorySlot (0)")
                    {
                        //Si c'est le cas le s�lectionner
                        SelectItem(slotIndex);
                    }
                }
            }

            //M�me que la pr�cendte mais avec la touche �
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                if (transform.childCount > 0)
                {
                    Transform child = transform.GetChild(0);
                    if (child.parent.name == "InventorySlot (1)")
                    {
                        SelectItem(slotIndex);
                    }
                }
            }

            //M�me que la pr�cendte mais avec la touche "
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {

                if (transform.childCount > 0)
                {
                    Transform child = transform.GetChild(0);
                    if (child.parent.name == "InventorySlot (2)")
                    {
                        SelectItem(slotIndex);
                    }
                }
            }

            //M�me que la pr�cendte mais avec la touche '
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                if (transform.childCount > 0)
                {
                    Transform child = transform.GetChild(0);
                    if (child.parent.name == "InventorySlot (3)")
                    {
                        SelectItem(slotIndex);
                    }
                }
            }

            //M�me que la pr�cendte mais avec la touche (
            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                if (transform.childCount > 0)
                {
                    Transform child = transform.GetChild(0);
                    if (child.parent.name == "InventorySlot (4)")
                    {
                        SelectItem(slotIndex);
                    }
                }
            }

            //M�me que la pr�cendte mais avec la touche -
            if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                if (transform.childCount > 0)
                {
                    Transform child = transform.GetChild(0);
                    if (child.parent.name == "InventorySlot (5)")
                    {
                        SelectItem(slotIndex);
                    }
                }
            }

            //Si le joueur appuie sur A
            if (Input.GetKeyDown(KeyCode.A))
            {
                //L�cher l'objet
                DropItem();
                GameObject.FindObjectOfType<AudioManager>().Play("Drop");
            }
        }
       
    }

    private void SelectItem(int i)
    {
        //Si le slot que j'essai s�lectionner l'est d�j�
        //alors je vais le d�slectionner
        if (inventory.isSelected[i])
        {
            //Mettre l'image du slot au slot par d�faut
            gameObject.GetComponent<Image>().sprite = defaultSlot;
            //Mettre l'�tat du slot s�lectionn� � faux
            inventory.isSelected[i] = false;
            //Mettre le dernier objet selectionn� � null
            inventory.lastSelected = null;
            //v�rifier que j'ai bien objet dans les mains
            if (dropPoint.GetChild(0))
            {
                //Mettre le transform de l'objet dans les mains dans tempChild
                Transform tempChild = dropPoint.GetChild(0);
                //Changer l'�tat active de l'objet � faux
                tempChild.gameObject.SetActive(false);
                //Enlever le dropPoint en tant que parent de l'objet et mettre
                //le slot de I en tant que son nouveau parent
                tempChild.SetParent(inventory.slots[i].transform);
                //Mettre la position et la rotation de l'objet comme celle du drop point
                tempChild.position = dropPoint.position;
                tempChild.rotation = dropPoint.rotation;
                //Mettre l'�tat Kinematic de l'objet � faux
                tempChild.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                //tempChild.gameObject.SetActive(false);
                //Pr�parer le contenu � envoyer � l'event
                object[] content = new object[] { tempChild.gameObject.GetComponent<PhotonView>().ViewID};
                //Pr�parer les param�tres de l'event dont envoyer l'�venement uniquement aux autres joeueurs
                RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others };
                //Lancer l'�venement ayant un id 2
                PhotonNetwork.RaiseEvent(2, content, raiseEventOptions, SendOptions.SendReliable);
            }
        }
        //Si le slot qui a pour index i n'est pas s�lectionn�
        //�a veut dire que je vais le s�lectionner
        else
        {
            //Remplacer son image par celle d'un slot s�lectionner
            gameObject.GetComponent<Image>().sprite = selectedSlot;
            //Mettre son �tat (S'il est s�lectionn�) � vrai
            inventory.isSelected[i] = true;
            //R�cup�rer l'objet en question (Le premier child est l'ic�ne et le deuxi�me
            //c'est l'objet)
            Transform child = transform.GetChild(1);
            //Si l'objet contient un Rigibody (Pour bien v�rifier que c'est un objet
            //qu'on a r�cup�rer et ainsi r�cup�rer son rigibody
            if (child.gameObject.TryGetComponent<Rigidbody>(out Rigidbody rb))
            {
                //V�rifier s'il n'y pas un slot qui est d�j� s�lectionn�
                if (inventory.lastSelected != null)
                {
                    //Si je s�lectionne un objet alors que j'en ai d�j� dans les mains (dropPoint)
                    if (dropPoint.childCount > 0)
                    {
                        //R�cup�rer l'objet qui est le fils du dropPoint(dans les mains)
                        Transform actualChildOfDropPoint = dropPoint.GetChild(0);
                        //Mettre les parents de l'objet dans les mains dans le slot o� il �tait
                        actualChildOfDropPoint.SetParent(inventory.lastSelected);
                        //Mettre son �tat Kinematic � faux
                        actualChildOfDropPoint.gameObject.GetComponent<Rigidbody>().isKinematic = false;
                        //Mettre son �tat Active � faux
                        actualChildOfDropPoint.gameObject.SetActive(false);
                        //Pr�parer le contenu � envoyer � l'event
                        object[] actualContent = new object[] { actualChildOfDropPoint.gameObject.GetComponent<PhotonView>().ViewID};
                        //Pr�parer les param�tres de l'event dont envoyer l'�venement uniquement aux autres joeueurs
                        RaiseEventOptions actualRaiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.Others };
                        //Lancer l'�venement ayant un id 2
                        PhotonNetwork.RaiseEvent(2, actualContent, actualRaiseEventOptions, SendOptions.SendReliable);
                    }

                }
                //Pr�parer le contenu � envoyer � l'event

                object[] content = new object[] { child.GetComponent<PhotonView>().ViewID, dropPoint.gameObject.GetComponent<PhotonView>().ViewID };
                //Pr�parer les param�tres de l'event dont envoyer l'�venement � tous les joeueurs
                RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All };
                //Lancer l'�venement ayant un id 1
                PhotonNetwork.RaiseEvent(1, content, raiseEventOptions, SendOptions.SendReliable);
                //Debug.Log("Test "+ child.name.ToUpper());
                //if (child.name.ToUpper().Contains("MIRROR STICK"))
                //{
                //    Debug.Log("Test OK " + child.name.ToUpper());
                //    child.gameObject.SetActive(false);
                //    child.GetComponent<PhotonView>().RPC("RPC_DisableMirrorStick", RpcTarget.Others, child.GetComponent<PhotonView>().ViewID);
                    
                //}
                //Debug.Log("Test End");

            }

            //Parcourir tous les slots
            foreach (Transform tempInventorySlot in transform.parent)
            {
                //Si le slot est diff�rent � l'actuel
                if(tempInventorySlot.GetComponent<Slot>().slotIndex != i)
                {
                    //Mettre l'image du slot actuel(Slot de la boucle) � l'image par d�faut
                    tempInventorySlot.GetComponent<Image>().sprite = defaultSlot;
                    //Mettre l'�tat is Selected avec l'indice du slot actuel(Slot de la boucle) � faux
                    inventory.isSelected[tempInventorySlot.GetComponent<Slot>().slotIndex] = false;
                }
            }
            //Mettre � jour le denier objet s�lectionn�
            inventory.lastSelected = transform;

        }
    }

    public void DropItem()
    {
        //Si j'ai bien objet s�lectionner
        if (inventory.lastSelected != null)
        {
            //Si j'ai bien un objet dans les mains
            if (dropPoint.childCount > 0)
            {
                //Mettre l'image du slot concern� � l'image par d�faut
                inventory.lastSelected.gameObject.GetComponent<Image>().sprite = defaultSlot;
                //V�rifier que j'ai toujours l'ic�ne de l'objet dans mon menu d'inventaire
                if (inventory.lastSelected.childCount > 0)
                {
                    //Si c'est le cas le d�truire
                    Destroy(inventory.lastSelected.GetChild(0).gameObject);
                }
                //Mettre l'�tat isSelected du slot concern� � faux
                inventory.isSelected[inventory.lastSelected.GetComponent<Slot>().slotIndex] = false;

                //Pr�parer le contenu � envoyer � l'event
                object[] content = new object[] { dropPoint.GetChild(0).gameObject.GetComponent<PhotonView>().ViewID, dropPoint.position }; // Array contains the target position and the IDs of the selected units
                //Pr�parer les param�tres de l'event dont envoyer l'�venement � tous les joeueurs
                RaiseEventOptions raiseEventOptions = new RaiseEventOptions { Receivers = ReceiverGroup.All }; // You would have to set the Receivers to All in order to receive this event on the local client as well
                //Lancer l'�venement ayant un id 3
                PhotonNetwork.RaiseEvent(3, content, raiseEventOptions, SendOptions.SendReliable);
            }
        }
    }

    //M�thode impl�ment� par IOnEventCallback
    public void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    //M�thode impl�ment� par IOnEventCallback
    public void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;
        if (eventCode == 1)
        {
            //R�cup�rer les donn�e envoy� par l'event
            object[] data = (object[])photonEvent.CustomData;

            //R�cup�rer la vue Photon de l'objet s�lectionner
            PhotonView viewGameObject = PhotonNetwork.GetPhotonView((int)data[0]);
            //R�cup�rer la vue Photo du Drop Point
            PhotonView viewDropPoint = PhotonNetwork.GetPhotonView((int)data[1]);
            //R�cup�rer le transform de l'objet dans les mains (s�lectionner)
            Transform child = viewGameObject.gameObject.transform;
            //R�cup�rer le transform du dropPoint 
            Transform dropPoint = viewDropPoint.gameObject.transform;
            //Mettre l'objet s�lectionn� en tant que fils du dropPoint
            child.SetParent(dropPoint);
            //Mettre son �tat Active � vrai
            child.gameObject.SetActive(true);
            if (child.gameObject.layer != 18)
                child.gameObject.layer = 2;
            //Mettre sa position et sa rotation comme celle du dropPoint
            child.position = dropPoint.position;
            child.rotation = dropPoint.rotation;
            //Debug.Log(child.name);

            if (child.name.Contains("MazeMaquette"))
            {
                child.localPosition = new Vector3(0, 0.2f, 0.3f);
                child.Rotate(new Vector3(-30, 0, 0));
            }
            if (child.name.Contains("StylRocksMagic"))
            {
                child.localPosition = new Vector3(0, 0.4f, 0f);
                child.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            }
            //Mettre son �tat Kinematic � Vrai pour qu'il ne tombe pas de ses mains
            child.gameObject.GetComponent<Rigidbody>().isKinematic = true;

        }

        if (eventCode == 2)
        {
            //R�cup�rer les donn�e envoy� par l'event
            object[] data = (object[])photonEvent.CustomData;
            //R�cup�rer la vue photon de l'objet
            PhotonView viewGameObject = PhotonNetwork.GetPhotonView((int)data[0]);
            //R�cuperer le transform de l'objet
            Transform child = viewGameObject.gameObject.transform;
            //Mettre le parent de l'objet � null pour qu'il puisse l'avoir
            //dans leur sc�ne � eux vu qu'il n'ont pas le canvas du joueur
            //qui poss�de l'objet actuellement
            child.SetParent(null);
            //Mettre son �tat Kinematic � faux
            child.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            //Mettre l'�tat Active de l'objet � faux
            child.gameObject.SetActive(false);

        }
        if (eventCode == 3)
        {
            //R�cup�rer les donn�e envoy� par l'event
            object[] data = (object[])photonEvent.CustomData;
            //R�cup�rer la vue photon de l'objet
            PhotonView viewGameObject = PhotonNetwork.GetPhotonView((int)data[0]);
            //R�cup�rer la position du dropPoint
            Vector3 dropPointPos = (Vector3)data[1];
            //R�cuperer le transform de l'objet � drop
            Transform itemToDrop = viewGameObject.gameObject.transform;
            //Enlever le parent de l'objet
            itemToDrop.SetParent(null);
            if (itemToDrop.gameObject.layer != 18)
                itemToDrop.gameObject.layer = 0;
            //Mettre l'�tat picked de l'objet � faux pour qu'il soit de nouveau r�cup�rable
            itemToDrop.gameObject.GetComponent<Pickup>().picked = false;
            //R�cup�rer son Rigibody
            Rigidbody rbItemToDrop = itemToDrop.gameObject.GetComponent<Rigidbody>();
            //Mettre l'�tat Kinematic de son rigibody � faux
            rbItemToDrop.isKinematic = false;
            //Mettre sa position � celle du dropPoint pour qu'il tombe � partir de ce point
            rbItemToDrop.transform.position = dropPointPos;

        }
    }

    



}
