using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class DropPointChecker : MonoBehaviour
{
    public Animator playerAnimator;
    bool isSelected = false;
    bool resetDropPoint = false;
    bool magicCubeQuestCompleted = false;
    bool gateQuestCompleted = false;
    bool mazeQuestCompleted = false;
    bool pillarsQuestCompleted = false;
    bool cemetryQuestCompleted = false;
    bool batteryQuestCompleted = false;
    PhotonView view;
    // Start is called before the first frame update
    void Start()
    {
        view = GetComponent<PhotonView>();
    }

    //Update is called once per frame
    void Update()
    {


        if (transform.childCount >= 1)
        {
            int itemInDropPointViewID = transform.GetChild(0).GetComponent<PhotonView>().ViewID;
            //isSelected = transform.GetChild(0).GetComponent<Pickup>().isSelected;
            //view.RPC("RPC_UpdateSelectState", RpcTarget.All);
            if (transform.GetChild(0).name.ToUpper().Contains("MIRROR STICK"))
            {
                view.RPC("RPC_DisableMirrorStick", RpcTarget.All, itemInDropPointViewID);
            }
            if (transform.GetChild(0).name.Contains("MazeMaquette") && !isSelected)
            {
                view.RPC("RPC_ScaleMaquette", RpcTarget.All, itemInDropPointViewID);
            }
            if (transform.GetChild(0).name.ToUpper().Contains("PICKAXE") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(0, -0.2f, 0), new Vector3(180, 0, 100));
            }
            if (transform.GetChild(0).name.ToUpper().Contains("TORCH") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(0.008f, -0.23f, 0.02f), new Vector3(0f, -90f, 0f));
            }
            if (transform.GetChild(0).name.ToUpper().Contains("ROPE") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(-0.23f, 0.008f, 0.18f), new Vector3(0f, 0f, 69f));
            }
            if (transform.GetChild(0).name.ToUpper().Contains("ORB") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(0f, -0.08f, 0f), new Vector3(0f, 0f, 0f));
            }
            if (transform.GetChild(0).name.ToUpper().Contains("DISK") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(0f, -0.09f, 0.05f), new Vector3(90f, 0f, 0f));
            }
            if (transform.GetChild(0).name.ToUpper().Contains("CROWN") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(0f, -0.09f, -0.04f), new Vector3(70f, 0f, 0f));
            }
            if (transform.GetChild(0).name.ToUpper().Contains("SAND") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(0f, -0.04f, 0f), new Vector3(0f, 0f, 0f));
            }
            if (transform.GetChild(0).name.ToUpper().Contains("SWORD") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(-0.2f, -0.2f, 0.04f), new Vector3(11f, 90f, 90f));
            }
            if (transform.GetChild(0).name.ToUpper().Equals("AXE") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(0, -0.2f, 0), new Vector3(0, 0, 100));
            }
            if (transform.GetChild(0).name.ToUpper().Contains("KEY") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(0, -0.12f, 0.05f), new Vector3(100, 0, 0));
            }
            if (transform.GetChild(0).name.ToUpper().Contains("BATTERY") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(0.16f, 0.25f, -0.075f), new Vector3(-80, -145, 235));
            }
            if (transform.GetChild(0).name.ToUpper().Contains("NOTEBOOK") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(0f, -0.25f, 0f), new Vector3(0, 0, 0));
            }
            if (transform.GetChild(0).name.ToUpper().Contains("GROUP8801") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(0.06f, -0.2f, 0.02f), new Vector3(0, 360, 80));
            }
            if (transform.GetChild(0).name.ToUpper().Contains("LEAF") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(-0.015f, -0.23f, 0.025f), new Vector3(100, -140, -60));
            }
            if (transform.GetChild(0).name.ToUpper().Contains("BANDAGE") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(0f, -0.45f, 0.02f), new Vector3(0, 0, 0));
            }
            if (transform.GetChild(0).name.Equals("StylRocksMagic_5_LOD2") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(0f, -0.1f, 0f), new Vector3(30, 0, 90));
            }
            if (transform.GetChild(0).name.Equals("StylRocksMagic_4_LOD2") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(0f, 0.05f, 0.02f), new Vector3(18, 0, 90));
            }
            if (transform.GetChild(0).name.Equals("StylRocksMagic_3_LOD2") && !isSelected)
            {
                view.RPC("RPC_EditItemTransform", RpcTarget.All, itemInDropPointViewID, new Vector3(0f, 0f, 0f), new Vector3(0, 0, 90));
            }
            
            //if (transform.GetChild(0).name.Contains("StylRocksMagic"))
            //{
            //    view.RPC("RPC_ScaleRock", RpcTarget.All, transform.GetChild(0).GetComponent<PhotonView>().ViewID);
            //}

        }
        else
        {
            if (resetDropPoint)
            {
                view.RPC("RPC_isSelectedReset", RpcTarget.All);
            }

        }
    }

    [PunRPC]
    public void RPC_DisableMirrorStick(int MirrorStickViewID)
    {
        PhotonView MirrorStickView = PhotonNetwork.GetPhotonView(MirrorStickViewID);
        if (MirrorStickView != null)
        {
            MirrorStickView.gameObject.SetActive(false);
            resetDropPoint = true;
            //transform.GetChild(0).GetComponent<Pickup>().isSelected = false;
        }

    }

    [PunRPC]
    public void RPC_ScaleMaquette(int MaquetteViewID)
    {
        PhotonView MaquetteView = PhotonNetwork.GetPhotonView(MaquetteViewID);
        if (MaquetteView != null)
        {
            //playerAnimator.SetBool("Maquette", true);
            MaquetteView.transform.localEulerAngles = new Vector3(0, -90, 180);
            isSelected = true;
            resetDropPoint = true;
            //MaquetteView.transform.localPosition = new Vector3(0, 0.2f, 0.3f);
            //MaquetteView.transform.Rotate(new Vector3(0, -90, 180));

            //transform.GetChild(0).GetComponent<Pickup>().isSelected = false;
        }
    }

    [PunRPC]
    public void RPC_EditItemTransform(int itemiewID, Vector3 position, Vector3 rotation)
    {
        PhotonView itemView = PhotonNetwork.GetPhotonView(itemiewID);
        if (itemView != null)
        {
            itemView.transform.localPosition = position;
            itemView.transform.localEulerAngles = rotation;
            isSelected = true;
            resetDropPoint = true;
        }
    }

    [PunRPC]
    public void RPC_EditRewardCapsule(int itemiewID)
    {
        PhotonView itemView = PhotonNetwork.GetPhotonView(itemiewID);
        if (itemView != null)
        {
            itemView.GetComponent<Rigidbody>().useGravity = true;
            if (!magicCubeQuestCompleted)
            {
                magicCubeQuestCompleted = true;
                GameObject.Find("GameStateChecker").GetComponent<GameStateChecker>().questCounter += 1;
            }
            isSelected = true;
            resetDropPoint = true;
        }
    }

    [PunRPC]
    public void RPC_RewardGasoil(int itemiewID)
    {
        PhotonView itemView = PhotonNetwork.GetPhotonView(itemiewID);
        if (itemView != null)
        {
            if (!gateQuestCompleted)
            {
                gateQuestCompleted = true;
                GameObject.Find("GameStateChecker").GetComponent<GameStateChecker>().questCounter += 1;
            }
            isSelected = true;
            resetDropPoint = true;
        }
    }

    [PunRPC]
    public void RPC_RewardCem(int itemiewID)
    {
        PhotonView itemView = PhotonNetwork.GetPhotonView(itemiewID);
        if (itemView != null)
        {
            if (!cemetryQuestCompleted)
            {
                cemetryQuestCompleted = true;
                GameObject.Find("GameStateChecker").GetComponent<GameStateChecker>().questCounter += 1;
            }
            isSelected = true;
            resetDropPoint = true;
        }
    }

    [PunRPC]
    public void RPC_RewardBattery(int itemiewID)
    {
        PhotonView itemView = PhotonNetwork.GetPhotonView(itemiewID);
        if (itemView != null)
        {
            if (!batteryQuestCompleted)
            {
                batteryQuestCompleted = true;
                GameObject.Find("GameStateChecker").GetComponent<GameStateChecker>().questCounter += 1;
            }
            isSelected = true;
            resetDropPoint = true;
        }
    }

    [PunRPC]
    public void RPC_RewardPillars(int itemiewID)
    {
        PhotonView itemView = PhotonNetwork.GetPhotonView(itemiewID);
        if (itemView != null)
        {
            if (!pillarsQuestCompleted)
            {
                pillarsQuestCompleted = true;
                GameObject.Find("GameStateChecker").GetComponent<GameStateChecker>().questCounter += 1;
            }
            isSelected = true;
            resetDropPoint = true;
        }
    }

    [PunRPC]
    public void RPC_RewardMaze(int itemiewID)
    {
        PhotonView itemView = PhotonNetwork.GetPhotonView(itemiewID);
        if (itemView != null)
        {
            if (!mazeQuestCompleted)
            {
                mazeQuestCompleted = true;
                GameObject.Find("GameStateChecker").GetComponent<GameStateChecker>().questCounter += 1;
            }

            isSelected = true;
            resetDropPoint = true;

        }
    }

    [PunRPC]
    public void RPC_isSelectedReset()
    {
        resetDropPoint = false;
        isSelected = false;
        //playerAnimator.SetBool("Maquette", false);
    }

    //[PunRPC]
    //public void RPC_EditPickaxePosition(int PickaxeViewID)
    //{
    //    PhotonView PickaxeView = PhotonNetwork.GetPhotonView(PickaxeViewID);
    //    if (PickaxeView != null)
    //    {

    //        PickaxeView.transform.localEulerAngles = new Vector3(180, 0, 100);
    //        PickaxeView.transform.localPosition = new Vector3(0, -0.2f, 0);
    //        isSelected = true;
    //        resetDropPoint = true;
    //    }
    //}

    //[PunRPC]
    //public void RPC_EditAxePosition(int AxeViewID)
    //{
    //    PhotonView AxeView = PhotonNetwork.GetPhotonView(AxeViewID);
    //    if (AxeView != null)
    //    {

    //        AxeView.transform.localEulerAngles = new Vector3(0, 0, 100);
    //        AxeView.transform.localPosition = new Vector3(0, -0.2f, 0);
    //        isSelected = true;
    //        resetDropPoint = true;
    //    }
    //}

    //[PunRPC]
    //public void RPC_ScaleRock(int RockViewID)
    //{
    //    PhotonView RockView = PhotonNetwork.GetPhotonView(RockViewID);
    //    if (RockView != null)
    //    {
    //        RockView.transform.localPosition = new Vector3(0, 0.4f, 0f);
    //        RockView.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
    //    }
    //}

    //[PunRPC]
    //public void RPC_UpdateSelectState()
    //{
    //    if(transform.childCount>=1)
    //    {
    //        transform.GetChild(0).GetComponent<Pickup>().isSelected = true;
    //    }

    //}
}
