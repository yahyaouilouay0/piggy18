using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Cette classe est mise sur tout objet qui sera r�cup�rable par le joueur
/// </summary>

public class Pickup : MonoBehaviour
{
    //Contient l'ic�ne de l'objet r�cup�rer
    public GameObject itemButton;
    //Contient l'�tat de l'objet s'il est dans l'inventaire du joueur ou non
    [HideInInspector]
    public bool picked=false;

    //[HideInInspector]
    //public bool isSelected = false;

    [HideInInspector]
    public Vector3 objectScale;

    //La vue photon de l'objet
    PhotonView view;

    void Awake()
    {
        //R�cup�rer la vue photon de l'objet
        view = GetComponent<PhotonView>();
    }

    private void Start()
    {
        objectScale = transform.localScale;
    }



}
