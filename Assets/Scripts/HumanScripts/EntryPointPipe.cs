using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Cette classe sert � envoyer des objets d'un point � un autre � travers les tubes
/// de communications
/// </summary>
public class EntryPointPipe : MonoBehaviour
{
    //L'endroit o� le joueur va recevoir son objet
    public Transform receivePoint;

    PhotonView view;
    // Start is called before the first frame update
    void Start()
    {
        //R�cup�rer la vu photon de l'objet
        view = GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //Quand un collider entre dans la zone de collision du point D'entr�e
        //V�rifier si c'est un objet avec le tag item
        if(other.CompareTag("item"))
        {
            //V�rifier si l'objet n'est pas dans les mains du joueur
            if(!other.GetComponent<Pickup>().picked)
            {
                //R�cup�rer la vue photon de l'objet en question
                PhotonView photonView = other.GetComponent<PhotonView>();
                //Envoyer un RPC � tout les joueurs pour les informer du changement de position
                view.RPC("SendItemWithPipe", RpcTarget.All, photonView.ViewID);
            }
            
        }
    }

    [PunRPC]
    void SendItemWithPipe(int viewID)
    {
        //Changer la position de l'objet � celle du point de r�ception
        PhotonNetwork.GetPhotonView(viewID).gameObject.transform.position = receivePoint.position;
        //Changer la rotation de l'objet � celle du point de r�ception
        PhotonNetwork.GetPhotonView(viewID).gameObject.transform.rotation = receivePoint.rotation;
    }
}
