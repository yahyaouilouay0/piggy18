using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzlePiece : MonoBehaviour
{
    [SerializeField]
    private Material highlightMaterial;
    private Transform cameraTransform;
    private float dotProductForwardVector, dotProductRightVector;
    private bool isPieceReadyForValidation;
    public bool isValid => isPieceReadyForValidation;

    // Start is called before the first frame update
    void Start()
    {
        cameraTransform = FindObjectOfType<Camera>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        CheckCameraView();
        isPieceReadyForValidation = IsCameraAndPieceAligned();
        //Debug.Log("Forward vector of :" + transform.name + " =" + dotProductForwardVector);
        //Debug.Log("Right vector of :" + transform.name + " =" + Mathf.Abs(dotProductRightVector));
        //if (isPieceReadyForValidation)
        //    Debug.Log(transform.name);
    }

    private bool IsCameraAndPieceAligned()
    {
        var isCameraFacingTheRightSideOfThePiece = dotProductForwardVector < 0.859f && dotProductForwardVector > 0.85f;
        var isCameraAlignedWithThePiece = Mathf.Abs(dotProductRightVector) > 0.003f && Mathf.Abs(dotProductRightVector) < 0.004f;
      
        return isCameraFacingTheRightSideOfThePiece && isCameraAlignedWithThePiece && transform.lossyScale == Vector3.one;
    }

    private void CheckCameraView()
    {
        var forward = cameraTransform.forward;
        dotProductForwardVector = Vector3.Dot(transform.forward, forward);
        dotProductRightVector = Vector3.Dot(transform.right, forward);
    }

    public void Highlight()
    {
        transform.gameObject.GetComponent<MeshRenderer>().material = highlightMaterial;
    }
}
