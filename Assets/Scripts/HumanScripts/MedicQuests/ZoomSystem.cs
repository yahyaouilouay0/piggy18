using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomSystem : MonoBehaviour
{
    [SerializeField]
    private Transform target;
    private Quaternion endRotation;
    private Vector3 endPosition, endScale;

    public void ZoomOnTarget()
    {
        CalculateZoom();
        ApplyZoom();
    }

    private void ApplyZoom()
    {
        var sequence = DOTween.Sequence();
        sequence.Join(transform.DOLocalMove(endPosition, 1));
        sequence.Join(transform.DOScale(endScale, 1));
        sequence.Join(transform.DORotate(endRotation.eulerAngles, 1));
    }

    private void CalculateZoom()
    {
        var scaleFactor =1/target.localScale.x;
        endRotation = Quaternion.Inverse(target.rotation);
        endPosition = endRotation * (-1 * target.localPosition * scaleFactor);
        endScale = transform.localScale * scaleFactor;
    }
}
