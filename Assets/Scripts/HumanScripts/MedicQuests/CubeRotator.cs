using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CubeRotator : MonoBehaviour
{
    [SerializeField]
    public float rotationSpeed = 1f;
    void OnMouseDrag()
    {
        float x = Input.GetAxis("Mouse X") * rotationSpeed;
        float y = Input.GetAxis("Mouse Y") * rotationSpeed;

            transform.Rotate(Vector3.forward, -x);
            transform.Rotate(Vector3.right, -y);
    }

}
