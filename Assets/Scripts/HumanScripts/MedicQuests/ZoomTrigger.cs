using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomTrigger : MonoBehaviour
{
    [SerializeField]
    private ZoomSystem zoomSystem;
    [SerializeField]
    private MagicCubeActivator magicCubeActivator;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && magicCubeActivator.isClicked)
        {
            zoomSystem.ZoomOnTarget();
        }
    }
}
