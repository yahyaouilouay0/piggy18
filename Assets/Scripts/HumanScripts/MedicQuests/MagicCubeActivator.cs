using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicCubeActivator : MonoBehaviour
{
    public bool isClicked;
    public bool isActive;
    public GameObject capsule;
    public GameObject sphere;
    public GameObject container;
    public GameObject Player;
    public GameObject PuzzleCamera;
    public GameObject magicCube;
    public GameObject Piece1;
    public GameObject Piece2;
    public GameObject Piece3;
    private GameObject canvas;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Delay());
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && !isClicked && isActive)
        {
            isClicked = true;
            Player.GetComponentInChildren<Camera>().gameObject.SetActive(false);
            canvas.SetActive(false);
            PuzzleCamera.SetActive(true);
            Piece1.GetComponent<PuzzlePiece>().enabled = true;
            Piece2.GetComponent<PuzzlePiece>().enabled = true;
            Piece3.GetComponent<PuzzlePiece>().enabled = true;
            capsule.SetActive(true);
            sphere.SetActive(false);
            container.SetActive(true); 
            Destroy(transform.GetChild(2).gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("item") && other.gameObject.name == "MagicCubeLocked(Clone)")
        {
            if (!other.GetComponent<Pickup>().picked)
            {
                other.gameObject.SetActive(false);
                magicCube.SetActive(true);
                isActive = true;
            }
                
        }
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.5f);
        Player = GameObject.Find("MedicFPS(Clone)");
        canvas = GameObject.FindObjectOfType<Canvas>().gameObject;
    }
}
