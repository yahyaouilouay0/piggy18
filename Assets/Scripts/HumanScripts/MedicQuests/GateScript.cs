using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GateScript : MonoBehaviour
{
    [SerializeField]
    private GameObject door;
    private void OnTriggerEnter(Collider other)
    {
        if(Vector3.Distance(other.transform.position, transform.GetChild(0).position) < Vector3.Distance(other.transform.position, transform.GetChild(1).position))
        {
            door.SetActive(false);
        }       
    }
}
