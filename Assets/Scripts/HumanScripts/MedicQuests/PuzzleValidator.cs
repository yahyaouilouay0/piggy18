using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PuzzleValidator : MonoBehaviour
{
    private List<PuzzlePiece> puzzlePieces;
    public GameObject Player;
    private GameObject canvas;
    public GameObject PuzzleCamera;

    private bool isPuzzleValid;
    // Start is called before the first frame update
    void Start()
    {
        puzzlePieces = FindObjectsOfType<PuzzlePiece>().ToList();
        StartCoroutine(Delay());
    }

    // Update is called once per frame
    void Update()
    {
        isPuzzleValid = CheckForPuzzlePieceValidation();
        if (isPuzzleValid)
        {
            ValidatePuzzle();
        }
    }

    private void ValidatePuzzle()
    {
        var obstacles = GameObject.FindGameObjectsWithTag("PuzzleObstacles");
        for(int i = 0; i < puzzlePieces.Count; i++)
        {
            puzzlePieces[i].Highlight();
            puzzlePieces.RemoveAt(i);
            Destroy(obstacles[i]);
        }
        for (int i = 0; i < puzzlePieces.Count; i++)
        {
            puzzlePieces[i].Highlight();
        }
        StartCoroutine(AfterSolvingPuzzle());
        
    }

    private bool CheckForPuzzlePieceValidation()
    {
        for(int i = 0; i < puzzlePieces.Count; i++)
        {
            if(!puzzlePieces[i].isValid)
                return false;
        }
        return true;
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.5f);
        if(GameObject.Find("MedicFPS(Clone)"))
        {
            Player = GameObject.Find("MedicFPS(Clone)");
            if(Player.transform.childCount>3)
            {
                canvas = Player.transform.GetChild(3).gameObject;
            }
        }
        
    }

    IEnumerator AfterSolvingPuzzle()
    {
        yield return new WaitForSeconds(5f);
        PuzzleCamera.SetActive(false);
        Player.transform.GetChild(0).gameObject.SetActive(true);
        canvas.SetActive(true);
    }
}
