using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Photon.Pun;
public class GhostQuest : MonoBehaviour
{
    public GameObject powerOrb;
    public bool offeringAccepted;

    private bool rewardProvided;



    void Start()
    {
        rewardProvided = false;
        offeringAccepted = false;

        if (gameObject.transform.childCount > 1)
            gameObject.transform.GetChild(2).gameObject.SetActive(false);
    }

    private void Update()
    {
        if (offeringAccepted && !rewardProvided)
        {
            if (gameObject.transform.childCount > 2)
            {
                gameObject.transform.GetChild(1).gameObject.SetActive(false);
                gameObject.transform.GetChild(2).gameObject.SetActive(true);

                PhotonNetwork.Instantiate(powerOrb.name, new Vector3(gameObject.transform.GetChild(3).transform.position.x, gameObject.transform.GetChild(3).transform.position.y + 0.5f, gameObject.transform.GetChild(3).transform.position.z), Quaternion.identity);
                rewardProvided = true;
            }
        }
    }
}