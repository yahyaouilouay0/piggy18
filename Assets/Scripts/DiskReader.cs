using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class DiskReader : MonoBehaviour
{
    private readonly List<int> lowerBandOrder = new List<int>();
    private readonly List<int> upperBandOrder = new List<int>();
    private readonly List<Color> bandColorOrder = new List<Color>();

    private bool diskPresent;
    private bool dataExtracted;
    private PhotonView view;




    private void Start()
    {
        view = GetComponent<PhotonView>();
        diskPresent = false;
        dataExtracted = false;


    }

    private void Update()
    {
        if(diskPresent && !dataExtracted && GameObject.Find("PrintButton").GetComponent<TicketPrinterButton>().rugLowerInputsFinal.Count > 0)
        {
            view.RPC("ExtractData", RpcTarget.All);
        }
    }



    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.name == "Disk(Clone)")
        {
            diskPresent = true;
        }
    }

    [PunRPC]
    public void ExtractData()
    {
        int startingPoint_Horizontal_Order = 5;
        int startingPoint_Vertical_Order = 10;
        int Horizontal_Counter = 0;
        int Vertical_Counter = 0;
        for (int i = 0; i < 5; i++)
        {
            lowerBandOrder.Add(GameObject.Find("PrintButton").GetComponent<TicketPrinterButton>().rugLowerInputsFinal[i]);
            upperBandOrder.Add(GameObject.Find("PrintButton").GetComponent<TicketPrinterButton>().rugUpperInputsFinal[i]);
        }

        for(int i=0; i<10; i++)
            bandColorOrder.Add(GameObject.Find("PrintButton").GetComponent<TicketPrinterButton>().rugColorsFinal[i]);


        for (int i = 0; i < 10; i++)
        {
            if(i % 2 == 0)
            {
                transform.GetChild(0).transform.GetChild(startingPoint_Horizontal_Order + upperBandOrder[Horizontal_Counter]).GetComponent<BandSetting>().bandColor = bandColorOrder[i];
                transform.GetChild(0).transform.GetChild(startingPoint_Horizontal_Order + upperBandOrder[Horizontal_Counter]).GetComponent<BandSetting>().bandPosition = i;
                transform.GetChild(0).transform.GetChild(startingPoint_Horizontal_Order + upperBandOrder[Horizontal_Counter]).GetComponent<BandSetting>().setBand = true;
                Horizontal_Counter++;
            }
            else
            {
                transform.GetChild(0).transform.GetChild(startingPoint_Vertical_Order + lowerBandOrder[Vertical_Counter]).GetComponent<BandSetting>().bandColor = bandColorOrder[i];
                transform.GetChild(0).transform.GetChild(startingPoint_Vertical_Order + lowerBandOrder[Vertical_Counter]).GetComponent<BandSetting>().bandPosition = i;
                transform.GetChild(0).transform.GetChild(startingPoint_Vertical_Order + lowerBandOrder[Vertical_Counter]).GetComponent<BandSetting>().setBand = true;
                Vertical_Counter++;
            }
        }

        GameObject.Find("RugGame").transform.GetChild(25).gameObject.SetActive(true);
        GameObject.Find("RugControls").transform.GetChild(1).gameObject.SetActive(true);
        dataExtracted = true;
    }
}
