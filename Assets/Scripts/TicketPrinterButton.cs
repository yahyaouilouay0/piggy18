using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class TicketPrinterButton : MonoBehaviour, IInteractable
{
    public bool readyToPrint;
    public bool donePrinting;
    public GameObject disk;
    public readonly List<int> rugLowerInputsFinal = new List<int>();
    public readonly List<int> rugUpperInputsFinal = new List<int>();
    public readonly List<Color> rugColorsFinal = new List<Color>();

    private readonly List<int> rugLowerInputs = new List<int>();
    private readonly List<int> rugUpperInputs = new List<int>();
    private readonly List<Color> rugColors = new List<Color>();
    private PhotonView view;



    private void Start()
    {
        view = GetComponent<PhotonView>();
        readyToPrint = false;
        donePrinting = false;

        for (int i=0; i<5;i++)
        {
            rugLowerInputs.Add(i);
        }
        for (int i = 0; i < 5; i++)
        {
            rugUpperInputs.Add(i);
        }

        rugColors.Add(new Color(0f, 0f, 0f, 1f));
        rugColors.Add(new Color(0f, 0f, 1f, 1f));
        rugColors.Add(new Color(0.5f, 0.5f, 0.5f, 1f));
        rugColors.Add(new Color(1f, 0f, 0f, 1f));
        rugColors.Add(new Color(0.2f, 0.3f, 0.4f, 1f));
        rugColors.Add(new Color(0f, 1f, 1f, 1f));
        rugColors.Add(new Color(1f, 0f, 1f, 1f));
        rugColors.Add(new Color(1f, 1f, 0f, 1f));
        rugColors.Add(new Color(0f, 1f, 0f, 1f));
        rugColors.Add(new Color(0.3f, .75f, 0.7f, 1f));

    }

    private void Update()
    {
        
        if (readyToPrint && !donePrinting)
        {
            PhotonNetwork.Instantiate(disk.name, new Vector3(transform.parent.position.x, transform.parent.position.y + 2.5f, transform.parent.position.z), Quaternion.identity);
            disk.tag = "usedItem";
            int lowerCount = 5;
            int upperCount = 5;
            int colorCount = 10;
            int i;

            for (i = 0; i < 5; i++)
            {
                Random.InitState((int)System.DateTime.Now.Ticks);
                int lowerMagicNumber = Random.Range(0, lowerCount);
                Random.InitState((int)System.DateTime.Now.Millisecond);
                int upperMagicNumber = Random.Range(0, upperCount);
                view.RPC("PrintBandOrder", RpcTarget.All, lowerMagicNumber, upperMagicNumber);
                lowerCount--;
                upperCount--;
            }


            for (i = 0; i < 10; i++)
            {
                Random.InitState((int)System.DateTime.Now.Ticks);
                int colorMagicNumber = Random.Range(0, colorCount);
                view.RPC("PrintBandColor", RpcTarget.All, colorMagicNumber);
                colorCount--;
            }


            donePrinting = true;
            disk.tag = "item";
            
        }
    }

    public void Interact()
    {
        if(transform.parent.GetChild(2).GetComponent<TicketPrinterSecurity>().redOrbPresent && transform.parent.GetChild(3).GetComponent<TicketPrinterSecurity>().greenOrbPresent && transform.parent.GetChild(4).GetComponent<TicketPrinterSecurity>().blueOrbPresent)
            readyToPrint = true;
    }

    [PunRPC]
    public void PrintBandOrder(int lowerMagicNumber, int upperMagicNumber)
    {
        rugLowerInputsFinal.Add(rugLowerInputs[lowerMagicNumber]);
        rugUpperInputsFinal.Add(rugUpperInputs[upperMagicNumber]);
        rugLowerInputs.RemoveAt(lowerMagicNumber);
        rugUpperInputs.RemoveAt(upperMagicNumber);
    }

    [PunRPC]
    public void PrintBandColor(int colorMagicNumber)
    {
        rugColorsFinal.Add(rugColors[colorMagicNumber]);
        rugColors.RemoveAt(colorMagicNumber);
    }
}
