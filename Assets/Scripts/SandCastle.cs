using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class SandCastle : MonoBehaviour
{
    public GameObject crown;
    public bool c8;

    private void Awake()
    {
        c8 = true;
    }

    void Update()
    {
        bool c1 = transform.GetChild(0).GetComponent<MeshRenderer>().enabled;
        bool c2 = transform.GetChild(1).GetComponent<MeshRenderer>().enabled;
        bool c3 = transform.GetChild(2).GetComponent<MeshRenderer>().enabled;
        bool c4 = transform.GetChild(3).GetComponent<MeshRenderer>().enabled;
        bool c5 = transform.GetChild(4).GetComponent<MeshRenderer>().enabled;
        bool c6 = transform.GetChild(5).GetComponent<MeshRenderer>().enabled;
        bool c7 = transform.GetChild(6).GetComponent<MeshRenderer>().enabled;

        if (c1 && c2 && c3 && c4 && c5 && c6 && c7 && c8)
        {
            PhotonNetwork.Instantiate(crown.name, new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 3.5f, gameObject.transform.position.z), Quaternion.identity);
            c8 = false;
        }
            


    }
}
