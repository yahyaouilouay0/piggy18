using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EmoteWheelButtonController : MonoBehaviour
{
    public int Id;
    private Animator anim;
    public string emoteName;
    public TextMeshProUGUI emoteText;
    private bool selected = false;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if(selected)
        {
            emoteText.text = emoteName;
        }
    }

    public void Selected()
    {
        selected = true;
        EmoteWheelController.emoteID = Id;
    }

    public void Deselected()
    {
        selected = false;
        EmoteWheelController.emoteID = 0;
    }

    public void HoverEnter()
    {
        anim.SetBool("Hover", true);
        emoteText.text = emoteName;
        EmoteWheelController.emoteID = 0;
    }

    public void HoverExit()
    {
        anim.SetBool("Hover", false);
        emoteText.text = "";
        EmoteWheelController.emoteID = 0;
    }

}
