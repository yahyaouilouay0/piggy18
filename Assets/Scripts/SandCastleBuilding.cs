using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SandCastleBuilding : MonoBehaviour
{

    public bool sandCastlePartIsBuilt;
    private bool buildingBlockDropped;

    private void Awake()
    {
        sandCastlePartIsBuilt = false;
        buildingBlockDropped = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            buildingBlockDropped = true;
        }
        
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.name == "Sand Pile" && buildingBlockDropped)
        {
            sandCastlePartIsBuilt = true;
            transform.GetComponent<BoxCollider>().isTrigger = false;
            transform.GetComponent<MeshRenderer>().enabled = true;
            other.gameObject.SetActive(false);
            buildingBlockDropped = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        buildingBlockDropped = false;
    }

}
