using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

public class CheckRug : MonoBehaviour, IInteractable
{
    public int checksum;
    public PhotonView view;
    public Animator animator;
    public Material correctAnswerMat;
    public Material wrongAnswerMat;
    public Material defaultMat;

    private readonly String[] largeBands = { "A Band", "B Band", "C Band", "D Band", "E Band", "F Band", "G Band", "H Band", "I Band", "J Band"};
    private readonly String[] smallBands = { "A Band Small", "B Band Small", "C Band Small", "D Band Small", "E Band Small", "F Band Small", "G Band Small", "H Band Small", "I Band Small", "J Band Small"};
    private bool actionDone;
    private Vector3[] initBandPositions = new Vector3[10];


    private void Start()
    {
        for (int i = 0; i < largeBands.Length; i++)
        {
            initBandPositions[i] = GameObject.Find(largeBands[i]).transform.localPosition;
        }
        view = GetComponent<PhotonView>();
        actionDone = true;
        checksum = 0;
    }

    




    public void Interact()
    {
        if(actionDone)
        {
            actionDone = false;
            view.RPC("CheckRugCompletion", RpcTarget.All);
            animator.SetBool("IsPressed", true);
            Invoke(nameof(ResetHandle), 1f);
        }
        
    }

    public void ResetHandle()
    {
        animator.SetBool("IsPressed", false);
        actionDone = true;
    }

    public void SetColorToWhite()
    {
        for (int i = 0; i < largeBands.Length; i++)
        {
            GameObject.Find(largeBands[i]).GetComponent<Renderer>().material = defaultMat;
            GameObject.Find(largeBands[i]).GetComponent<Renderer>().material.color = Color.white;
        }
        
    }

    [PunRPC]
    public void CheckRugCompletion()
    {
        
        for(int i=0; i<largeBands.Length; i++)
        {
            if(GameObject.Find(largeBands[i]).GetComponent<BandSetting>().bandPosition == GameObject.Find(smallBands[i]).GetComponent<BandSetting>().bandPosition)
            {
                if (GameObject.Find(largeBands[i]).GetComponent<BandSetting>().bandColor == GameObject.Find(smallBands[i]).GetComponent<BandSetting>().bandColor)
                    checksum++;
            }
        }

        if (checksum == 10)
        {
            for (int i = 0; i < largeBands.Length; i++)
            {
                GameObject.Find(largeBands[i]).transform.localPosition = initBandPositions[i];
                GameObject.Find(largeBands[i]).GetComponent<Renderer>().material = correctAnswerMat;
                actionDone = false;
            }
        }
        else
        {
            for (int i = 0; i < largeBands.Length; i++)
            {
                GameObject.Find(largeBands[i]).transform.localPosition = initBandPositions[i];
                GameObject.Find(largeBands[i]).GetComponent<BandSetting>().bandPosition = 0;
                GameObject.Find(largeBands[i]).GetComponent<BandSetting>().setBand = false;
                GameObject.Find(largeBands[i]).GetComponent<BandSetting>().bandIsSet = false;
                GameObject.Find(largeBands[i]).GetComponent<Renderer>().material = wrongAnswerMat;
                transform.parent.GetChild(0).GetComponent<SetBandTrait>().bandNumberSet = 0;
            }
            Invoke(nameof(SetColorToWhite), 1f);
        }
    }
}
