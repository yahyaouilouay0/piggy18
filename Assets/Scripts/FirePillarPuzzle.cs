using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
public class FirePillarPuzzle : MonoBehaviour
{
    private bool FireOneActive = false;
    private bool FireTwoActive = false;
    private bool FireThreeActive = false;
    private bool FireFourActive = false;
    private bool FireFiveActive = false;
    private bool FireSixActive = false;


    GameObject Key;

    private void Start()
    {
        Key = GameObject.Find("Key");
    }
    void Update()
    {
        FireOneActive = GameObject.Find("FirePillarOne").gameObject.transform.GetChild(0).gameObject.activeSelf;
        FireTwoActive = GameObject.Find("FirePillarTwo").gameObject.transform.GetChild(0).gameObject.activeSelf;
        FireThreeActive = GameObject.Find("FirePillarThree").gameObject.transform.GetChild(0).gameObject.activeSelf;
        FireFourActive = GameObject.Find("FirePillarFour").gameObject.transform.GetChild(0).gameObject.activeSelf;
        FireFiveActive = GameObject.Find("FirePillarFive").gameObject.transform.GetChild(0).gameObject.activeSelf;
        FireSixActive = GameObject.Find("FirePillarSix").gameObject.transform.GetChild(0).gameObject.activeSelf;

        if (FireOneActive && FireTwoActive && FireThreeActive && FireFourActive && FireFiveActive && FireSixActive)
        {
            gameObject.SetActive(false);
            Key.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        }
        else
            Key.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

    }
}
