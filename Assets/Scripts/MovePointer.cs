using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class MovePointer : MonoBehaviour, IInteractable
{
    public GameObject pointer;
    public PhotonView view;

    private readonly String[] horizontalBands = {"A Band","B Band","C Band","D Band","E Band"};
    private readonly String[] verticalBands = {"J Band","I Band","H Band","G Band","F Band"};
    private bool move;


    private void Start()
    {
        view = GetComponent<PhotonView>();
        pointer = GameObject.Find("Pointer");
        move = false;
    }

    private void Update()
    {
        if (move)
        {
            view.RPC("PointerMovement", RpcTarget.All);
            move = false;
        }
    }

    public void Interact()
    {
        move = true;
    }

    [PunRPC]
    public void PointerMovement()
    {
        pointer = GameObject.Find("Pointer");

        if (gameObject.name == "GoRight")
        {
            Debug.Log("level 2");
            bool switchDone = false;
            Debug.Log(pointer.GetComponent<PointerPosition>().currentBlock);
            Debug.Log(pointer);
            Debug.Log(pointer.GetComponent<PointerPosition>());
            if (Array.Exists(horizontalBands, element => element == pointer.GetComponent<PointerPosition>().currentBlock))
            {
                Debug.Log("level 3");
                for (int i = 0; i < 5; i++)
                {
                    Debug.Log("level 4");
                    if (horizontalBands[i] == pointer.GetComponent<PointerPosition>().currentBlock && i != 4 && !switchDone)
                    {
                        Debug.Log("level 5");
                        pointer.GetComponent<PointerPosition>().currentBlock = horizontalBands[i + 1];
                        pointer.transform.localPosition = new Vector3(pointer.transform.localPosition.x - 2f, pointer.transform.localPosition.y, pointer.transform.localPosition.z);
                        switchDone = true;
                    }
                }

            }

            if (Array.Exists(verticalBands, element => element == pointer.GetComponent<PointerPosition>().currentBlock))
            {
                for (int i = 0; i < 5; i++)
                {
                    if (verticalBands[i] == pointer.GetComponent<PointerPosition>().currentBlock && i != 4 && !switchDone)
                    {
                        pointer.GetComponent<PointerPosition>().currentBlock = verticalBands[i + 1];
                        pointer.transform.localPosition = new Vector3(pointer.transform.localPosition.x, pointer.transform.localPosition.y + 2f, pointer.transform.localPosition.z);
                        switchDone = true;
                    }
                }
            }

            if (pointer.GetComponent<PointerPosition>().currentBlock == "None" && !switchDone)
            {
                pointer.GetComponent<PointerPosition>().currentBlock = "A Band";
                pointer.transform.localPosition = new Vector3(pointer.transform.localPosition.x - 1.7f, pointer.transform.localPosition.y, pointer.transform.localPosition.z);
            }

            if (pointer.GetComponent<PointerPosition>().currentBlock == "F Band" && !switchDone)
            {
                pointer.GetComponent<PointerPosition>().currentBlock = "None";
                pointer.transform.localPosition = new Vector3(pointer.transform.localPosition.x, pointer.transform.localPosition.y + 1.7f, pointer.transform.localPosition.z);
            }
        }
        else if (gameObject.name == "GoLeft")
        {
            bool switchDone = false;

            if (Array.Exists(horizontalBands, element => element == pointer.GetComponent<PointerPosition>().currentBlock))
            {
                for (int i = 4; i > -1; i--)
                {
                    if (horizontalBands[i] == pointer.GetComponent<PointerPosition>().currentBlock && i != 0 && !switchDone)
                    {
                        pointer.GetComponent<PointerPosition>().currentBlock = horizontalBands[i - 1];
                        pointer.transform.localPosition = new Vector3(pointer.transform.localPosition.x + 2f, pointer.transform.localPosition.y, pointer.transform.localPosition.z);
                        switchDone = true;
                    }
                }
            }

            if (Array.Exists(verticalBands, element => element == pointer.GetComponent<PointerPosition>().currentBlock))
            {
                for (int i = 4; i > -1; i--)
                {
                    if (verticalBands[i] == pointer.GetComponent<PointerPosition>().currentBlock && i != 0 && !switchDone)
                    {
                        pointer.GetComponent<PointerPosition>().currentBlock = verticalBands[i - 1];
                        pointer.transform.localPosition = new Vector3(pointer.transform.localPosition.x, pointer.transform.localPosition.y - 2f, pointer.transform.localPosition.z);
                        switchDone = true;
                    }
                }
            }

            if (pointer.GetComponent<PointerPosition>().currentBlock == "None" && !switchDone)
            {
                pointer.GetComponent<PointerPosition>().currentBlock = "F Band";
                pointer.transform.localPosition = new Vector3(pointer.transform.localPosition.x, pointer.transform.localPosition.y - 1.7f, pointer.transform.localPosition.z);
            }

            if (pointer.GetComponent<PointerPosition>().currentBlock == "A Band" && !switchDone)
            {
                pointer.GetComponent<PointerPosition>().currentBlock = "None";
                pointer.transform.localPosition = new Vector3(pointer.transform.localPosition.x + 1.7f, pointer.transform.localPosition.y, pointer.transform.localPosition.z);
            }
        }
    }
 }
