using Newtonsoft.Json;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameStateChecker : MonoBehaviour
{

    private string _url = "https://jokasgame.herokuapp.com/";
    private string jokaId;
    private string zelaId;
    private string engineerId;
    private string medicId;
    private string athleteId;
    private PhotonView view;

    public float maxTime = 20;
    float secondstimer;
    float minutestimer;
    [HideInInspector]
    public float questCounter;
    private float numberOfQuests = 9;
    private Transform progressBar;
    GameObject GlobalTimer;
    string minutesString = "";
    string secondsString = "";
    bool gameEnded = false;
    bool lastminute = false;
    float animationTimer;

    [HideInInspector]
    public bool HumansWin, JokaZelaWin = false;

    // Start is called before the first frame update
    void Start()
    {
        questCounter = 0;
        minutestimer = maxTime;
        secondstimer = 0;
        jokaId = Session.Id;
        zelaId = Session.Id;
        engineerId = Session.Id;
        medicId = Session.Id;
        athleteId = Session.Id;
        HumansWin = true;
        view = GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        if (minutestimer == 0)
        {
            lastminute = true;
        }
        if(GameObject.Find("ProgressBar"))
            progressBar = GameObject.Find("ProgressBar").transform.GetChild(0);
        if (GameObject.Find("GameTimer"))
            GlobalTimer = GameObject.Find("GameTimer");
        if (gameEnded)
        {
            animationTimer += Time.deltaTime;
        }
        else
        {
            secondstimer -= Time.deltaTime;
        }

        if (secondstimer <= 0 && !lastminute)
        {
            secondstimer = 59;
            minutestimer--;
        }
        minutesString = minutestimer.ToString();
        secondsString = Mathf.CeilToInt(secondstimer).ToString();
        if (minutesString.Length == 1)
            minutesString = "0" + minutesString;
        if (secondsString.Length == 1)
            secondsString = "0" + secondsString;

        GlobalTimer.transform.GetChild(0).GetComponent<Text>().text = minutesString;
        GlobalTimer.transform.GetChild(2).GetComponent<Text>().text = secondsString;

        progressBar.GetComponent<Image>().fillAmount = questCounter / numberOfQuests;

        if ((questCounter >= numberOfQuests) && (minutestimer >= 0) && !gameEnded)
        {
            gameEnded = true;
            HumansWin = true;
            JokaZelaWin = false;
            if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 1 ||
             (int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 2 ||
             (int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 3)
            {
                //alors il peut tourner la valve
                view.RPC("EndGame_RPC", RpcTarget.All);
            }
        }
        else
        {
            if (questCounter < numberOfQuests && minutestimer <= 0 && secondstimer <= 0 && !gameEnded)
            {
                gameEnded = true;
                JokaZelaWin = true;
                HumansWin = false;
                if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 0)
                {
                    //alors il peut tourner la valve
                    view.RPC("EndGame_RPC", RpcTarget.All);
                }
            }

        }

        if (animationTimer >= 5)
        {
            if (JokaZelaWin && !HumansWin)
            {
                SceneManager.LoadScene("JokaWon");
            }
            else
            {
                if (!JokaZelaWin && HumansWin)
                {
                    SceneManager.LoadScene("HumansWon");
                }
            }
        }

    }

    [PunRPC]
    public void EndGame_RPC()
    {
        StartCoroutine(PostMatch());
    }

    private IEnumerator PostMatch()
    {
        string idUser = "";
        WWWForm form = new WWWForm();
        switch ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"])
        {
            case 0:
                form.AddField("role", "JOKA");
                form.AddField("win", JokaZelaWin.ToString());
                idUser = jokaId;
                break;
            case 1:
                form.AddField("role", "ENGINEER");
                form.AddField("win", HumansWin.ToString());
                idUser = engineerId;
                break;
            case 2:
                form.AddField("role", "MEDIC");
                form.AddField("win", HumansWin.ToString());
                idUser = medicId;
                break;
            case 3:
                form.AddField("role", "ATHLETE");
                form.AddField("win", HumansWin.ToString());
                idUser = athleteId;
                break;
        }
        Debug.Log("data" + form.data);
        using (UnityWebRequest www = UnityWebRequest.Post(_url + "match/", form))
        {

            yield return www.SendWebRequest();
            if (www.result != UnityWebRequest.Result.Success)
            {

                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Post Match");
                Dictionary<string, string> result = JsonConvert.DeserializeObject<Dictionary<string, string>>(www.downloadHandler.text);
                result.TryGetValue("_id", out string _id);
                StartCoroutine(AssignMatch(idUser, _id));
                Debug.Log(www.downloadHandler.text);
                Debug.Log("Form upload complete!");
            }
        }
    }

    private IEnumerator AssignMatch(string idUser, string _id)
    {

        byte[] myData = System.Text.Encoding.UTF8.GetBytes("This is some test data");
        using (UnityWebRequest request = UnityWebRequest.Put(_url + "match/" + idUser + "/" + _id, myData))
        {
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log(request.error);
            }
            else
            {
                if (HumansWin && !JokaZelaWin)
                {
                    if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 1 ||
                        (int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 2 ||
                        (int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 3)
                    {
                        StartCoroutine(CheckAchievements("Winner winner couscous dinner"));
                    }
                }
                else if (!HumansWin && JokaZelaWin)
                {
                    if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 0)
                    {
                        StartCoroutine(CheckAchievements("Winner winner couscous dinner"));
                    }
                }

                Debug.Log(request.responseCode);
            }
        }
    }

    private IEnumerator CheckAchievements(string achievementName)
    {
        string idUser = "";
        switch ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"])
        {
            case 0:
                idUser = jokaId;
                break;
            case 1:
                idUser = engineerId;
                break;
            case 2:
                idUser = medicId;
                break;
            case 3:
                idUser = athleteId;
                break;
        }

        using (UnityWebRequest request = UnityWebRequest.Get(_url + "achievements/" + idUser + "/" + achievementName))
        {
            yield return request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log(request.error);
            }
            else
            {
                //Debug.Log("Received" + request.downloadHandler.text);
                Dictionary<string, bool> achievements = JsonConvert.DeserializeObject<Dictionary<string, bool>>(request.downloadHandler.text);
                achievements.TryGetValue("result", out bool achichevementCheck);
                if (!achichevementCheck)
                    StartCoroutine(AssignAchievements(idUser, "Winner winner couscous dinner", "You have some skills over there", "SILVER"));
            }
        }
    }

    private IEnumerator AssignAchievements(string idUser, string achievementName, string achievementDescription, string achievementType)
    {

        byte[] myData = System.Text.Encoding.UTF8.GetBytes("This is some test data");
        using (UnityWebRequest request = UnityWebRequest.Put(_url + "achievements/" + idUser + "/" + achievementName, myData))
        {
            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {
                Debug.Log(request.error);
            }
            else
            {
                Transform achievementPanel = GameObject.Find("AchievmentPanel").transform;
                achievementPanel.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/" + achievementType);
                achievementPanel.GetChild(1).GetChild(0).GetComponent<Text>().text = achievementName;
                achievementPanel.GetChild(1).GetChild(1).GetComponent<Text>().text = achievementDescription;
                achievementPanel.GetComponent<Animator>().SetTrigger("startNotif");
                Debug.Log(request.responseCode);
            }
        }
    }


}
