using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;

public class JokaAbilitiesManager : MonoBehaviourPun
{
    [SerializeField]
    private Material transparentMaterial;
    [SerializeField]
    private Material defaultMaterial;

    private PhotonView view;
    [SerializeField]
    private float abilityTime = 8f;
    [SerializeField]
    private Text abilityCoolDownText;
    [SerializeField]
    private Button toxicGazAbilityButton;
    [SerializeField]
    private Text toxicGazAbilityText;
    [SerializeField]
    private float abilityCoolDown = 10f;
    [SerializeField]
    private GameObject[] vfx;
    [SerializeField]
    private Button[] buttons;
    [SerializeField]
    private Text[] texts;

    Animator animator;
    private float abilityCoolDownTimer;
    private float abilityTimeTimer;
    private bool isActive;
    private GameObject jokaGameObject;

    private float timeElapsed;
    private float lerpDuration = 30;
    private float[] sizeToLerp;
    private float[] alphaToLerp;
    private float[] soundToLerp;
    private Vector3[] scaleToLerp;
    private bool[] gazIsAtivated;
    public bool toxicGazIsUsed;
    [HideInInspector]
    public bool abilitiesDeactivated;
    private bool disablingToxic;

    private void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        view = gameObject.GetComponent<PhotonView>();
        jokaGameObject = GameObject.Find("Joka(Clone)");
        vfx = GameObject.FindGameObjectsWithTag("toxicgaz");
        sizeToLerp = new float[vfx.Length];
        alphaToLerp = new float[vfx.Length];
        soundToLerp = new float[vfx.Length];
        scaleToLerp = new Vector3[vfx.Length];
        gazIsAtivated = new bool[vfx.Length];
        for (int i = 0; i < vfx.Length; i++)
        {
            sizeToLerp[i] = vfx[i].GetComponent<VisualEffect>().GetFloat("Size");       
            alphaToLerp[i] = vfx[i].GetComponent<VisualEffect>().GetFloat("Alpha");
            scaleToLerp[i] = vfx[i].transform.GetChild(0).transform.localScale;
            soundToLerp[i] = vfx[i].GetComponent<AudioSource>().volume;
            gazIsAtivated[i] = false;
        }
        if(view.IsMine)
            StartCoroutine(Delay(2));   
    }

    IEnumerator Delay(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        GameObject.Find("EngineerFPS(Clone)").transform.GetChild(0).gameObject.layer = 3;
        GameObject.Find("AthelteFPS(Clone)").transform.GetChild(0).gameObject.layer = 3;
        GameObject.Find("MedicFPS(Clone)").transform.GetChild(0).gameObject.layer = 3;
    }

    private void Update()
    {
        if (view.IsMine){
            if (abilityCoolDownTimer > 0)
            {
                abilityCoolDownTimer -= Time.deltaTime;
                abilityCoolDownText.text = ((int)abilityCoolDownTimer).ToString();
            }
            else
            {
                abilityCoolDownText.text = "";
            }
            if (abilityTimeTimer > 0)
            {
                abilityTimeTimer -= Time.deltaTime;
            }
            else if (isActive)
            {
                isActive = false;
                gameObject.GetComponent<SkinnedMeshRenderer>().material = defaultMaterial;
                base.photonView.RPC("ChangeJokaMaterial", RpcTarget.Others, false);
            }   
        }
        for (int i = 0; i < vfx.Length; i++)
        {

            if (gazIsAtivated[i])
            {
                if (timeElapsed < lerpDuration)
                {
                    sizeToLerp[i] = Mathf.Lerp(1, 100, timeElapsed / lerpDuration);
                    vfx[i].GetComponent<VisualEffect>().SetFloat("Size", sizeToLerp[i]);
                    soundToLerp[i] = Mathf.Lerp(0, 1, timeElapsed / lerpDuration);
                    vfx[i].GetComponent<AudioSource>().volume =  soundToLerp[i];
                    alphaToLerp[i] = Mathf.Lerp(0, 1, timeElapsed / lerpDuration);
                    vfx[i].GetComponent<VisualEffect>().SetFloat("Alpha", alphaToLerp[i]);
                    scaleToLerp[i] = Vector3.Lerp(new Vector3(1f, 1f, 1f), new Vector3(100f, 100f, 100f), timeElapsed / lerpDuration);
                    vfx[i].transform.GetChild(0).transform.localScale = scaleToLerp[i];
                    timeElapsed += Time.deltaTime;
                }
                else 
                {
                    StartCoroutine(DelayAfterGaxIsActivated());
                }
            }
        }
        if (disablingToxic)
        {
            for (int i = 0; i < vfx.Length; i++)
            {

                if (!gazIsAtivated[i])
                {
                        if (timeElapsed < lerpDuration)
                        {
                            //sizeToLerp[i] = Mathf.Lerp(100, 1, timeElapsed / lerpDuration);
                            //vfx[i].GetComponent<VisualEffect>().SetFloat("Size", sizeToLerp[i]);
                            alphaToLerp[i] = Mathf.Lerp(1, 0, timeElapsed / lerpDuration);
                            vfx[i].GetComponent<VisualEffect>().SetFloat("Alpha", alphaToLerp[i]);
                        soundToLerp[i] = Mathf.Lerp(1, 0, timeElapsed / lerpDuration);
                        vfx[i].GetComponent<AudioSource>().volume = soundToLerp[i];
                        scaleToLerp[i] = Vector3.Lerp(new Vector3(100f, 100f, 100f), new Vector3(1f, 1f, 1f), timeElapsed / lerpDuration);
                            vfx[i].transform.GetChild(0).transform.localScale = scaleToLerp[i];
                            timeElapsed += Time.deltaTime;
                        }
                }
            }
        }
        
    }

    IEnumerator DelayAfterGaxIsActivated()
    {
        yield return new WaitForSeconds(20);
        disablingToxic=true;
        for (int i = 0; i < vfx.Length; i++)
        {
            gazIsAtivated[i] = false;
        }
        timeElapsed =  0;
        lerpDuration = 30;
    }


    public void OnClickTurnInvisible()
    {
        if (view.IsMine && abilityCoolDownTimer <=0) {
            isActive = true;
            animator.SetTrigger("isInvisible");
            gameObject.GetComponent<SkinnedMeshRenderer>().material = transparentMaterial;
            //gameObject.transform.GetChild(0).gameObject.GetComponent<Renderer>().material = transparentMaterial;
            base.photonView.RPC("ChangeJokaMaterial", RpcTarget.Others, true);
            abilityCoolDownTimer = abilityCoolDown;
            abilityTimeTimer = abilityTime;
        }  
    }

    public void OnClickToxicGaz()
    {
        if (view.IsMine)
        {
            for (int i = 0; i < vfx.Length; i++)
            {
                vfx[i].GetComponent<VisualEffect>().Play();
                gazIsAtivated[i] = true;
            }
            animator.SetTrigger("toxicGaz");
            toxicGazIsUsed = true;
            toxicGazAbilityButton.interactable = false;
            toxicGazAbilityText.text = "Used";
            base.photonView.RPC("ActivateToxicGaz", RpcTarget.Others);
            
        }
    }

    [PunRPC]
    public void ChangeJokaMaterial(bool isChanged)
    {
        if (isChanged)
        {
            jokaGameObject.SetActive(false);
        }
        else
        {
            jokaGameObject.SetActive(true);
        }     
    }

    [PunRPC]
    public void ActivateToxicGaz()
    {
        for (int i = 0; i < vfx.Length; i++)
        {
            vfx[i].GetComponent<VisualEffect>().Play();
            gazIsAtivated[i] = true;
        }
        
    }

    public void DeactivateAbilities()
    {
        abilitiesDeactivated = false;
        for (int i = 0;i < buttons.Length; i++)
        {
            buttons[i].interactable = false;
            buttons[i].GetComponentInChildren<Text>().text = "Disabled";
        }
    }

    public void ActivateAbilities()
    {
        abilitiesDeactivated = false;
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].interactable = true;
        }
        //texts[0].text = "Stun";
        //texts[1].text = "Slow";
        //texts[2].text = "Burn";
        //texts[4].text = "Invisible";
        //if(toxicGazIsUsed)
        //    texts[3].text = "Used";
        //else
        //    texts[3].text = "Out Of Range";
    }
}
