using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using System;

public class JokaTrapsManager : MonoBehaviour
{
    [SerializeField]
    private GameObject slowTrapPrefab;
    [SerializeField]
    private GameObject burnTrapPrefab;
    [SerializeField]
    private GameObject stunTrapPrefab;

    //[SerializeField]
    //private KeyCode newSlowTrapPrefab = KeyCode.E;
    //[SerializeField]
    //private KeyCode newBurnTrapPrefab = KeyCode.R;
    //[SerializeField]
    //private KeyCode newStunTrapPrefab = KeyCode.A;

    [SerializeField]
    private float slowTrapCd = 10f;
    [SerializeField]
    private float burnTrapCd = 10f; 
    [SerializeField]
    private float stunTrapCd = 10f;

    private float slowTrapCdTimer = 0f;
    private float burnTrapCdTimer = 0f;
    private float stunTrapCdTimer = 0f;

    [SerializeField]
    private float trapRange = 20f;

    bool BurnTrapButton;

    [SerializeField]
    private Text burnTrapText;
    [SerializeField]
    private Text slowTrapText;
    [SerializeField]
    private Text stunTrapText;

    [SerializeField]
    private Material trapInstanceMaterial;
    [SerializeField]
    private Material trapOutOfRangeMaterial;

    Animator animator;
    private bool trapPlaced;
    private bool burnTrapInstantiated;
    private bool slowTrapInstantiated;
    private bool stunTrapInstantiated;
    private bool trapInstanceInRange;

    private GameObject currentTrapObject;
    private Material currentTrapObjectMaterial;
    private PhotonView view;

    private float mouseWheelRotation;

    private void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        view = GetComponent<PhotonView>();
    }
    private void Update()
    {
        if (view.IsMine) { 
            HandleNewSlowTrapHotkey();
            HandleNewStunTrapHotkey();
            HandleNewBurnTrapHotkey();

            if (currentTrapObject != null)
            {
                MoveCurrentObjectToMouse();
                RotateFromMouseWheel();
                ReleaseIfClicked();
            }

            HandleBurnTrapCd();
            HandleSlowTrapCd();
            HandleStunTrapCd();
        }

    }

    /**************************************************
                    BUTN TRAP MANAGEMENT
    **************************************************/
    /// <summary>
    ///     Test if the user is using the hotkey for burn trap and decrement the cooldown on the spell
    ///     if it is on cooldown
    /// </summary>
    private void HandleNewBurnTrapHotkey()
    {
        if(burnTrapCdTimer > 0)
        {
            burnTrapCdTimer -= Time.deltaTime;
            burnTrapText.text = Math.Truncate(burnTrapCdTimer).ToString();
        }
        else
        {
            burnTrapText.text = "";
        }

        //if (Input.GetKeyDown(newBurnTrapPrefab))
        //{
        //    enableBurnTrap();
        //}
    }

    /// <summary>
    ///     Enable the burn trap by creating an instance of the trap will follow the mouse cursor until user confirm the instantiation 
    ///     or destroy it
    /// </summary>
    public void enableBurnTrap()
    {
        if (currentTrapObject != null)
        {
            PhotonNetwork.Destroy(currentTrapObject);
        }
        else if (burnTrapCdTimer <= 0)
        {
            currentTrapObject = PhotonNetwork.Instantiate(burnTrapPrefab.name, Input.mousePosition, Quaternion.identity);
            currentTrapObjectMaterial = currentTrapObject.GetComponent<Renderer>().material;
            burnTrapInstantiated = true;
        }
    }

    /// <summary>
    ///      Manage when the Burn trap will be on cd
    /// </summary>
    void HandleBurnTrapCd()
    {
        if (trapPlaced && burnTrapInstantiated)
        {
            burnTrapCdTimer = burnTrapCd;
            burnTrapInstantiated = false;
            trapPlaced = false;
        }
    }

    /**************************************************
                    STUN TRAP MANAGEMENT
    **************************************************/
    /// <summary>
    ///     Test if the user is using the hotkey for stun trap and decrement the cooldown on the spell
    ///     if it is on cooldown
    /// </summary>
    private void HandleNewStunTrapHotkey()
    {

        if (stunTrapCdTimer > 0)
        {
            stunTrapCdTimer -= Time.deltaTime;
            stunTrapText.text = Math.Truncate(stunTrapCdTimer).ToString();
        }
        else
        {
            stunTrapText.text = "";
        }


        //if (Input.GetKeyDown(newStunTrapPrefab))
        //{
        //    enableStunTrap();
        //}
    }
    /// <summary>
    ///     Enable the stun trap by creating an instance of the trap will follow the mouse cursor until user confirm the instantiation 
    ///     or destroy it
    /// </summary>
    public void enableStunTrap()
    {
        if (currentTrapObject != null)
        {
            PhotonNetwork.Destroy(currentTrapObject);
        }
        else if (stunTrapCdTimer <= 0)
        {
            currentTrapObject = PhotonNetwork.Instantiate(stunTrapPrefab.name, Input.mousePosition, Quaternion.identity);
            currentTrapObjectMaterial = currentTrapObject.GetComponent<Renderer>().material;
            stunTrapInstantiated = true;
        }
    }
    /// <summary>
    ///      Manage when the stun trap will be on cd
    /// </summary>
    void HandleStunTrapCd()
    {
        if (trapPlaced && stunTrapInstantiated)
        {
            stunTrapCdTimer = stunTrapCd;
            stunTrapInstantiated = false;
            trapPlaced = false;
        }
    }

    /**************************************************
                    SLOW TRAP MANAGEMENT
    **************************************************/
    ///<summary>
    ///     Test if the user is using the hotkey for slow trap and decrement the cooldown on the spell
    ///     if it is on cooldown
    /// </summary>
    private void HandleNewSlowTrapHotkey()
    {
        if (slowTrapCdTimer > 0)
        {
            slowTrapCdTimer -= Time.deltaTime;
            slowTrapText.text = Math.Truncate(slowTrapCdTimer).ToString();
        }
        else
        {
            slowTrapText.text = "";
        }

        //if (Input.GetKeyDown(newSlowTrapPrefab))
        //{
        //    enableSlowTrap();
        //}
    }
    /// <summary>
    ///     Enable the slow trap by creating an instance of the trap will follow the mouse cursor until user confirm the instantiation 
    ///     or destroy it
    /// </summary>
    public void enableSlowTrap()
    {
        if (currentTrapObject != null)
        {
            PhotonNetwork.Destroy(currentTrapObject);
        }
        else if (slowTrapCdTimer <= 0)
        {
            currentTrapObject = PhotonNetwork.Instantiate(slowTrapPrefab.name, Input.mousePosition, Quaternion.identity);
            currentTrapObjectMaterial = currentTrapObject.GetComponent<Renderer>().material;
            slowTrapInstantiated = true;
        }
    }
    /// <summary>
    ///      Manage when the slow trap will be on cd
    /// </summary>
    void HandleSlowTrapCd()
    {
        if (trapPlaced && slowTrapInstantiated)
        {
            slowTrapCdTimer = slowTrapCd;
            slowTrapInstantiated = false;
            trapPlaced = false;
        }
    }
    /// <summary>
    ///     This method will make the instance of the trap follow the mouse curser until he release it or destroy it
    ///     Change the material of the trap depending on its location (green in range/red out range)
    /// </summary>
    private void MoveCurrentObjectToMouse()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo))
        {
            currentTrapObject.transform.position = hitInfo.point;
            currentTrapObject.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);
            if (Vector3.Distance(hitInfo.point, transform.position) < trapRange)
            {
                trapInstanceInRange = true;
                currentTrapObject.GetComponent<Renderer>().material = trapInstanceMaterial;
            }
            else
            {
                trapInstanceInRange = false;
                currentTrapObject.GetComponent<Renderer>().material = trapOutOfRangeMaterial;
            }
        }
    }
    /// <summary>
    ///     this method will make the instance of the trap rotate while the user is scrolling the mouse wheel
    /// </summary>
    private void RotateFromMouseWheel()
    {
        mouseWheelRotation += Input.mouseScrollDelta.y;
        currentTrapObject.transform.Rotate(Vector3.up, mouseWheelRotation * 10f);
    }
    /// <summary>
    ///     this method will confirm the trap location by releasing the instance if its in range
    /// </summary>
    private void ReleaseIfClicked()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (trapInstanceInRange)
            {
                currentTrapObject.GetComponent<Renderer>().material = currentTrapObjectMaterial;
                currentTrapObject = null;
                animator.SetTrigger("placeTrap");
                trapPlaced = true;
            }
            else
            {
                PhotonNetwork.Destroy(currentTrapObject);
            }
        }
        
    }

    

}
