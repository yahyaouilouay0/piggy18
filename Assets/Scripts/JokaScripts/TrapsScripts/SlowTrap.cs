using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowTrap : MonoBehaviour
{
    [SerializeField]
    private float trapRange = 2f;
    [SerializeField]
    private LayerMask targerMask;
    [SerializeField]
    private float debuffLength;

    private GameObject playerHit;
    private float debuffTimer;
    private bool playerInRange;
    private float opacity = 1f;
    private Color trapColor;

    private void Start()
    {
        StartCoroutine(trapCheckRoutine());
    }

    private void Update()
    {
        if (playerInRange)
        {
            //Handle the debuff couldown on the human who got hit then destroy the trap from the scene
            if (debuffTimer > 0)
            {
                debuffTimer -= Time.deltaTime;
                if (opacity >= 0)
                {
                    opacity -= 0.005f;
                }
                gameObject.GetComponent<Renderer>().material.color = new Color(trapColor.r, trapColor.g, trapColor.b, opacity);
            }
            else
            {
                playerHit.GetComponent<PlayerMovement>().speed *= 4;
                gameObject.SetActive(false);
                //PhotonNetwork.Destroy(gameObject);
            }
        }
    }

    /// <summary>
    ///     Routine make the trap check is a human in range evry 0.2 second
    /// </summary>
    /// <returns>Delay time between checks</returns>
    private IEnumerator trapCheckRoutine()
    {
        WaitForSeconds delayTime = new WaitForSeconds(0.2f);

        while (true)
        {
            yield return delayTime;

            lookForHumans();
        }
    }

    /// <summary>
    ///     The trap will check for every game object with have human layer by using a sphere with a modified range 
    /// </summary>
    private void lookForHumans()
    {
        Collider[] interractions = Physics.OverlapSphere(transform.position, trapRange, targerMask);

        if (interractions.Length != 0 && !playerHit)
        {
            Debug.Log("Someone hit");
            trapColor = gameObject.GetComponent<Renderer>().material.color;
            playerHit = interractions[0].gameObject;
            playerHit.GetComponent<PlayerMovement>().speed /= 4;
            playerInRange = true;
            debuffTimer = debuffLength;
            //GameObject.FindObjectOfType<AudioManager>().Play("SlowTrap");
            PhotonView view = gameObject.GetComponent<PhotonView>();
            view.RPC("Play3d", RpcTarget.All, view.ViewID);
        }
    }

    [PunRPC]
    public void Play3d(int id)
    {
        PhotonNetwork.GetPhotonView(id).gameObject.GetComponent<AudioSource>().Play();
    }
}