using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Cette classe permet � joka de saboter les tubes de communication
/// ainsi rendre les deux points d'entr�e d�sactiv�
/// </summary>
public class CommunicationSabotage : MonoBehaviourPun
{
    // Le point d'entr�e 1 du tube de communication
    public GameObject entryPoint1;
    // Le point d'entr�e 2 du tube de communication
    public GameObject entryPoint2;

    //L'�tat de la valve si elle peut tourner ou non
    bool canTurn=false;
    //Le GO de la valve
    GameObject valve;
    //La vue du GO qui contient le script
    PhotonView view;
    //L'animateur qui lance l'animation de la valve
    Animator valveAnimation;
    

    private void Awake()
    {
        //R�cup�rer la PhotonView du GO
        view = GetComponent<PhotonView>();
    }

    // Start is called before the first frame update
    void Start()
    {
        //R�cup�rer la valve � tourner pour fermer les communications
        valve = transform.GetChild(2).gameObject;
        //R�cup�rer l'animateur sur la valve
        valveAnimation = valve.GetComponent<Animator>();
    }

    //Update is called once per frame
    void Update()
    {
        //V�rifier si Joka est � cot� de la valve 
        // et qu'il a appuiy� sur le bouton F
        if (Input.GetKeyDown(KeyCode.F)&&
            canTurn &&
            (int)PhotonNetwork.LocalPlayer.CustomProperties["Role"]==0)
        {
            //Envoyer un RPC � tous les joueurs pour modifiers l'�tat
            //des points d'entr�e et lancer l'animation
            view.RPC("TurnValve", RpcTarget.All);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        //Si Joka est entr� dans la zone de collision
        if(other.gameObject.name.ToUpper().Contains("JOKA"))
        {
            //alors il peut tourner la valve
            canTurn = true;
        }  
    }

    private void OnTriggerExit(Collider other)
    {
        //Si Joka sort de la zone de collision
        if (other.gameObject.name.ToUpper().Contains("JOKA"))
        {
            //alors il ne peut plus tourner la valve
            canTurn = false;
        }
    }

    [PunRPC]
    public void TurnValve()
    {
        //Lancer l'animation (valve qui tourne)
        valveAnimation.SetTrigger("click");
        //D�sactiver le point d'entr�e 1
        entryPoint1.SetActive(false);
        //D�sactiver le point d'entr�e 2
        entryPoint2.SetActive(false);
    }
}
