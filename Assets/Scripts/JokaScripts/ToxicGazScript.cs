using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToxicGazScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !other.gameObject.name.Equals("Joka(Clone)"))
        {
            other.gameObject.GetComponent<Human>().isDying = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && !other.gameObject.name.Equals("Joka(Clone)"))
        {
            other.gameObject.GetComponent<Human>().isDying = false;
        }
    }
}
