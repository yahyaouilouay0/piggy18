using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToxicGazTriggerArea : MonoBehaviour
{
    private GameObject joka;
    private void Update()
    {
        if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 0)
        {
            joka = GameObject.Find("Joka(Clone)");
        }
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 0 && other.gameObject.CompareTag("Player")){
            if (!joka.GetComponent<JokaAbilitiesManager>().toxicGazIsUsed)
            {
                joka.transform.GetChild(2).transform.GetChild(3).GetComponent<Button>().interactable = true;
            }
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] == 0 && other.gameObject.CompareTag("Player"))
        {
            if (!joka.GetComponent<JokaAbilitiesManager>().toxicGazIsUsed)
            {
                joka.transform.GetChild(2).transform.GetChild(3).GetComponent<Button>().interactable = false;
            }
            
        }
    }
}
