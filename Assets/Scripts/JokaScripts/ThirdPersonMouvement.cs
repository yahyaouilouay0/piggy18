using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using Cinemachine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(PhotonView))]
[RequireComponent(typeof(PhotonTransformView))]
public class ThirdPersonMouvement : MonoBehaviour, IOnEventCallback
{
    PhotonView view;

    [Range(4, 100)]
    public float speed = 4f;

    [Range(4, 100)]
    public float flySpeed = 10f;

    [Range(0, 1)]
    public float turnSmoothing = 0.05f;

    public float sens = 700f;

    CharacterController controller;
    float turnSmoothVelocity;
    Transform playerCameraTransform;
    Animator animator;
    public CinemachineFreeLook camRef;

    void Start()
    {
        animator = gameObject.GetComponent<Animator>();
        view = gameObject.GetComponent<PhotonView>();
        if (!view.IsMine)
        {
            
            Destroy(GetComponentInChildren<Camera>().gameObject);
            Destroy(GetComponentInChildren<Cinemachine.CinemachineFreeLook>().gameObject);
            Destroy(GetComponentInChildren<Canvas>().gameObject);
            GameObject.FindObjectOfType<AudioManager>().Play("Theme");
            GameObject.FindObjectOfType<AudioManager>().Play("Ocean");
        }
        else
        {
            Cursor.visible = false;
            GameObject.FindObjectOfType<AudioManager>().Play("Theme");
            GameObject.FindObjectOfType<AudioManager>().Play("Ocean");
            playerCameraTransform = GetComponentInChildren<Camera>().gameObject.transform;
            controller = GetComponent<CharacterController>();
        }
    }

    void Update()
    {
        if (view.IsMine)
        {
            if (Input.GetKeyDown(KeyCode.LeftAlt))
            {
                Cursor.visible = !Cursor.visible;
            }
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            if (Input.GetKey("space"))
            {
                animator.SetBool("isFlying", true);
                controller.Move(Vector3.up * flySpeed * Time.deltaTime);
            }
            else
            {
                animator.SetBool("isFlying", false);
            }
            if (Input.GetKey(KeyCode.LeftControl))
            {
                animator.SetBool("isFalling", true);
                controller.Move(Vector3.down * flySpeed * Time.deltaTime);
            }
            else
            {
                animator.SetBool("isFalling", false);
            }


            //Mouvement et Rotation
            //La direction est fix� selon la direction de cam�ra
            Vector3 direction = playerCameraTransform.transform.right * horizontal + playerCameraTransform.transform.forward * vertical;
            direction.y = 0f;
            direction = direction.normalized;

            if (direction.magnitude >= 0.1f)
            {

                //Ajouter une animation pour la rotation
                float movingAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
                float angleSmoothed = Mathf.SmoothDampAngle(transform.eulerAngles.y, movingAngle, ref turnSmoothVelocity, turnSmoothing);
                transform.rotation = Quaternion.Euler(0f, angleSmoothed, 0f);

                //Appliquer le mouvement en utilisant la classe charcter controller
                Vector3 moveDirection = Quaternion.Euler(0f, movingAngle, 0f) * Vector3.forward;
                controller.Move(moveDirection.normalized * speed * Time.deltaTime);

            }

            if (!Input.GetKey(KeyCode.Mouse0))
            {
                camRef.m_XAxis.m_MaxSpeed = 0f;
                camRef.m_YAxis.m_MaxSpeed = 0f;
                Cursor.visible = true;
            }
            else
            {
                camRef.m_XAxis.m_MaxSpeed = sens;
                camRef.m_YAxis.m_MaxSpeed = 4f;
                Cursor.visible = false;
            }


        }
    }

    public void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
    }

    public void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
    }

    public void OnEvent(EventData photonEvent)
    {
        byte eventCode = photonEvent.Code;
        if (eventCode == 1)
        {
            object[] data = (object[])photonEvent.CustomData;

            PhotonView viewGameObject = PhotonNetwork.GetPhotonView((int)data[0]);
            PhotonView viewDropPoint = PhotonNetwork.GetPhotonView((int)data[1]);
            Transform child = viewGameObject.gameObject.transform;
            Transform dropPoint = viewDropPoint.gameObject.transform;
            child.SetParent(dropPoint);
            child.gameObject.SetActive(true);
            child.position = dropPoint.position;
            child.rotation = dropPoint.rotation;
            child.gameObject.GetComponent<Rigidbody>().isKinematic = true;

        }

        if (eventCode == 2)
        {
            object[] data = (object[])photonEvent.CustomData;

            PhotonView viewGameObject = PhotonNetwork.GetPhotonView((int)data[0]);
            Transform child = viewGameObject.gameObject.transform;

            child.SetParent(null);
            child.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            child.gameObject.SetActive(false);

        }
        if (eventCode == 3)
        {
            object[] data = (object[])photonEvent.CustomData;

            PhotonView viewGameObject = PhotonNetwork.GetPhotonView((int)data[0]);
            Vector3 dropPointPos = (Vector3)data[1];

            Transform itemToDrop = viewGameObject.gameObject.transform;
            itemToDrop.SetParent(null);
            itemToDrop.gameObject.GetComponent<Pickup>().picked = false;
            Rigidbody rbItemToDrop = itemToDrop.gameObject.GetComponent<Rigidbody>();
            rbItemToDrop.isKinematic = false;
            rbItemToDrop.transform.position = dropPointPos;

        }
    }
}
