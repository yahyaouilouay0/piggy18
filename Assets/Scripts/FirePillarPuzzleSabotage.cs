using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class FirePillarPuzzleSabotage : MonoBehaviour, IJokaInteractable
{
    public float cageCooldown = 15f;
    public float cageTimer = 10f;

    PhotonView view;

    [SerializeField]
    private bool cageTriggered = false;
    private float initialCagetimer;
    private float initialCageCooldown;





    private void Awake()
    {
        gameObject.transform.GetChild(0).gameObject.SetActive(false);
        gameObject.transform.GetChild(1).gameObject.SetActive(false);
        gameObject.transform.GetChild(2).gameObject.SetActive(false);
        gameObject.transform.GetChild(3).gameObject.SetActive(false);
        gameObject.transform.GetChild(4).gameObject.SetActive(false);
        gameObject.transform.GetChild(5).gameObject.SetActive(false);

        view = GetComponent<PhotonView>();
        initialCagetimer = cageTimer;
        initialCageCooldown = cageCooldown;
        cageCooldown = 0;
        
    }

    private void Update()
    {
        if(cageTriggered)
        {
            if(cageTimer>0)
            {
                cageTimer -= Time.deltaTime;
            }
            else
            {
                gameObject.transform.GetChild(0).gameObject.SetActive(false);
                gameObject.transform.GetChild(1).gameObject.SetActive(false);
                gameObject.transform.GetChild(2).gameObject.SetActive(false);
                gameObject.transform.GetChild(3).gameObject.SetActive(false);
                gameObject.transform.GetChild(4).gameObject.SetActive(false);
                gameObject.transform.GetChild(5).gameObject.SetActive(false);

                cageTriggered = false;
                cageTimer = initialCagetimer;
            }
        }

        if (!cageTriggered && (cageCooldown > 0))
            cageCooldown -= Time.deltaTime;
        else if (cageCooldown <= 0)
            cageCooldown = 0;


    }


    void IJokaInteractable.Interact()
    {
        view.RPC("InteractionRPC", RpcTarget.All);
    }

    [PunRPC]
    public void InteractionRPC()
    {
        if(cageCooldown<=0)
        {
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
            gameObject.transform.GetChild(1).gameObject.SetActive(true);
            gameObject.transform.GetChild(2).gameObject.SetActive(true);
            gameObject.transform.GetChild(3).gameObject.SetActive(true);
            gameObject.transform.GetChild(4).gameObject.SetActive(true);
            gameObject.transform.GetChild(5).gameObject.SetActive(true);

            cageTriggered = true;
            cageCooldown = initialCageCooldown;
        }
    }
}
