using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Cette classe contient le script de toutes les interactions entre les humains et 
/// les objets dans le jeu
/// Interaction int�gr� :
/// -Interactive avec les livres.
/// </summary>
public class InteractWithItem : MonoBehaviour
{
    public int bandageHeal=250;
    public Animator playerAnimator;
    //
    public float throwForce = 10f;
    //Les coordon�es � partir des quelle le joueur va jeter son objet
    public Transform dropPoint;
    //le panel qui affiche le UI du livre
    public GameObject panel;
    //le panel qui affiche le UI du Crafting syste
    public GameObject CraftingUI;

    public Transform playerInventory;
    //La zone de saisie o� le joueur va �crire ses notes
    public InputField zoneDeSaisie;

    //La vu photon du gameobject
    PhotonView view;

    [HideInInspector]
    public bool isCrafting=false;
    public GameObject invNotification;

    //Pour r�cuperer l'objet qui est dans les mains du joueurs L39
    GameObject selectedGameObject;

    public Sprite defaultSlot;

    private GameObject currentObjectToPlace;
    private List<Material> currentObjectToPlaceMaterialList = new List<Material>();
    private bool ObjectToPlaceInstantiated;
    private bool ObjectToPlaceInstanceInRange;
    [SerializeField]
    private float ObjectToPlaceRange = 20f;
    [SerializeField]
    private Material ObjectToPlaceInstanceMaterial;
    [SerializeField]
    private Material ObjectToPlaceOutOfRangeMaterial;
    // Start is called before the first frame update
    void Start()
    {
        //R�cuperer la vu photon du gameobject
        view = GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        //Si c'est ma vu photon
        if(view.IsMine)
        {
            if(!isCrafting)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    //Si j'ai un objet dans les mains
                    if (dropPoint.childCount > 0)
                    {
                        //Recuperer l'objet que j'ai dans les mains
                        selectedGameObject = dropPoint.GetChild(0).gameObject;
                        //Si l'objet est un livre (notebook)
                        if (selectedGameObject.name.ToUpper().Contains("NOTEBOOK"))
                        {
                            //Si le joueur n'a pas encore ouvert le livre
                            if (!selectedGameObject.GetComponent<NotesSave>().isOpen)
                            {
                                //remettre le curseur active
                                Cursor.lockState = CursorLockMode.None;
                                //afficher le UI du livre
                                panel.SetActive(true);
                                //Rendre la zone de saisie active
                                zoneDeSaisie.ActivateInputField();
                                //Changer l'�tat d'�criture en vrai pour les mouvement du joueur
                                //pour bloquer le mouvement par la suite
                                GetComponent<PlayerMovement>().isWriting = true;
                                //Changer l'�tat d'�criture en vrai pour la cam�ra du joueur
                                //pour bloquer la cam�ra par la suite
                                //GameObject.Find("Main Camera").GetComponent<MouseLook>().isWriting = true;
                                //R�cup�rer le text du livre et le mettre dans la zone de saisie
                                zoneDeSaisie.text = selectedGameObject.GetComponent<NotesSave>().text;
                                //Changer l'�tat du livre en "Ouvert" pour ne pas l'ouvrir une autre fois
                                selectedGameObject.GetComponent<NotesSave>().isOpen = true;


                            }
                        }

                        if (selectedGameObject.name.ToUpper().Contains("MIRROR STICK"))
                        {
                            enablePlacingObject();
                            
                        }

                        if (selectedGameObject.name.ToUpper().Contains("BALL"))
                        {
                            playerAnimator.SetTrigger("Throw");
                            Inventory inventory = GetComponent<Inventory>();
                            if (inventory.lastSelected.childCount > 0)
                            {
                                //Si c'est le cas le d�truire
                                Destroy(inventory.lastSelected.GetChild(0).gameObject);
                            }
                            inventory.isSelected[inventory.lastSelected.GetComponent<Slot>().slotIndex] = false;
                            inventory.lastSelected.gameObject.GetComponent<Image>().sprite = defaultSlot;
                            view.RPC("RPC_ThrowBall",RpcTarget.All, selectedGameObject.GetComponent<PhotonView>().ViewID);

                        }

                        if (selectedGameObject.name.ToUpper().Contains("BANDAGE"))
                        {
                            if (!(GetComponent<Human>().health > GetComponent<Human>().initialHealth))
                            {
                                Inventory inventory = GetComponent<Inventory>();
                                if (inventory.lastSelected.childCount > 0)
                                {
                                    //Si c'est le cas le d�truire
                                    Destroy(inventory.lastSelected.GetChild(0).gameObject);
                                }
                                inventory.isSelected[inventory.lastSelected.GetComponent<Slot>().slotIndex] = false;
                                inventory.lastSelected.gameObject.GetComponent<Image>().sprite = defaultSlot;
                                view.RPC("RPC_UseBandage", RpcTarget.All);
                            }
                            

                        }

                    }
                }
            }
            if (currentObjectToPlace != null)
            {
                MoveCurrentObjectToMouse();
                ReleaseIfClicked();
            }
        }
    }

    

    private void enablePlacingObject()
    {
        if (currentObjectToPlace != null)
        {
            PhotonNetwork.Destroy(currentObjectToPlace);
        }
        else 
        {

            currentObjectToPlace = PhotonNetwork.Instantiate("Mirror stick", Input.mousePosition, Quaternion.identity);
            currentObjectToPlace.transform.GetChild(1).tag="Untagged";
            currentObjectToPlace.GetComponent<BoxCollider>().isTrigger = false;
            //currentObjectToPlace.GetComponent<BoxCollider>().size = new Vector3(1, 1, 1);
            //Destroy(currentObjectToPlace.GetComponent<Rigidbody>());

            currentObjectToPlace.GetComponent<Rigidbody>().detectCollisions = false;
            currentObjectToPlace.GetComponent<Rigidbody>().useGravity = false;

            currentObjectToPlaceMaterialList.Add(currentObjectToPlace.transform.GetChild(0).GetComponent<Renderer>().material);
            currentObjectToPlaceMaterialList.Add(currentObjectToPlace.transform.GetChild(1).GetComponent<Renderer>().material);
            ObjectToPlaceInstantiated = true;
        }
    }

    private void MoveCurrentObjectToMouse()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        LayerMask mask = LayerMask.GetMask("IgnoreRayCast");
        if (Physics.Raycast(ray, out hitInfo,mask))
        {
            currentObjectToPlace.transform.position = hitInfo.point;
            //currentObjectToPlace.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitInfo.normal);
            if (Vector3.Distance(hitInfo.point, transform.position) < ObjectToPlaceRange)
            {
                ObjectToPlaceInstanceInRange = true;
                currentObjectToPlace.transform.GetChild(0).GetComponent<Renderer>().material = ObjectToPlaceInstanceMaterial;
                currentObjectToPlace.transform.GetChild(1).GetComponent<Renderer>().material = ObjectToPlaceInstanceMaterial;

            }
            else
            {
                ObjectToPlaceInstanceInRange = false;
                currentObjectToPlace.transform.GetChild(0).GetComponent<Renderer>().material = ObjectToPlaceOutOfRangeMaterial;
                currentObjectToPlace.transform.GetChild(1).GetComponent<Renderer>().material = ObjectToPlaceOutOfRangeMaterial;

            }
           
        }
    }


    private void ReleaseIfClicked()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (ObjectToPlaceInstanceInRange)
            {
                currentObjectToPlace.transform.GetChild(1).tag = "Mirror";
                currentObjectToPlace.transform.GetChild(0).GetComponent<Renderer>().material = currentObjectToPlaceMaterialList[0];
                currentObjectToPlace.transform.GetChild(1).GetComponent<Renderer>().material = currentObjectToPlaceMaterialList[1];
                currentObjectToPlace.GetComponent<BoxCollider>().isTrigger = true;
                currentObjectToPlace.GetComponent<Rigidbody>().detectCollisions = true;
                currentObjectToPlace.GetComponent<Rigidbody>().isKinematic = true;

                Inventory inventory = GetComponent<Inventory>();
                inventory.lastSelected.gameObject.GetComponent<Image>().sprite = defaultSlot;
                //V�rifier que j'ai toujours l'ic�ne de l'objet dans mon menu d'inventaire
                if (inventory.lastSelected.childCount > 0)
                {
                    //Si c'est le cas le d�truire
                    Destroy(inventory.lastSelected.GetChild(0).gameObject);
                }
                //Mettre l'�tat isSelected du slot concern� � faux
                inventory.isSelected[inventory.lastSelected.GetComponent<Slot>().slotIndex] = false;
                PhotonNetwork.Destroy(dropPoint.GetChild(0).gameObject);

                currentObjectToPlace = null;
                currentObjectToPlaceMaterialList.Clear();
            }
            else
            {
                PhotonNetwork.Destroy(currentObjectToPlace);
            }
        }
    }


    //Cette fonction est appel� par l'event OnClick du bouton Close
    //(Icone de croix sur le livre en haut � droite)
    public void OnCloseNoteBook()
    {
        //Envoyer un RPC � tous les joueurs pour modifier les notes du livres
        //avec la vu de l'objet dans les mains et le contenu de la zone de saisie
        view.RPC("OnChangeNoteBookText", RpcTarget.All, selectedGameObject.GetComponent<PhotonView>().ViewID, zoneDeSaisie.text);
        //Enlever le UI du livre
        panel.SetActive(false);
        //Remettre l'�tat du curseur en active
        Cursor.lockState = CursorLockMode.Locked;
        //Remettre l'�tat de l'�criture � faux pour le joueur dans son script de mouvement
        //pour qu'il puisse de nouveau bouger
        GetComponent<PlayerMovement>().isWriting = false;
        //Remettre l'�tat de l'�criture � faux pour le joueur dans son script de cam�ra
        //pour qu'il puisse de nouveau bouger sa cam�ra
        //GameObject.Find("Main Camera").GetComponent<MouseLook>().isWriting = false;
    }

    [PunRPC]
    void OnChangeNoteBookText(int viewID, string text)
    {
        //Recuperer la vu de l'objet qui contient les notes
        PhotonView photonView = PhotonNetwork.GetPhotonView(viewID);
        //Mettre le texte �crit par la joueur dans une chaine de caract�re nomm� text
        photonView.gameObject.GetComponent<NotesSave>().text = text;
        //Remmettre l'�tat du livre � faux (N'est plus ouvert)
        photonView.gameObject.GetComponent<NotesSave>().isOpen = false;
    }


    public void OnCloseCraftingUI()
    {
        CraftingUI.SetActive(false);
        isCrafting = false;
        //Remettre l'�tat du curseur en active
        Cursor.lockState = CursorLockMode.Locked;
        //Remettre l'�tat de l'�criture � faux pour le joueur dans son script de mouvement
        //pour qu'il puisse de nouveau bouger
        GetComponent<PlayerMovement>().isWriting = false;
    }

    public void OnCraftClick(Transform panel)
    {
        Transform recipePanel = panel.GetChild(0);
        Transform craftPanel = panel.GetChild(1);
        if (view.IsMine)
        {
            List<Transform> recipeList = new List<Transform>();
            for (int i = 0; i < recipePanel.childCount; i++)
            {
                if (recipePanel.GetChild(i).name.ToUpper().Contains("ICON"))
                    recipeList.Add(recipePanel.GetChild(i));
            }

            List<Transform> slotsChecked = new List<Transform>();
            foreach (Transform transformRecipeList in recipeList)
            {
                for (int i = 0; i < playerInventory.childCount; i++)
                {
                    if (playerInventory.GetChild(i).childCount > 0)
                    {
                        if (playerInventory.GetChild(i).GetChild(0).name.Contains(transformRecipeList.name)&&
                            !slotsChecked.Contains(playerInventory.GetChild(i)))
                        {
                            slotsChecked.Add(playerInventory.GetChild(i));
                            break;
                        }
                            
                        //if (recipeList.Count == slotsChecked.Count)
                        //    break;
                    }

                }
            }

            if (recipeList.Count == slotsChecked.Count)
            {
                foreach (Transform transformSlotsChecked in slotsChecked)
                {
                    GetComponent<Inventory>().isFull[transformSlotsChecked.GetComponent<Slot>().slotIndex] = false;
                    Destroy(transformSlotsChecked.GetChild(0).gameObject);
                    PhotonView photonViewToDestroy;
                    if (transformSlotsChecked.childCount == 2)
                    {
                        photonViewToDestroy = transformSlotsChecked.GetChild(1).GetComponent<PhotonView>();
                        if (photonViewToDestroy.ViewID <= 999)
                            view.RPC("RPC_DestroyItemFromInvByMaster", RpcTarget.MasterClient, photonViewToDestroy.ViewID);
                        else
                            view.RPC("RPC_DestroyItemFromInv", RpcTarget.All, photonViewToDestroy.ViewID);

                    }
                    else
                    {
                        if (transformSlotsChecked.childCount == 1)
                        {
                            photonViewToDestroy = dropPoint.GetChild(0).GetComponent<PhotonView>();
                            if (photonViewToDestroy.ViewID <= 999)
                                view.RPC("RPC_DestroyItemFromInvByMaster", RpcTarget.MasterClient, photonViewToDestroy.ViewID);
                            else
                                view.RPC("RPC_DestroyItemFromInv", RpcTarget.All, photonViewToDestroy.ViewID);
                        }
                    }


                }

                //StartCoroutine(CleanInventoryBeforeCraft());

                GameObject craftedItem = PhotonNetwork.Instantiate(
                    craftPanel.GetChild(1).name.Replace(" Icon",""),
                    transform.position,
                    transform.rotation);
                craftedItem.GetComponent<Pickup>().picked = true;
                GameObject itemButton = craftedItem.GetComponent<Pickup>().itemButton;
                Inventory inventory = GetComponent<Inventory>();
               
                bool checkInvFull=false;
                for (int i = 0; i < inventory.slots.Length; i++)
                {
                    //Si le slots[i] n'est pas remplis et que le gameobject slots[i] n'a aucun fils
                    //�a veut dire que le slot est libre
                    if (inventory.isFull[i] == false)
                    {
                        //Mettre la case i du tableau full � vrai
                        //pour indiquer que le slot est rempli
                        inventory.isFull[i] = true;
                        //Instancier l'�cone du raccourci dans le UI du joueur
                        Instantiate(itemButton, inventory.slots[i].transform, false);
                        //Envoyer un RPC aux autres joueurs pour notifier l'�tat de l'objet r�cup�r�
                        view.RPC("RPC_PickUpObjectAfterCraft", RpcTarget.Others, craftedItem.GetComponent<PhotonView>().ViewID);
                        //Si c'est ma vue (test potentiellement inutile)
                        if (view.IsMine)
                        {
                            //Mettre l'objet en �tat Kinematic
                            craftedItem.GetComponent<Rigidbody>().isKinematic = true;
                            //Activer le rigibody
                            craftedItem.GetComponent<Rigidbody>().WakeUp();
                            //Mettre l'objet en tant que fils du slot
                            craftedItem.transform.SetParent(inventory.slots[i].transform);
                            //Mettre son �tat active � faux
                            craftedItem.SetActive(false);
                        }
                        //Quitter la boucle d�s qu'on r�cup�re un objet
                        checkInvFull = true;
                        break;
                    }
                }

                //if (!checkInvFull)
                //{
                //    invNotification.GetComponent<Text>().text = "Your inventory is full";
                //    invNotification.GetComponent<Animator>().SetTrigger("triggerNotif");
                //}
            }
            else
            {
               // invNotification.GetComponent<Text>().text = "You don't have enough items\nto craft this";
                invNotification.GetComponent<Animator>().SetTrigger("triggerNotif");
            }

        }
    }

    [PunRPC]
    public void RPC_DestroyItemFromInvByMaster(int viewToDestroyID)
    {
        //StartCoroutine(DestroyItemFromInvByMaster(PhotonNetwork.GetPhotonView(viewToDestroyID)));
        PhotonNetwork.Destroy(PhotonNetwork.GetPhotonView(viewToDestroyID));
    }

    [PunRPC]
    public void RPC_DestroyItemFromInv(int viewToDestroyID)
    {
        //StartCoroutine(DestroyItemFromInv(PhotonNetwork.GetPhotonView(viewToDestroyID)));
        if (PhotonNetwork.GetPhotonView(viewToDestroyID).IsMine)
        {
            PhotonNetwork.Destroy(PhotonNetwork.GetPhotonView(viewToDestroyID));
        }
    }

    //IEnumerator DestroyItemFromInv(PhotonView photonView)
    //{
    //    if (photonView.IsMine)
    //    {
    //        PhotonNetwork.Destroy(photonView);
    //    }
    //    yield return null;
    //}

    [PunRPC]
    public void RPC_PickUpObjectAfterCraft(int viewToDisableID)
    {
        //R�cuperer la vue Photon de l'objet r�cup�rer par le joueur
        PhotonView viewToDisable = PhotonNetwork.GetPhotonView(viewToDisableID);
        //Annoncer aux autres joueurs que son �tat est Picked
        viewToDisable.gameObject.GetComponent<Pickup>().picked = true;
        //Mettre l'�tat active de l'objet � Faux pour qu'il n'appara�t plus dans leur sc�ne
        viewToDisable.gameObject.SetActive(false);
    }

    [PunRPC]
    public void RPC_ThrowBall(int itemToThrowViewID)
    {
        
        GameObject ball = PhotonNetwork.GetPhotonView(itemToThrowViewID).gameObject;
        

        Transform itemToDrop = ball.gameObject.transform;
        //Enlever le parent de l'objet
        itemToDrop.SetParent(null);
        //Mettre l'�tat picked de l'objet � faux pour qu'il soit de nouveau r�cup�rable
        itemToDrop.gameObject.GetComponent<Pickup>().picked = false;
        itemToDrop.gameObject.layer = 0;
        //R�cup�rer son Rigibody
        Rigidbody rbItemToDrop = itemToDrop.gameObject.GetComponent<Rigidbody>();
        //Mettre l'�tat Kinematic de son rigibody � faux
        rbItemToDrop.isKinematic = false;
        //Mettre sa position � celle du dropPoint pour qu'il tombe � partir de ce point
        rbItemToDrop.transform.position = dropPoint.position;
        Vector3 direction = Camera.main.ScreenPointToRay(Input.mousePosition).direction;
        itemToDrop.gameObject.GetComponent<Rigidbody>().AddForce(direction * throwForce);

    }

    [PunRPC]
    public void RPC_UseBandage()
    {
        PhotonView photonViewToDestroy = dropPoint.GetChild(0).GetComponent<PhotonView>();
        if (photonViewToDestroy.ViewID <= 999)
            view.RPC("RPC_DestroyItemFromInvByMaster", RpcTarget.MasterClient, photonViewToDestroy.ViewID);
        else
            view.RPC("RPC_DestroyItemFromInv", RpcTarget.All, photonViewToDestroy.ViewID);

        if (GetComponent<Human>().health + bandageHeal > GetComponent<Human>().initialHealth)
            GetComponent<Human>().health = GetComponent<Human>().initialHealth;
        else
            GetComponent<Human>().health += bandageHeal;
    }




}
