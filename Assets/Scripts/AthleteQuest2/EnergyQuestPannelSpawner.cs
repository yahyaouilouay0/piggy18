using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

/// <summary>
///     Script to add on the athlete the make him able to interract with the enegry station to be able to do the puzzle in it
/// </summary>
public class EnergyQuestPannelSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject[] grids;
    private GameObject chosenGrid;
    PhotonView view;

    public Text warningText;

    public GameObject batteryRef;

    private int totalCorrectMoves;
    private int currentCorrectMoves;

    public bool puzzleSolved;

    private void Start()
    {
        view = gameObject.GetComponent<PhotonView>();
    }

    private void Update()
    {
        if (view.IsMine)
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var selection = hit.transform;
                if (selection.CompareTag("EnergyStation") && Input.GetKeyDown(KeyCode.F) && puzzleSolved == false)
                {
                    int rand = Random.Range(0, grids.Length);
                    chosenGrid = grids[rand];
                    totalCorrectMoves = chosenGrid.GetComponent<GridSolution>().totalMoves;
                    currentCorrectMoves = chosenGrid.GetComponent<GridSolution>().nbQuadpipes;
                    Cursor.lockState = CursorLockMode.None;
                    GameObject.Find("AthelteFPS(Clone)").GetComponent<PlayerMovement>().isWriting = true;
                    GameObject gridInstance = Instantiate(chosenGrid, chosenGrid.transform.position, Quaternion.identity);
                    gridInstance.transform.SetParent(gameObject.GetComponentInChildren<Canvas>().gameObject.transform);
                    gridInstance.GetComponent<RectTransform>().localPosition = new Vector3(0f, 0f, 0f);
                }
            }
        } 
    }

    public void CloseQuestPanel()
    {
        Cursor.lockState = CursorLockMode.Locked;
        GameObject.Find("AthelteFPS(Clone)").GetComponent<PlayerMovement>().isWriting = false;
        Destroy(GameObject.FindObjectOfType<GridSolution>().gameObject);
    }

    private IEnumerator ColosingPuzzleDelay()
    {
        yield return new WaitForSeconds(1f);
        CloseQuestPanel();
        PhotonNetwork.Instantiate(batteryRef.name, new Vector3(867.748413f, 55.5821762f, -712.038086f), batteryRef.transform.rotation);     
    }
    public void CorrectMove()
    {
        currentCorrectMoves++;

        if (currentCorrectMoves == totalCorrectMoves)
        {
            puzzleSolved = true;
            GameObject.FindObjectOfType<AudioManager>().Play("QuestCompleted");
            StartCoroutine(ColosingPuzzleDelay());
        }
    }

    public void WrongMove()
    {
        currentCorrectMoves--;
    }
}
