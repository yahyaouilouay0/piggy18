using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///     This class manage the pipes of the Energy Quest 
///         - Pipes Rotation
///         
/// </summary>
public class Pipes : MonoBehaviour
{

    private bool rotating;
    float[] rotations = { 0, 90, 180, 270};
    public float[] correctRotation;
    public bool pieceOfSolution;
    public bool inPlace = false;

    public EnergyQuestPannelSpawner managerRef;

    private void Start()
    {
        managerRef = GameObject.Find("AthelteFPS(Clone)").GetComponent<EnergyQuestPannelSpawner>();

        int rand = Random.Range(0, rotations.Length);
        transform.eulerAngles = new Vector3(0f, 0f, rotations[rand]);
        
        if (pieceOfSolution)
        {
            switch (correctRotation.Length)
            {
                case 1:
                    if (transform.eulerAngles.z >= correctRotation[0] - 5 && transform.eulerAngles.z <= correctRotation[0] + 5)
                    {
                        inPlace = true;
                        managerRef.CorrectMove();
                    }
                    break;
                case 2:
                    if ((transform.eulerAngles.z >= correctRotation[0] - 5 && transform.eulerAngles.z <= correctRotation[0] + 5) ||
                        (transform.eulerAngles.z >= correctRotation[1] - 5 && transform.eulerAngles.z <= correctRotation[1] + 5))
                    {
                        inPlace = true;
                        managerRef.CorrectMove();
                    }
                    break;
                default:
                    inPlace = true;
                    break;
            }
        }
        
    }

    private IEnumerator Rotate(Vector3 angles, float duration)
    {
        rotating = true;
        Quaternion startRotation = gameObject.transform.rotation;
        Quaternion endRotation = Quaternion.Euler(angles) * startRotation;
        for (float t = 0; t < duration; t += Time.deltaTime)
        {
            gameObject.transform.rotation = Quaternion.Lerp(startRotation, endRotation, t / duration);
            yield return null;
        }
        gameObject.transform.rotation = endRotation;
        rotating = false;
        if (pieceOfSolution)
        {
            switch (correctRotation.Length)
            {
                case 1:
                    if ((transform.eulerAngles.z >= correctRotation[0] - 5 && transform.eulerAngles.z <= correctRotation[0] + 5) && 
                        inPlace == false)
                    {
                        inPlace = true;
                        managerRef.CorrectMove();
                    }
                    else if (inPlace == true)
                    {
                        inPlace = false;
                        managerRef.WrongMove();
                    }
                    break;
                case 2:
                    if (((transform.eulerAngles.z >= correctRotation[0] - 5 && transform.eulerAngles.z <= correctRotation[0] + 5) ||
                        (transform.eulerAngles.z >= correctRotation[1] - 5 && transform.eulerAngles.z <= correctRotation[1] + 5)) && 
                        inPlace == false)
                    {
                        inPlace = true;
                        managerRef.CorrectMove();
                    }
                    else if (inPlace == true)
                    {
                        inPlace = false;
                        managerRef.WrongMove();
                    }
                    break;
                default:
                    inPlace = true;
                    break;
            }
        }
    }

    public void RotatePipe()
    {
        if (!rotating)
        {
            StartCoroutine(Rotate(new Vector3(0, 0, 90), 1));
            
        }
            
    }
}
