using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.VFX;
using Photon.Realtime;

public class PillarThreeInteractable : MonoBehaviour, IInteractable
{

    public GameObject pillarThree;
    public GameObject pillarSix;
    public GameObject pillarFour;

    PhotonView view;

    private void Awake()
    {
        view = GetComponent<PhotonView>();
        transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
    }

    private void Start()
    {
        pillarThree = GameObject.Find("FirePillarThree");
        pillarSix = GameObject.Find("FirePillarSix");
        pillarFour = GameObject.Find("FirePillarFour");
    }
    void IInteractable.Interact()
    {
        view.RPC("InteractionRPC", RpcTarget.All);
    }

    
    [PunRPC]
    public void InteractionRPC()
    {

        if (pillarThree.transform.GetChild(0).gameObject.activeSelf)
        {
            pillarThree.transform.GetChild(0).gameObject.SetActive(false);
            pillarThree.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(false);
            pillarThree.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
        }
        else
        {
            pillarThree.transform.GetChild(0).gameObject.SetActive(true);
            pillarThree.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(true);
            pillarThree.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StartFire");
        }


        if (pillarSix.transform.GetChild(0).gameObject.activeSelf)
        {
            pillarSix.transform.GetChild(0).gameObject.SetActive(false);
            pillarSix.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(false);
            pillarSix.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
        }
        else
        {
            pillarSix.transform.GetChild(0).gameObject.SetActive(true);
            pillarSix.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(true);
            pillarSix.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StartFire");
        }


        if (pillarFour.transform.GetChild(0).gameObject.activeSelf)
        {
            pillarFour.transform.GetChild(0).gameObject.SetActive(false);
            pillarFour.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(false);
            pillarFour.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
        }
        else
        {
            pillarFour.transform.GetChild(0).gameObject.SetActive(true);
            pillarFour.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(true);
            pillarFour.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StartFire");
        }
    }
}
