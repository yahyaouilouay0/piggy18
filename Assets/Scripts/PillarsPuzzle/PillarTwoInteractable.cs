using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.VFX;
using Photon.Realtime;

public class PillarTwoInteractable : MonoBehaviour, IInteractable
{
    public GameObject pillarOne;
    public GameObject pillarFour;
    PhotonView view;


    private void Awake()
    {
        view = GetComponent<PhotonView>();
        transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
    }

    private void Start()
    {
        pillarOne = GameObject.Find("FirePillarOne");
        pillarFour = GameObject.Find("FirePillarFour");
    }

    void IInteractable.Interact()
    {
        view.RPC("InteractionRPC", RpcTarget.All);
    }



    
    [PunRPC]
    public void InteractionRPC()
    {

        if (pillarOne.transform.GetChild(0).gameObject.activeSelf)
        {
            pillarOne.transform.GetChild(0).gameObject.SetActive(false);
            pillarOne.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(false);
            pillarOne.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
        } 
        else
        {
            pillarOne.transform.GetChild(0).gameObject.SetActive(true);
            pillarOne.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(true);
            pillarOne.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StartFire");
        }

        if (pillarFour.transform.GetChild(0).gameObject.activeSelf)
        {
            pillarFour.transform.GetChild(0).gameObject.SetActive(false);
            pillarFour.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(false);
            pillarFour.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
        }
        else
        {
            pillarFour.transform.GetChild(0).gameObject.SetActive(true);
            pillarFour.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(true);
            pillarFour.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StartFire");
        }
    }
    
}
