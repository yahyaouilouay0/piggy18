using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.VFX;
using Photon.Realtime;


public class PillarOneInteractable : MonoBehaviour, IInteractable
{

    PhotonView view;
    public GameObject pillarTwo;
    public GameObject pillarFive;
    

    private void Awake()
    {
        view = GetComponent<PhotonView>();

    }
    private void Start()
    {
        pillarTwo = GameObject.Find("FirePillarTwo");
        pillarFive = GameObject.Find("FirePillarFive");
        transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");

    }
    void IInteractable.Interact()
    {
        view.RPC("InteractionRPC", RpcTarget.All);
    }

    
    [PunRPC]
    public void InteractionRPC()
    {
        

        if (pillarTwo.transform.GetChild(0).gameObject.activeSelf)
        {
            pillarTwo.transform.GetChild(0).gameObject.SetActive(false);
            pillarTwo.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(false);
            pillarTwo.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
        }
        else
        {
            pillarTwo.transform.GetChild(0).gameObject.SetActive(true);
            pillarTwo.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(true);
            pillarTwo.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StartFire");
        }

        if (pillarFive.transform.GetChild(0).gameObject.activeSelf)
        {
            pillarFive.transform.GetChild(0).gameObject.SetActive(false);
            pillarFive.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(false);
            pillarFive.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StopFire");
        }
        else
        {
            pillarFive.transform.GetChild(0).gameObject.SetActive(true);
            pillarFive.transform.GetChild(1).transform.GetChild(1).gameObject.SetActive(true);
            pillarFive.transform.GetChild(1).transform.GetChild(0).GetComponent<VisualEffect>().SendEvent("StartFire");
        }
    }
    
}
