using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EmoteWheelController : MonoBehaviour
{
    public Animator anim;
    public Animator playerAnimator;
    private bool emoteWheelSelected = false;
    public static int emoteID;
    PhotonView view;
    private bool isLocked = true;

    private void Start()
    {
        view = transform.parent.parent.GetComponent<PhotonView>();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            emoteWheelSelected = !emoteWheelSelected;
        }

        if (emoteWheelSelected)
        {
            anim.SetBool("OpenEmoteWheel", true);
            //transform.parent.parent.GetComponent<PlayerMovement>().isWriting = true;
            Cursor.lockState = CursorLockMode.None;
            isLocked = false;
        }
        else
        {
            anim.SetBool("OpenEmoteWheel", false);
            //transform.parent.parent.GetComponent<PlayerMovement>().isWriting = true;
            if (Cursor.lockState == CursorLockMode.None && !isLocked)
            {
                Cursor.lockState = CursorLockMode.Locked;
                isLocked = true;
            }

        }

        switch (emoteID)
        {
            case 0:
                break;
            case 1:
                playerAnimator.SetTrigger("Threat");
                view.RPC("RPC_PlayAnimationWithTrigger", RpcTarget.Others, "Threat");
                emoteWheelSelected = false;
                emoteID = 0;
                break;
            case 3:
                playerAnimator.SetTrigger("Chicken");
                view.RPC("RPC_PlayAnimationWithTrigger", RpcTarget.Others, "Chicken");
                emoteWheelSelected = false;
                emoteID = 0;
                break;
            case 5:
                playerAnimator.SetTrigger("Time");
                view.RPC("RPC_PlayAnimationWithTrigger", RpcTarget.Others, "Time");
                emoteWheelSelected = false;
                emoteID = 0;
                break;
            case 7:
                playerAnimator.SetTrigger("Waving");
                view.RPC("RPC_PlayAnimationWithTrigger", RpcTarget.Others, "Waving");
                emoteWheelSelected = false;
                emoteID = 0;
                break;



        }
    }
}
