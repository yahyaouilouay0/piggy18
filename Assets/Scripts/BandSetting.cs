using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BandSetting : MonoBehaviour
{
    // Start is called before the first frame update
    private PhotonView view;
    public bool setBand;
    public float bandPosition;
    public Color bandColor;
    public bool bandIsSet;
    

    private void Start()
    {
        view = GetComponent<PhotonView>();
        setBand = false;
        bandIsSet = false;
    }
    private void Update()
    {
        if(setBand && !bandIsSet)
        {
            
            view.RPC("SettingBandColorAndPosition", RpcTarget.All);
        }
    }

    [PunRPC]
    public void SettingBandColorAndPosition()
    {
        gameObject.GetComponent<Renderer>().material.color = bandColor;
        gameObject.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z + 0.005f + (bandPosition * 0.005f));
        bandIsSet = true;
    }
}
