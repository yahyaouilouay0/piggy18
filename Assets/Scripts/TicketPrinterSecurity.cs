using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicketPrinterSecurity : MonoBehaviour
{

    public bool redOrbPresent;
    public bool greenOrbPresent;
    public bool blueOrbPresent;

    private GameObject redOrb;
    private GameObject greenOrb;
    private GameObject blueOrb;


    private void Start()
    {
        redOrbPresent = false;
        greenOrbPresent = false;
        blueOrbPresent = false;
    }

    private void Update()
    {
        if (redOrbPresent && redOrb != null)
            redOrb.transform.SetPositionAndRotation(new Vector3(transform.position.x, transform.position.y + 0.2f, transform.position.z), Quaternion.Euler(new Vector3(0f, 0f, 0f)));
        if (greenOrbPresent && greenOrb != null)
            greenOrb.transform.SetPositionAndRotation(new Vector3(transform.position.x, transform.position.y + 0.2f, transform.position.z), Quaternion.Euler(new Vector3(0f, 0f, 0f)));
        if (blueOrbPresent && blueOrb != null)
            blueOrb.transform.SetPositionAndRotation(new Vector3(transform.position.x, transform.position.y + 0.2f, transform.position.z), Quaternion.Euler(new Vector3(0f, 0f, 0f)));

    }




    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.name == "Red Orb(Clone)" && gameObject.name == "RedOrbSpot")
        {
            redOrbPresent = true;
            collision.gameObject.tag = "usedItem";
            redOrb = collision.gameObject;
        }

        if (collision.transform.name == "Green Orb(Clone)" && gameObject.name == "GreenOrbSpot")
        {
            greenOrbPresent = true;
            collision.gameObject.tag = "usedItem";
            greenOrb = collision.gameObject;
        }

        if (collision.transform.name == "Blue Orb(Clone)" && gameObject.name == "BlueOrbSpot")
        {
            blueOrbPresent = true;
            collision.gameObject.tag = "usedItem";
            blueOrb = collision.gameObject;
        }
    }

}


