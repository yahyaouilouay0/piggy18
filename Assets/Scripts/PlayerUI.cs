using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class for player UI management
/// </summary>
public class PlayerUI : MonoBehaviour
{
    private PhotonView view;
    public Text playerName;
    public List<GameObject> players;
    public Text human1;
    public Text human2;
    public Image healthBar;
    private float currentHealth;
    private float maxHealth;

    // Start is called before the first frame update
    void Start()
    {
        if ((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] != 0)
        {
            maxHealth = transform.parent.gameObject.GetComponent<Human>().health;
            view = transform.parent.gameObject.GetComponent<PhotonView>();
        }
            
        playerName.text = PhotonNetwork.LocalPlayer.NickName;
        //Looping through players list to display human names + their roles for each human player
        foreach (Player player in PhotonNetwork.PlayerListOthers)
        {
            //Condition to test if the player is not Joka
            if ((int)player.CustomProperties["Role"] != 0)
            {
                if ((int)player.CustomProperties["Role"] == 1)
                {
                    human1.text = "Engineer: " + player.NickName;
                    foreach (Player player2 in PhotonNetwork.PlayerListOthers)
                    {
                        if ((int)player2.CustomProperties["Role"] != 0 && (int)player2.CustomProperties["Role"] != 1)
                        {
                            if ((int)player2.CustomProperties["Role"] == 2)
                            {
                                human2.text = "Medic: " + player2.NickName;
                                break;
                            }
                            if ((int)player2.CustomProperties["Role"] == 3)
                            {
                                human2.text = "Athlete: " + player2.NickName;
                                break;
                            }
                        }
                    }
                }
                if ((int)player.CustomProperties["Role"] == 2)
                {
                    human1.text = "Medic: " + player.NickName;
                    foreach (Player player2 in PhotonNetwork.PlayerListOthers)
                    {
                        if ((int)player2.CustomProperties["Role"] != 0 && (int)player2.CustomProperties["Role"] != 2)
                        {
                            if ((int)player2.CustomProperties["Role"] == 1)
                            {
                                human2.text = "Engineer: " + player2.NickName;
                                break;
                            }
                            if ((int)player2.CustomProperties["Role"] == 3)
                            {
                                human2.text = "Athlete: " + player2.NickName;
                                break;
                            }
                        }
                    }
                }
                if ((int)player.CustomProperties["Role"] == 3)
                {
                    human1.text = "Athlete: " + player.NickName;
                    foreach (Player player2 in PhotonNetwork.PlayerListOthers)
                    {
                        if ((int)player2.CustomProperties["Role"] != 0 && (int)player2.CustomProperties["Role"] != 3)
                        {
                            if ((int)player2.CustomProperties["Role"] == 1)
                            {
                                human2.text = "Engineer: " + player2.NickName;
                                break;
                            }
                            if ((int)player2.CustomProperties["Role"] == 2)
                            {
                                human2.text = "Medic: " + player2.NickName;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    private void Update()
    {
        if((int)PhotonNetwork.LocalPlayer.CustomProperties["Role"] != 0)
        {
            if (view.IsMine)
            {
                currentHealth = transform.parent.gameObject.GetComponent<Human>().health;
                healthBar.fillAmount = currentHealth / maxHealth;
            }
        }
            
    }
}
